
<?php if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) ob_start('ob_gzhandler'); else ob_start(); 
use Session As Session;
if(isset($_GET['pid'])){
  Session::put("room",$_GET['pid']);
}else{
  Session::put("room","0");
}
?>
@include('js-localization::head')
<!DOCTYPE html>
<html lang="en">
  <head>
    @yield('js-localization.head')
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--meta http-equiv="refresh" content="1"-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <title>Dahlia - Sales Room</title>
  <script type="text/javascript">
  var timer;
  var times = 30;
  var slider = null;
  var isSliderLoaded = false;
/*var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-80283379-1']);
_gaq.push(['_setDomainName', 'none']);
_gaq.push(['_setAllowLinker', 'true']);
_gaq.push(['_trackPageview']);

(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();*/
</script>
  <link rel="stylesheet" href="{{asset('asset/css/bootstrap.min.css')}}">
  <script src="{{asset('asset/js/jquery.min.js')}}"></script>
  <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>-->
  <script src="{{asset('asset/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('asset/js/sweetalert2.min.js')}}"></script>
  <link rel="stylesheet" href="{{asset('asset/css/animate.css')}}">
  <!--<script src="//oss.maxcdn.com/jquery.form/3.50/jquery.form.min.js"></script>-->
  <link rel="stylesheet" href="{{asset('asset/css/sweetalert2.min.css')}}">
  <link rel="stylesheet" href="{{asset('asset/css/lightslider.min.css')}}">
  <script type="text/javascript" src="{{asset('asset/js/lightslider.min.js')}}"></script>
  <!--<script src="{{asset('asset/js/dialogbox.js')}}"></script>-->
  <script type="text/javascript" src="{{asset('asset/js/typed.js')}}"></script>
  <script type="text/javascript" src="{{asset('asset/js/canvasjs.min.js')}}"></script>
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/flaticon.css')}}"> 
  <!--script type="text/javascript" src="{{asset('asset/js/jquery.timer.js')}}"></script-->
  <!--link rel="stylesheet" href="{{asset('asset/css/main.css')}}"-->
  <!--link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css"-->
<!-- Latest compiled and minified JavaScript -->
<script type="text/javascript" src="{{asset('asset/js/continentmap.js')}}"></script>
<script type="text/javascript" src="{{asset('asset/js/mapdata.js')}}"></script>
<script type="text/javascript" src="{{asset('asset/js/jquery.slimscroll.min.js')}}"></script>
  <style type="text/css">
  @font-face {
  font-family: 'Lato';
  font-style: normal;
  src: local('Lato Regular'), local('Lato-Regular'), url({{asset('asset/font/IRANSansWeb_Light.woff2')}}) format('woff2');
}
/* latin */
@font-face {
  font-family: 'Lato';
  font-style: normal;
  src: local('Lato Regular'), local('Lato-Regular'), url({{asset('asset/font/IRANSansWeb_Light.woff2')}}) format('woff2');
}
/* latin-ext */
@font-face {
  font-family: 'Yanone Kaffeesatz';
  font-style: normal;
  src: local('Yanone Kaffeesatz Regular'), local('YanoneKaffeesatz-Regular'), url({{asset('asset/font/IRANSansWeb_Light.woff2')}}) format('woff2');
}
/* latin */
@font-face {
  font-family: 'Yanone Kaffeesatz';
  font-style: normal;
  src: local('Yanone Kaffeesatz Regular'), local('YanoneKaffeesatz-Regular'), url({{asset('asset/font/IRANSansWeb_Light.woff2')}}) format('woff2');
}
  button,input,select,.form-control,*,.btn{
    border-radius: 0px;
  font-family: 'Yanone Kaffeesatz', sans-serif;
  }
  nav{
    box-shadow: 0px 0px 3px #000;
  font-family: 'Yanone Kaffeesatz', sans-serif;
  }
  body{
    width: 100%;
    height: 100%; 
    padding: 0px;
    margin: 0px;
    overflow-x: hidden;
  font-family: 'Yanone Kaffeesatz', sans-serif;
  font-size: normal;
  background: #f0f0f0;
  position: absolute;


  background:url({{asset("asset/image/background/salesroom-2-min.png")}});
      background-size: 100% 100%;
      background-repeat: no-repeat;
      background-attachment: fixed;


  }
    .row{
      margin: 0px;
      padding: 0px;
    }
    #main{
      width:80%;
      position:absolute;
      top:0;
      right:20%;
      bottom:0;
      /*background:url({{asset("asset/image/background/salesroom-2-min.png")}});
      background-size: 100% 100%;
      background-repeat: no-repeat;
      background-attachment: fixed;*/
      z-index:900;
    }
    @media (max-width: 700px) {
      #users{
        display: none;
      }
      #main{
        left:0px;
        width:100%;
      }
    }
    #floataction{
      position:fixed;bottom:30px;right:30px;background:#297dce;border-radius:30px;width:60px;height:60px;
      transition: 0.3s;
      box-shadow: 0px 0px 8px #202020;
    }
    #floataction:hover{
      background:#1D5C98;
    }
    .container{
      width:100%;
      height: 100%;
      margin:0 auto;
      margin:0px;
      padding: 50px 0px 0px 0px;
    }
    .btn-login{
      background:#297dce;
      display:block;
      width:100%;
      text-align:center;
      transition: 0.3s;
    }
    .btn-login:hover{
    background: #226CB3;
    border-color:#226CB3;
    }
    #child{
      transition: 0.3s;
      position: relative;
      margin-right: 0px;
      padding-right: 0px;
      padding: 0px;
      margin-bottom: 10px;
      /*margin-bottom:5px;
      border-top: 2px #297dce solid;
      border-bottom:1px #cacaca solid;*/
    }
    #child .row{
      position:relative;width:98%;height:460px;
      background: #fff;
      transition: 0.5s;
      overflow-x: hidden;
      box-shadow:0px 1px 2px #808080;
    }
    #child:hover .row{
      /*border-bottom: 2px #297dce solid;*/
    }
    #child #pic{
      transition: 0.3s;
    }
    #child #prop{
      padding-left: 5px;
      position: relative;
      overflow: hidden;
    }
    #child #prop h3{
      color:#297dce;margin-top:10px;margin-bottom:0px;transition: 0.3s;
    }
    #child:hover #prop h3{
      color:#FA7B05;
    }
    #child:hover #pic{
      -webkit-filter: contrast(120%); /* Chrome, Safari, Opera */
      filter: contrast(120%);
    }
    #child:hover{
      border-color:#297dce;
    }
    .btn-order{
      margin:auto -3px;
      display: inline-block;
      transition: 0.3s;
      border-color: #eaeaea;
    }
    .btn-order:hover{
      background: #eaeaea;
      border-color: #eaeaea;
    }
    .btn-order-active{
      background: #297dce;
      color: #fff;
      border-color: #297dce;
      padding:5px 2px 4px 2px;
    }
    .btn-order-active:hover{
      background: #297dce;
      color: #fff;
      border-color: #297dce;
    }
    #order-box button:first-child{
      border-radius: 5px 0px 0px 5px;
    }
    #order-box button:last-child{
      border-radius: 0px 5px 5px 0px;
    }
    .bottom-bar-input{
      position: absolute;
      bottom: 0px;
      left: 0px;
      width:100%;
      border-top: 1px #d0d0d0 solid;
      z-index: 1001;
    }
    .price-header{
      position: absolute;
      top: 8px;
      margin: 0 auto;
      left:0px;
      right:0px;
      width:100%;
      text-align: center;
    }
    #content{
      position: absolute;
      top: 45px;
      margin: 0 auto;
      left:0px;
      right:0px;
      width:95%;
      padding: 0px 5px;
      background: rgba(256 ,256 ,256 ,0.8);
      box-shadow:0px 1px 10px #555;
      border-top: 2px #297dce solid;
      border-radius: 0px 0px 2px 2px;
      color: #000;
    }
    .price-header h3{
      margin:0px;
      padding: 3px 40px;
      text-align: center;
      width: 100%;
      display: inline;
      background: #fff;
      border-radius: 0px 0px 8px 8px;
      /*box-shadow: 0px 0px 3px blue;
      border-bottom: 2px #297dce solid;*/
      box-shadow: 0px 1px 8px #297dce;
    }
    .price-input{
      border: 0px;
      width: 100%;
      padding: 8px 30px;
      font-size: large;
      color: #303030;
      vertical-align: middle;
      outline: 0;
    }
    .send{
      position: absolute;
      top: 0px;
      left: 0px;
      bottom: 0px;
      border: 0px;
      background: #fff;
      font-size: larger;
      color: #297dce;
      padding: 3px 20px;
      vertical-align: middle;
      display: block;
      transition: 0.3s;
    }
    .send:hover{
      /*color:#FA7B05;*/
      background: #f0f0f0;
    }
    .send:focus{
      color:#FA7B05;
      /*background: #f0f0f0;*/
    }
    .user-row{
      width:100%;
      position: relative;
      padding:3px 10px;
      transition:0.3s;
      cursor:pointer;
    }
    .user-row:hover{
      background:#eaeaea;
    }
    .user-row .profile-pic{
      width:40px;
      height: 40px;
      display: inline-block;
      background: #ff9800;
      border-radius: 50%;
      position: relative;
      top:2px;
    }
    .user-row:nth-child(18n + 1) .profile-pic{background:#ff9800;}
    .user-row:nth-child(18n + 2) .profile-pic{background:#f44336;}
    .user-row:nth-child(18n + 3) .profile-pic{background:#673ab7;}
    .user-row:nth-child(18n + 4) .profile-pic{background:#03a9f4;}
    .user-row:nth-child(18n + 5) .profile-pic{background:#4caf50;}
    .user-row:nth-child(18n + 6) .profile-pic{background:#ffeb3b;}
    .user-row:nth-child(18n + 7) .profile-pic{background:#ff5722;}
    .user-row:nth-child(18n + 8) .profile-pic{background:#e91e63;}
    .user-row:nth-child(18n + 9) .profile-pic{background:#3f51b5;}
    .user-row:nth-child(18n + 10) .profile-pic{background:#00bcd4;}
    .user-row:nth-child(18n + 11) .profile-pic{background:#8bc34a;}
    .user-row:nth-child(18n + 12) .profile-pic{background:#ffc107;}
    .user-row:nth-child(18n + 13) .profile-pic{background:#795548;}
    .user-row:nth-child(18n + 14) .profile-pic{background:#9c27b0;}
    .user-row:nth-child(18n + 15) .profile-pic{background:#2196f3;}
    .user-row:nth-child(18n + 16) .profile-pic{background:#009688;}
    .user-row:nth-child(18n + 17) .profile-pic{background:#cddc39;}
    .user-row .profile-pic h4{
      position: absolute;
      top: 45%;
      transform: translateY(-50%);
      color: #fff;
      padding: 0px;
      margin: 0px;
      left: 0px;
      right: 0px;
      line-height: 0px;
      text-align: center;
    }
    .user-row .profile-info{
      position: absolute;
      top: 50%;
      left:0px;
      right:60px;
      transform: translateY(-50%);
    }
    .user-row .profile-info h4{
      margin: 0px;
    }
    .user-row .profile-info span{
      position: relative;
      display: inline-block;
      padding: 0px 5px;
      font-size: small;
      color: #297dce;
      font-weight: bold;
    }
    #content-content{
      position: relative;
      padding: 5px 10px;
    }
    span#extra{
      padding:5px 20px;
      color:#fff;
      position:absolute;
      top:-2px;
      left:10px;
      background:#297dce;
      border-radius:5px;
      transition: 0.3s;
      cursor: pointer;
    }
    span#extra:hover{
      background: #ff9800;
    }
    .gray-box{
      position: fixed;
      display: block;
      content: ' ';
      width: 100%;
      height: 100%;
      top:0px;
      left:0px;
      background: rgba(0,0,0,0.6);
      z-index: 3000;
      display: none;
    }
    #attachment{
      z-index: 3500;
      position: fixed;
      top: 50%;
      width: 100%;
      transform: translateY(-50%);
      display: none;
    }
    #chat{
      z-index: 3500;
      position: fixed;
      top: 50%;
      width: 100%;
      transform: translateY(-50%);
      display: none;
    }
    #chat > div{
      background: #fff;
      padding: 0px;
      /*box-shadow: 0px 0px 1px #eaeaea;*/
    }
    #attachment > div{
      background: #fff;
      /*box-shadow: 0px 0px 1px #eaeaea;*/
      padding: 5px 10px;
      border-top: 3px #297dce solid;
    }
    .dialog > div > img{
      position: absolute;
      top: -12px;
      right: -10px;
      cursor: pointer;
    }
    .chat-content{
      padding: 2px 0px;
    }
    .chat-content > div{
      width:100%;
      padding: 2px 10px;
      position: relative;
      display: block;
    }
    .chat-content > div.me{
      float: right;
    }
    .chat-content > div.he{
      float: left;
    }
    .chat-content > div.me > span{
      display: block;
      padding: 5px 10px;
      background: #90caf9;
      max-width: 60%;
      float: right;
      border-radius: 2px;
      position: relative;
      border-bottom: 2px #2196f3 solid;
    }
    .chat-content > div.he > span{
      display: block;
      padding: 5px 10px;
      background: #eeeeee;
      max-width: 60%;
      float: left;
      border-radius: 2px;
      position: relative;
      border-bottom: 2px #9e9e9e solid;
    }
    .chat-content > div.me.alone > span:before{
      display: block;
      content: ' ';
      position: absolute;
      right: -5px;
      top: 4px;
      width: 0; 
      height: 0;
      border-top: 5px solid transparent;
      border-bottom: 5px solid transparent;
      border-left: 5px solid #90caf9;
    }
    .chat-content > div.he.alone > span:before{
      display: block;
      content: ' ';
      position: absolute;
      left: -5px;
      top: 4px;
      width: 0; 
      height: 0;
      border-top: 5px solid transparent;
      border-bottom: 5px solid transparent;
      border-right: 5px solid #eeeeee;
    }
    canvas{
      position: relative;
      right: 0px;
    }
    #users-panel{
      display: none;
    }
    #users-panel > .panel-body{
      padding: 0px;
    }
    #users-panel > .panel-body > ul{
      padding: 0px;
      margin: 0px;
      display: block;
    }
    #users-panel > .panel-body > ul > li{
      margin:0px;
      width: 100%;
      padding: 5px 50px 5px 5px;
      display: block;
      border-bottom: 1px #ccc solid;
      position: relative;
      float: left;
    }
    #users-panel > .panel-body > ul > li > .num{
      background: #efefef;
      padding:12px 14px;
      position: absolute;
      right: 0px;
      top: 0px;
    }
    #users-panel > .panel-body > ul > li > .name{
      position: relative;
      top: 9px;
    }
    #users-panel > .panel-body > ul > li > .btn{
      float: left;
      transition: 0.3s;
    }
    #map svg{
      position: relative;
      top: 25px;
      width: 430px;
      height: 250px;
    }


    #bar-profile{
  position:absolute;
  top:7px;
  left: 20px;
  background: #1565c0;
  text-decoration:none;
  padding: 10px 50px 10px 20px;
  transition: 0.3s;
}
#bar-profile:hover{
  background: #0d47a1;
}
#bar-profile > a{
  color: #fff;
  position: relative;
  display: block;
  width: 100%;
  text-decoration: none;
}
  </style>
  </head>
  <body dir="rtl">
<div class="gray-box">
  
</div>
<div id="attachment" class="dialog">
  <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12 col-xs-offset-0">
  <img src="{{asset('asset/image/icon/error.png')}}" width="25" height="25">
     <ul id="imageGallery1" style="display:block;width:100%;">
                <li data-thumb="{{asset("asset/image/2.jpg")}}" data-src="{{asset("asset/image/2.jpg")}}">
                  <img src="{{asset("asset/image/2.jpg")}}" height="250" style="display:block;margin:0px auto;"/>
                </li>
                <li data-thumb="{{asset("asset/image/3.jpg")}}" data-src="{{asset("asset/image/3.jpg")}}">
                  <img src="{{asset("asset/image/3.jpg")}}" height="300" style="display:block;margin:0px auto;"/>
                </li>
                <li data-thumb="{{asset("asset/image/4.jpg")}}" data-src="{{asset("asset/image/4.jpg")}}">
                  <img src="{{asset("asset/image/4.jpg")}}" height="250" style="display:block;margin:0px auto;"/>
                </li>
                </ul>

  </div>
</div>

<div id="chat" class="dialog">
  <div class="col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2 col-xs-12 col-xs-offset-0">
    <img src="{{asset('asset/image/icon/error.png')}}" width="25" height="25">
    <div class="chat-box" style="overflow:hidden;">
      <div class="chat-header">
        <span style="background:#297dce;font-size:larger;display:block;width:100%;color:#fff;height:46px;padding:3px 0px;">
          <span style="padding:0px 10px;vertical-align:middle;">@lang('salesroom.chatwithseller')</span>
        </span>
      </div>
      <div class="chat-content" style="height:350px;background:url({{asset("asset/image/background/salesroom-1.jpeg")}});;width:100%;">
        <div class="me alone">
          <span>Hello i have a question</span>
        </div>
        <div class="me">
          <span>Have you any time to answer me?</span>
        </div>
        <div class="he alone">
          <span>Yeah of course!</span>
        </div>
        <div class="he">
          <span>Tell me your question</span>
        </div>
        <div class="he">
          <span>I'll become happy to help you!</span>
        </div>
        <div class="me alone">
          <span>Just i wanna know the price :-/</span>
        </div>
      </div>
      <div class="chat-footer" style="background:#eaeaea;box-shadow:0px -1px 1px #297dce;position:relative;">
        <input type="text" name="msg" placeholder="Enter text" style="border:none;outline:0;padding:7px 10px;background:#eaeaea;width:100%;">
        <button type="button" style="position:absolute;right:0px;top:0px;background:#297dce;padding:8px 20px;border:none;outline:0;color:#fff;">@lang('salesroom.send')</button>
      </div>
    </div>
  </div>
</div>
<nav class="navbar navbar-inverse navbar-fixed-top" style="background:#297dce;border-color:#297dce;width:100%;margin:0px;">
  <div class="container-fluid">
    <div class="navbar-header" style="float:right;">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#" style="color:#fff;">@lang('main.dahlia')</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar" style="float:right;">
      <span style="vertical-align:center;color:#fff;margin:5px 10px;position:relative;top:12px;display:none;">@lang('salesroom.activeusers') (10)
      </span>
      <ul class="nav navbar-nav" style="padding-right:0px;">
        <li><a href="#" style="color:#fff;">تعداد کاربران ۲ نفر</a></li>
        <li><a href="#" style="color:#fff;position:relative;">گفتگو ها
        <span class="badge" style="background:#f00;font-family:'Ubuntu';position:absolute;top:4px;left:3px;">5</span>
        </a></li>
        <li><a href="#" style="color:#fff;">@lang('main.home')</a></li>
      </ul>
      <div id="bar-profile">
        <a href="#" id="open-cart">
        <span>
        کاربر
        </span>
        <img src="{{asset('asset/image/icon/user-pro.png')}}" width="40" height="40" style="position:absolute;right:-50px;top:-10px;background:#0d47a1;border:0px;padding:7px;">
        </a>
      </div>
      <ul class="nav navbar-nav navbar-right">
        <!--<li><a href="#"><span class="glyphicon glyphicon-star"></span> Sign Up</a></li>
        <li><a href="#"><span class="glyphicon glyphicon-plus"></span> Login</a></li>-->
      </ul>
    </div>
  </div>
</nav>
    <div class="container">
      <div style="position:relative;width:100%;height:100%;">
        <div style="width:20%;position:fixed;top:45px;right:0;bottom:0;background:#fff;box-shadow:0px 2px 3px #505050;z-index:1000;padding-top:8px;overflow:hidden;" id="users">
        @foreach($sales as $sale)
          <div class="user-row" id="id-{{$sale->Id}}">
            <div class="profile-pic">
              <h4>{{substr(strtoupper($sale->Name),0,2)}}</h4>
            </div>
            <div class="profile-info">
              <h4>{{$sale->Name}}</h4>
              <span style="display:none;">@lang('salesroom.istyping')</span>
            </div>
          </div>
          @endforeach
        </div>
        <div id="main">
          <div style="width:100%;height:100%;position:relative;">
            <div class="price-header">
              <h3 style="font-weight:400;letter-spacing: 0px;font-size:16px;">@lang('salesroom.noprice')</h3>
            </div>
            <div id="content">
              <div style="width:100%;margin:0 auto;text-align:center;">
                <div style="display:inline-block;width:25%;height:60px;background:#03a9f4;margin:0.5%;position:relative;box-shadow:0px 0px 10px #000;cursor:pointer;" id="manage-user">
                  <img src="{{asset('asset/image/icon/group.png')}}" width="60" height="60" style="position:absolute;top:0px;right:0px;padding:10px 10px;">
                  <h4 style="display:inline-block;margin:0px;color:#fff;position:absolute;top:50%;transform: translateY(-50%);right:80px;">مدیریت کاربران</h4>
                </div>
                <div style="display:inline-block;width:25%;height:60px;background:#4caf50;margin:0.5%;position:relative;box-shadow:0px 0px 10px #000;">
                  <img src="{{asset('asset/image/icon/businessman.png')}}" width="60" height="60" style="position:absolute;top:0px;right:0px;padding:10px 10px;">
                  <h4 style="display:inline-block;margin:0px;color:#fff;position:absolute;top:50%;transform: translateY(-50%);right:80px;">مشخصات حراجی</h4>
                </div>
                <div style="display:inline-block;width:25%;height:60px;background:#f44336;margin:0.5%;position:relative;box-shadow:0px 0px 10px #000;">
                  <img src="{{asset('asset/image/icon/delete-shopping1.png')}}" width="60" style="position:absolute;top:0px;right:0px;padding:10px 10px;">
                  <h4 style="display:inline-block;margin:0px;color:#fff;position:absolute;top:50%;transform: translateY(-50%);right:80px;">پایان حراجی</h4>
                </div>
              </div>
              <div class="col-md-12">
                <div class="alert alert-success">
                  <strong>خوش آمدید!</strong> در صورتی که با کار با پنل مشکلی دارید به راهنمای پنل مراجعه کنید
                  <br><a href="#">راهنمای پنل</a>
                </div>
              </div>
              <div class="col-md-12">
              <div class="panel panel-default" id="users-panel">
                <div class="panel-heading"><h4>
                لیست کاربران
                </h4></div>
                <div class="panel-body">
                   <ul>
                    <li>
                      <span class="num">
                        1
                      </span>
                      <span class="name">
                        محسن
                      </span>
                      <button type="button" class="btn btn-success">گفتگو</button>
                      <button type="button" class="btn btn-primary">اطلاعات</button>
                      <button type="button" class="btn btn-danger">حذف</button>
                    </li>
                    <li>
                      <span class="num">
                        2
                      </span>
                      <span class="name">
                        محمد
                      </span>
                      <button type="button" class="btn btn-success">گفتگو</button>
                      <button type="button" class="btn btn-primary">اطلاعات</button>
                      <button type="button" class="btn btn-danger">حذف</button>
                    </li>
                   </ul>
                </div>
              </div>
              </div>
              <div style="width:100%;">
              <div class="col-md-6">
                <div class="panel panel-primary">
                  <div class="panel-heading"><h4>
                  مقایسه ی قیمت های گذاشته شده توسط کاربران
                  </h4></div>
                  <div class="panel-body" style="direction:ltr;">
                    <div id="chartContainer" style="height:257px;width:100%;"></div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="panel panel-primary">
                  <div class="panel-heading"><h4>
                  گستره ی مکانی کاربران
                  </h4></div>
                  <div class="panel-body" style="direction:ltr;">
                    <div id="map" style="width:100%;"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</body>
</html>
<script type="text/javascript">
var baseUrl = "{{substr(asset(''), 0, strlen(asset('')) - 1)}}";
var timeout;
var isTyping = false;
var marr = ['1','2'];
var lastPrice = 100;
function issetArray(str, arr) {
  var i;
  for(i = 0; i < arr.length; i++){
    if(arr[i] == str)
      return true;
  }
  return false;
}
setInterval(function(){
  $.ajax({
    url : baseUrl + "test/getType",
    success : function(data) {
      /*if(status.indexOf("Is not typing")  == -1 && data.indexOf("Is not typing")  == -1){
        status = data;
        console.log(data);
      }else{
        console.log('not going');
      }*/
      var i;
      var str = data.split("|");
      for (var i = 0; i < 20; i++) {
        if(!issetArray(i.toString(), marr))
          $("#id-" + i).find(".profile-info").find("span").fadeOut();
      }
      marr = [];
      for(i = 0;i < str.length;i++){
        if(str[i] != null && str[i] != ""){
          arr = str[i].split("#");
          marr[i] = arr[0].toString();
          $("#id-" + arr[0]).find(".profile-info").find("span").fadeIn();
          $("#id-" + arr[0]).find(".profile-info").find("span").html("Is Typing " + arr[1].substr(0,25));
        }
      }
    }
  });
},2000);


setInterval(function(){
  $.ajax({
    url : baseUrl + "test/getPrice",
    success : function(data) {
      if(data.status == 200){
        if(parseFloat(data.price) != lastPrice){
          lastPrice = parseFloat(data.price);
          $(".price-header > h3").typed({
            strings: [Lang.get('salesroom.lastprice') + " " + parseFloat(data.price) + "  " + Lang.get('salesroom.toman') + " " +  Lang.get('salesroom.from') + "  " + data.name],
            typeSpeed: 100,
            backDelay: 5000,
            showCursor: false
          });
        }
      }else{
        if(lastPrice != 0){
        lastPrice = 0;
        $(".price-header > h3").typed({
            strings: [Lang.get('salesroom.nothing')],
            typeSpeed: 100,
            backDelay: 5000,
            showCursor: false
          });
        }
      }
    }
  });
},3000);
$(document).ready(function(){
        var chart = new CanvasJS.Chart("chartContainer", {
        title: {
          text: "لیست قیمت ها",
          fontSize: 30
        },
        animationEnabled: true,
        axisX: {
          gridColor: "Silver",
          tickColor: "silver"
        },
        toolTip: {
          shared: true
        },
        theme: "theme2",
        axisY: {
          gridColor: "Silver",
          tickColor: "silver"
        },
        legend: {
          verticalAlign: "center",
          horizontalAlign: "right"
        },
        data: [
        {
          type: "line",
          showInLegend: true,
          lineThickness: 2,
          name: "محسن",
          color: "#F08080",
          dataPoints: [
          { x: 1, y: 500 },
          { x: 2, y: 1000 },
          { x: 4, y: 2500 },
          { x: 6, y: 5000 },
          { x: 8, y: 15000 },
          { x: 10, y: 40000 }
          ]
        },
        {
          type: "line",
          showInLegend: true,
          name: "محمد",
          color: "#20B2AA",
          lineThickness: 2,

          dataPoints: [
          { x: 3, y: 2000 },
          { x: 5, y: 3800 },
          { x: 7, y: 9000 },
          { x: 9, y: 25000 }
          ]
        }
        ],
        legend: {
          cursor: "pointer",
          itemclick: function (e) {
            if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
              e.dataSeries.visible = false;
            }
            else {
              e.dataSeries.visible = true;
            }
            chart.render();
          }
        }
      });

      chart.render();

      $("div#manage-user").click(function(){
        $("#users-panel").fadeIn(500);
      });
});
</script>