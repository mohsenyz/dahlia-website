<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <style type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz" rel="stylesheet"> 
    body{
    	margin: 0px;
    	padding: 0px;
    	width: 100%;
    	background: #efefef;
    	font-family: 'Yanone Kaffeesatz', sans-serif;
    }
    *{
    	font-family: 'Yanone Kaffeesatz', sans-serif;
    }
    @keyframes spinner {
    	from {transform: rotate(0deg);}
    	to {transform: rotate(360deg);}
	}
    .con{
    	display: block;
    	margin:0 auto;
    	padding: 0px;
    	width: 90%;
    	position: relative;
    	transition:0.5s;
    }
    .header{
    	background: #0d0d0d;
    	padding:2px 10px;
    	vertical-align: middle;
    	padding-left: 20px;
    }
    .header img{
    	width: 35px;
    	vertical-align: middle;
    	animation-name: spinner;
    	animation-duration: 5s;
    	animation-iteration-count: infinite;
    	animation-timing-function:linear;
    }
    .header h3{
    	display: inline-block;
    	color: white;
    	margin-left: 20px;
    	padding: 5px;
    }
    .con h2{
    	width: 100%;
    	display: inline-block;
    	text-align: center;
    }
    .cont{
    	padding: 0px 2%;
    	font-size: large;
    }
    .form{
    	width:100%;
    	text-align: center;
    	display: block;
    	padding:10px 0px;
    }
    .form span{
    	background: #2196f3;
    	color: #fff;
    	padding: 10px 50px;
    	border: 0px;
    	font-size: large;
    	transition: 0.3s;
    }
    .form button.a:hover{
    	background:  #3f51b5;
    	box-shadow: 0px 0px 3px #000;
    }
    .footer{
    	width:100%;
    	background: #0d0d0d;
    	text-align: center;
    	padding:20px 0px;
    	vertical-align: center;
    }
    .footer img{
    	padding: 0px 10px;
    	transition:0.5s;
    }
    .footer img:hover{
    	transform: rotate(360deg);
    }
    .alert{
    	width: 100%;
    	background: #2196f3;
    	margin: 0px;
    	margin-top: 40px;
    	position: relative;
    	padding: 0px;
    }
    .alert:before{
    	content:'';
    	display: block;
    	padding: 0px;
    	margin: 0px auto;
    	width: 0px;
    	height: 0px;
    	border-bottom: 15px #2196f3 solid;
    	border-left: 15px transparent solid;
		border-right: 15px transparent solid;
		position: absolute;
		top: -15px;
		left: 49%;
    }
    .alert span{
    	display: inline-block;
    	padding: 15px 5%;
    	color: #000000;
    	color: #fff;
    }
    </style>
</head>
<body>
<div class="con">
<div class="header">
	<img src="https://i.imgsafe.org/7c455d3a0b.png"><h3>Dahlia</h3>
</div>

<div>
</div>
<div class="cont">
<h2>Complete your registration</h2>
<p>Hello!<br>Thanks for your registring.<br>to complete your registring please enter below code in registration form:</p>
<div class="form"><span href="verify">{!! $code !!}</span></div>
</div>
<div class="alert"><span>If you dont request for registration in <a href="dahliaco.com">dahliaco.com</a> (free world side advertise system) We are sorry becuse of sending this message. please ignore it!</span></div>
<div class="footer"><img src="http://i.imgsafe.org/7cf58bd0ac.png"><img src="http://i.imgsafe.org/7d0251cfcd.png"><img src="http://i.imgsafe.org/7d02d528e7.png"></div>
</div>
</body>
</html>