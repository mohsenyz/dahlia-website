
<?php if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) ob_start('ob_gzhandler'); else ob_start(); 
use Session As Session;
if(isset($_GET['pid'])){
  Session::put("room",$_GET['pid']);
}else{
  Session::put("room","0");
}
?>
@include('js-localization::head')
<!DOCTYPE html>
<html lang="en">
  <head>
      @yield('js-localization.head')
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--meta http-equiv="refresh" content="1"-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <title>Dahlia - Sales Room</title>
  <script type="text/javascript">
  var timer;
  var times = 30;
  var slider = null;
  var isSliderLoaded = false;
/*var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-80283379-1']);
_gaq.push(['_setDomainName', 'none']);
_gaq.push(['_setAllowLinker', 'true']);
_gaq.push(['_trackPageview']);

(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();*/
</script>
  <link rel="stylesheet" href="{{asset('asset/css/bootstrap.min.css')}}">
  <script src="{{asset('asset/js/jquery.min.js')}}"></script>
  <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>-->
  <script src="{{asset('asset/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('asset/js/sweetalert2.min.js')}}"></script>
  <link rel="stylesheet" href="{{asset('asset/css/animate.css')}}">
  <!--<script src="//oss.maxcdn.com/jquery.form/3.50/jquery.form.min.js"></script>-->
  <link rel="stylesheet" href="{{asset('asset/css/sweetalert2.min.css')}}">
  <link rel="stylesheet" href="{{asset('asset/css/lightslider.min.css')}}">
  <script type="text/javascript" src="{{asset('asset/js/lightslider.min.js')}}"></script>
  <!--<script src="{{asset('asset/js/dialogbox.js')}}"></script>-->
  <script type="text/javascript" src="{{asset('asset/js/typed.js')}}"></script>
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/flaticon.css')}}"> 
  <!--script type="text/javascript" src="{{asset('asset/js/jquery.timer.js')}}"></script-->
  <!--link rel="stylesheet" href="{{asset('asset/css/main.css')}}"-->
  <!--link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css"-->
  <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet"> 
<!-- Latest compiled and minified JavaScript -->
<script type="text/javascript" src="{{asset('asset/js/jquery.mask.min.js')}}"></script>
<script type="text/javascript" src="{{asset('asset/js/jquery.slimscroll.min.js')}}"></script>
  <style type="text/css">
  @font-face {
  font-family: 'Lato';
  font-style: normal;
  src: local('Lato Regular'), local('Lato-Regular'), url({{asset('asset/font/IRANSansWeb_Light.woff2')}}) format('woff2');
}
/* latin */
@font-face {
  font-family: 'Lato';
  font-style: normal;
  src: local('Lato Regular'), local('Lato-Regular'), url({{asset('asset/font/IRANSansWeb_Light.woff2')}}) format('woff2');
}
/* latin-ext */
@font-face {
  font-family: 'Yanone Kaffeesatz';
  font-style: normal;
  src: local('Yanone Kaffeesatz Regular'), local('YanoneKaffeesatz-Regular'), url({{asset('asset/font/IRANSansWeb_Light.woff2')}}) format('woff2');
}
/* latin */
@font-face {
  font-family: 'Yanone Kaffeesatz';
  font-style: normal;
  src: local('Yanone Kaffeesatz Regular'), local('YanoneKaffeesatz-Regular'), url({{asset('asset/font/IRANSansWeb_Light.woff2')}}) format('woff2');
}
  button,input,select,.form-control,*,.btn{
    border-radius: 0px;
  font-family: 'Yanone Kaffeesatz', sans-serif;
  }
  nav{
    box-shadow: 0px 0px 3px #000;
  font-family: 'Yanone Kaffeesatz', sans-serif;
  }
  body{
    width: 100%;
    height: 100%; 
    padding: 0px;
    margin: 0px;
    overflow-x: hidden;
  font-family: 'Yanone Kaffeesatz', sans-serif;
  font-size: normal;
  background: #f0f0f0;
  position: absolute;
  }
    .row{
      margin: 0px;
      padding: 0px;
    }
    #main{
      width:75%;
      position:absolute;
      top:0;
      right:25%;
      bottom:0;
      background:url({{asset("asset/image/background/salesroom-2-min.png")}});
      background-size: 100% 100%;
      background-repeat: no-repeat;
      z-index:900;
    }
    @media (max-width: 700px) {
      #users{
        display: none;
      }
      #main{
        left:0px;
        width:100%;
      }
    }
    #floataction{
      position:fixed;bottom:30px;right:30px;background:#297dce;border-radius:30px;width:60px;height:60px;
      transition: 0.3s;
      box-shadow: 0px 0px 8px #202020;
    }
    #floataction:hover{
      background:#1D5C98;
    }
    .container{
      width:100%;
      height: 100%;
      margin:0 auto;
      margin:0px;
      padding: 50px 0px 0px 0px;
    }
    .btn-login{
      background:#297dce;
      display:block;
      width:100%;
      text-align:center;
      transition: 0.3s;
    }
    .btn-login:hover{
    background: #226CB3;
    border-color:#226CB3;
    }
    #child{
      transition: 0.3s;
      position: relative;
      margin-right: 0px;
      padding-right: 0px;
      padding: 0px;
      margin-bottom: 10px;
      /*margin-bottom:5px;
      border-top: 2px #297dce solid;
      border-bottom:1px #cacaca solid;*/
    }
    #child .row{
      position:relative;width:98%;height:460px;
      background: #fff;
      transition: 0.5s;
      overflow-x: hidden;
      box-shadow:0px 1px 2px #808080;
    }
    #child:hover .row{
      /*border-bottom: 2px #297dce solid;*/
    }
    #child #pic{
      transition: 0.3s;
    }
    #child #prop{
      padding-left: 5px;
      position: relative;
      overflow: hidden;
    }
    #child #prop h3{
      color:#297dce;margin-top:10px;margin-bottom:0px;transition: 0.3s;
    }
    #child:hover #prop h3{
      color:#FA7B05;
    }
    #child:hover #pic{
      -webkit-filter: contrast(120%); /* Chrome, Safari, Opera */
      filter: contrast(120%);
    }
    #child:hover{
      border-color:#297dce;
    }
    .btn-order{
      margin:auto -3px;
      display: inline-block;
      transition: 0.3s;
      border-color: #eaeaea;
    }
    .btn-order:hover{
      background: #eaeaea;
      border-color: #eaeaea;
    }
    .btn-order-active{
      background: #297dce;
      color: #fff;
      border-color: #297dce;
      padding:5px 2px 4px 2px;
    }
    .btn-order-active:hover{
      background: #297dce;
      color: #fff;
      border-color: #297dce;
    }
    #order-box button:first-child{
      border-radius: 5px 0px 0px 5px;
    }
    #order-box button:last-child{
      border-radius: 0px 5px 5px 0px;
    }
    .bottom-bar-input{
      position: absolute;
      bottom: 0px;
      left: 0px;
      width:100%;
      border-top: 1px #d0d0d0 solid;
      z-index: 1001;
    }
    .price-header{
      position: absolute;
      top: 8px;
      margin: 0 auto;
      left:0px;
      right:0px;
      width:100%;
      text-align: center;
    }
    #content{
      position: absolute;
      top: 45px;
      margin: 0 auto;
      left:0px;
      right:0px;
      width:95%;
      padding: 0px 5px;
      background: rgba(256 ,256 ,256 ,0.8);
      box-shadow:0px 1px 10px #555;
      border-top: 2px #297dce solid;
      border-radius: 0px 0px 2px 2px;
      color: #000;
    }
    .price-header h3{
      margin:0px;
      padding: 3px 40px;
      text-align: center;
      width: 100%;
      display: inline;
      background: #fff;
      border-radius: 0px 0px 8px 8px;
      /*box-shadow: 0px 0px 3px blue;
      border-bottom: 2px #297dce solid;*/
      box-shadow: 0px 1px 8px #297dce;
    }
    .price-input{
      border: 0px;
      width: 100%;
      padding: 8px 30px;
      font-size: large;
      color: #303030;
      vertical-align: middle;
      outline: 0;
    }
    .send{
      position: absolute;
      top: 0px;
      left: 0px;
      bottom: 0px;
      border: 0px;
      background: #fff;
      font-size: larger;
      color: #297dce;
      padding: 3px 20px;
      vertical-align: middle;
      display: block;
      transition: 0.3s;
    }
    .send:hover{
      /*color:#FA7B05;*/
      background: #f0f0f0;
    }
    .send:focus{
      color:#FA7B05;
      /*background: #f0f0f0;*/
    }
    .user-row{
      width:100%;
      position: relative;
      padding:3px 10px;
      transition:0.3s;
      cursor:pointer;
    }
    .user-row:hover{
      background:#eaeaea;
    }
    .user-row .profile-pic{
      width:50px;
      height: 50px;
      display: inline-block;
      background: #ff9800;
      border-radius: 50%;
      position: relative;
      top:2px;
    }
    .user-row:nth-child(18n + 1) .profile-pic{background:#ff9800;}
    .user-row:nth-child(18n + 2) .profile-pic{background:#f44336;}
    .user-row:nth-child(18n + 3) .profile-pic{background:#673ab7;}
    .user-row:nth-child(18n + 4) .profile-pic{background:#03a9f4;}
    .user-row:nth-child(18n + 5) .profile-pic{background:#4caf50;}
    .user-row:nth-child(18n + 6) .profile-pic{background:#ffeb3b;}
    .user-row:nth-child(18n + 7) .profile-pic{background:#ff5722;}
    .user-row:nth-child(18n + 8) .profile-pic{background:#e91e63;}
    .user-row:nth-child(18n + 9) .profile-pic{background:#3f51b5;}
    .user-row:nth-child(18n + 10) .profile-pic{background:#00bcd4;}
    .user-row:nth-child(18n + 11) .profile-pic{background:#8bc34a;}
    .user-row:nth-child(18n + 12) .profile-pic{background:#ffc107;}
    .user-row:nth-child(18n + 13) .profile-pic{background:#795548;}
    .user-row:nth-child(18n + 14) .profile-pic{background:#9c27b0;}
    .user-row:nth-child(18n + 15) .profile-pic{background:#2196f3;}
    .user-row:nth-child(18n + 16) .profile-pic{background:#009688;}
    .user-row:nth-child(18n + 17) .profile-pic{background:#cddc39;}
    .user-row .profile-pic h3{
      position: absolute;
      top: 50%;
      transform: translateY(-50%);
      color: #fff;
      padding: 0px;
      margin: 0px;
      left: 0px;
      right: 0px;
      text-align: center;
    }
    .user-row .profile-info{
      position: absolute;
      top: 50%;
      left:0px;
      right:80px;
      transform: translateY(-50%);
    }
    .user-row .profile-info h4{
      margin: 0px;
    }
    .user-row .profile-info span{
      position: relative;
      display: inline-block;
      padding: 0px 5px;
      font-size: small;
      color: #297dce;
      font-weight: bold;
    }
    #content-content{
      position: relative;
      padding: 5px 10px;
    }
    span#extra{
      padding:5px 20px;
      color:#fff;
      position:absolute;
      top:-2px;
      left:10px;
      background:#297dce;
      border-radius:5px;
      transition: 0.3s;
      cursor: pointer;
    }
    span#extra:hover{
      background: #ff9800;
    }
    .gray-box{
      position: fixed;
      display: block;
      content: ' ';
      width: 100%;
      height: 100%;
      top:0px;
      left:0px;
      background: rgba(0,0,0,0.6);
      z-index: 3000;
      display: none;
    }
    #attachment{
      z-index: 3500;
      position: fixed;
      top: 20%;
      width: 100%;
      transform: translateY(-50%);
      display: none;
      direction: ltr;
    }
    #chat{
      z-index: 3500;
      position: fixed;
      top: 17%;
      width: 100%;
      transform: translateY(-50%);
      display: none;
    }
    #chat > div{
      background: #fff;
      padding: 0px;
      /*box-shadow: 0px 0px 1px #eaeaea;*/
    }
    #attachment > div{
      background: #fff;
      /*box-shadow: 0px 0px 1px #eaeaea;*/
      padding: 5px 10px;
      direction: ltr;
      border-top: 3px #297dce solid;
    }
    .dialog > div > img{
      position: absolute;
      top: -12px;
      right: -10px;
      cursor: pointer;
    }
    .chat-content{
      padding: 2px 0px;
    }
    .chat-content > div{
      width:100%;
      padding: 2px 10px;
      position: relative;
      display: block;
    }
    .chat-content > div.me{
      float: right;
    }
    .chat-content > div.he{
      float: left;
    }
    .chat-content > div.me > span{
      display: block;
      padding: 5px 10px;
      background: #90caf9;
      max-width: 60%;
      float: right;
      border-radius: 2px;
      position: relative;
      border-bottom: 2px #2196f3 solid;
    }
    .chat-content > div.he > span{
      display: block;
      padding: 5px 10px;
      background: #eeeeee;
      max-width: 60%;
      float: left;
      border-radius: 2px;
      position: relative;
      border-bottom: 2px #9e9e9e solid;
    }
    .chat-content > div.me.alone > span:before{
      display: block;
      content: ' ';
      position: absolute;
      right: -5px;
      top: 4px;
      width: 0; 
      height: 0;
      border-top: 5px solid transparent;
      border-bottom: 5px solid transparent;
      border-left: 5px solid #90caf9;
    }
    .chat-content > div.he.alone > span:before{
      display: block;
      content: ' ';
      position: absolute;
      left: -5px;
      top: 4px;
      width: 0; 
      height: 0;
      border-top: 5px solid transparent;
      border-bottom: 5px solid transparent;
      border-right: 5px solid #eeeeee;
    }


    #bar-profile{
  position:absolute;
  top:7px;
  left: 20px;
  background: #1565c0;
  text-decoration:none;
  padding: 10px 50px 10px 20px;
  transition: 0.3s;
}
#bar-profile:hover{
  background: #0d47a1;
}
#bar-profile > a{
  color: #fff;
  position: relative;
  display: block;
  width: 100%;
  text-decoration: none;
}

    #entername{
      z-index: 3500;
      position: fixed;
      top: 45%;
      display: none;
      width: 100%;
      transform: translateY(-50%);
    }
    #entername > div{
      background: #fff;
      padding: 0px;
      /*box-shadow: 0px 0px 1px #eaeaea;*/
    }
  </style>
  </head>
  <body dir="rtl">
<div class="gray-box">
  
</div>
<div id="attachment" class="dialog">
  <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12 col-xs-offset-0">
  <img src="{{asset('asset/image/icon/error.png')}}" width="25" height="25">
     <ul id="imageGallery1" style="display:block;width:100%;direction:ltr;">
                <li data-thumb="{{asset("asset/image/2.jpg")}}" data-src="{{asset("asset/image/2.jpg")}}">
                  <img src="{{asset("asset/image/2.jpg")}}" height="250" style="display:block;margin:0px auto;"/>
                </li>
                <li data-thumb="{{asset("asset/image/3.jpg")}}" data-src="{{asset("asset/image/3.jpg")}}">
                  <img src="{{asset("asset/image/3.jpg")}}" height="300" style="display:block;margin:0px auto;"/>
                </li>
                <li data-thumb="{{asset("asset/image/4.jpg")}}" data-src="{{asset("asset/image/4.jpg")}}">
                  <img src="{{asset("asset/image/4.jpg")}}" height="250" style="display:block;margin:0px auto;"/>
                </li>
                </ul>

  </div>
</div>

<div id="chat" class="dialog">
  <div class="col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2 col-xs-12 col-xs-offset-0">
    <img src="{{asset('asset/image/icon/error.png')}}" width="25" height="25">
    <div class="chat-box" style="overflow:hidden;">
      <div class="chat-header">
        <span style="background:#297dce;font-size:larger;display:block;width:100%;color:#fff;height:46px;padding:13px 0px;">
          <span style="padding:0px 10px;vertical-align:middle;">@lang('salesroom.chatwithseller')</span>
        </span>
      </div>
      <div class="chat-content" style="background:url({{asset("asset/image/background/salesroom-1.jpeg")}});width:100%;" id="chat-content">
      </div>
      <div class="chat-footer" style="background:#eaeaea;box-shadow:0px -1px 1px #297dce;position:relative;">
        <input type="text" name="msg" placeholder="متن خود را بنویسید" style="border:none;outline:0;padding:9px 10px;background:#eaeaea;width:100%;" id="msg-input">
        <button type="button" style="position:absolute;left:0px;top:0px;background:#297dce;padding:10px 20px;border:none;outline:0;color:#fff;" id="chat-send-btn">@lang('salesroom.send')</button>
      </div>
    </div>
  </div>
</div>
<div id="entername" class="dialog">
  <div class="col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2 col-xs-12 col-xs-offset-0" style="padding:0px;position:relative;border-top:#297dce 2px solid;">
    <!--div id="content" style="width:100%;position:relative;"-->
      <input type="text" id="name" style="display:block;width:100%;padding:8px;border:0;font-size:larger;" placeholder="لطفا نام خود را وارد کنید">
      <button id="namesubmit" class="btn btn-primary" style="position:absolute;left:0px;top:-2px;padding:10px 20px;">تایید</button>
    <!--/div-->
  </div>
</div>
<nav class="navbar navbar-inverse navbar-fixed-top" style="background:#297dce;border-color:#297dce;width:100%;margin:0px;">
  <div class="container-fluid">
    <div class="navbar-header" style="float:right;">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#" style="color:#fff;">@lang('main.dahlia')</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar" style="float:right;">
      <span style="vertical-align:center;color:#fff;margin:5px 10px;position:relative;top:12px;display:none;">@lang('salesroom.activeusers') (10)
      </span>
      <ul class="nav navbar-nav" style="padding-right:0px;">
        <li id="btn-chat"><a href="#" style="color:#fff;position:relative;">@lang('salesroom.chatwithseller')
        <span class="badge" id="chat-unreaded" style="background:#f00;font-family:'Ubuntu';position:absolute;top:4px;left:3px;">0</span>
        </a></li>
        <li><a href="#" style="color:#fff;">@lang('main.home')</a></li>
      </ul>
      <div id="bar-profile">
        <a href="#" id="open-cart">
        <span>
        کاربر
        </span>
        <img src="{{asset('asset/image/icon/user-pro.png')}}" width="40" height="40" style="position:absolute;right:-50px;top:-10px;background:#0d47a1;border:0px;padding:7px;">
        </a>
      </div>
      <ul class="nav navbar-nav navbar-right">
        <!--<li><a href="#"><span class="glyphicon glyphicon-star"></span> Sign Up</a></li>
        <li><a href="#"><span class="glyphicon glyphicon-plus"></span> Login</a></li>-->
      </ul>
    </div>
  </div>
</nav>
    <div class="container">
      <div style="position:relative;width:100%;height:100%;">
        <div style="width:25%;position:absolute;top:0;right:0;bottom:0;background:#fff;box-shadow:0px 2px 3px #505050;z-index:1000;padding-top:8px;overflow:hidden;" id="users">
        @foreach($sales as $sale)
          <div class="user-row" id="id-{{$sale->Id}}">
            <div class="profile-pic">
              <h3>{{substr(strtoupper($sale->Name),0,2)}}</h3>
            </div>
            <div class="profile-info">
              <h4>{{$sale->Name}}</h4>
              <span style="display:none;">@lang('salesroom.istyping')</span>
            </div>
          </div>
          @endforeach
        </div>
        <div id="main">
          <div style="width:100%;height:100%;position:relative;">
            <div class="price-header">
              <h3 style="font-weight:400;letter-spacing: 0px;font-size:16px;">@lang('salesroom.noprice')</h3>
            </div>
            <div id="content">
              <div style="position:relative;">
                <span id="extra">@lang('salesroom.extra')</span>
                <h3 style="margin-top:10px;">{{$salesinfo->subject}}</h3> 
              </div>
              <hr style="margin:5px 0px;border-color:#cacaca;">
              <div id="content-content">
              <p style="font-size:large;line-height:25px;">
              {{$salesinfo->content}}
              </p>
              </div>
              <br>
              <div style="direction:ltr;">
              <ul id="imageGallery" style="display:block;width:100%;">
                <?php
                if(strpos($salesinfo->pic, ",") !== false){
                  $var = explode(",",$salesinfo->pic);
                  foreach ($var as $key => $value) {
                    if($value != "0"){
                ?>
                <li data-thumb="https://drive.google.com/uc?export=view&id={{$value}}" data-src="https://drive.google.com/uc?export=view&id={{$value}}">
                  <img src="https://drive.google.com/uc?export=view&id={{$value}}" height="300" style="display:block;margin:0px auto;"/>
                </li>
                <?php
                    }
                  }
                }else{
                ?>
                <li data-thumb="https://drive.google.com/uc?export=view&id={{$adv->Pic}}" data-src="https://drive.google.com/uc?export=view&id={{$salesinfo->pic}}">
                  <img src="https://drive.google.com/uc?export=view&id={{$adv->Pic}}" height="300" style="display:block;margin:0px auto;"/>
                </li>
                <?php
                }
                ?>
                </ul>
              </div>


            </div>
            <div style="position:absolute;bottom:40px;display:block;width:100%;">
              <div style="position:relative;width:100%;display:block;">
                              
              </div>
            </div>
            <div id="input" class="bottom-bar-input">
              <div style="position:relative;width:100%;height:100%;">
                <input type="text" name="text" class="price-input money" placeholder="@lang('salesroom.pricerule')" id="price-input">
                <button type="button" class="send">@lang('salesroom.send')</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</body>
</html>
<script type="text/javascript">
var baseUrl = "{{substr(asset(''), 0, strlen(asset('')) - 1)}}";
var lastId = 0;
var isChatShow = false;
var yourId = '{{Auth::user()->id}}';
    $.fn.hideChatDialog = function(){
      var it = $("div#chat");
      it.removeClass("animated fadeInDown");
      it.removeClass("animated fadeOutUp").addClass("animated fadeOutUp");
      $(".gray-box").fadeOut(600,function(){
        it.hide();
      });
      isChatShow = false;
    }
    $.fn.showChatDialog = function(){
      var it = $("div#chat");
      it.show();
      it.removeClass("animated fadeOutUp");
      it.removeClass("animated fadeInDown").addClass("animated fadeInDown");
      $(".gray-box").fadeIn();
      $(".dialog > div > img").off().click(function(){
        $(this).hideChatDialog();
      });
      isChatShow = true;
      $("#chat-content").scrollTop(1000 * 1000);
    }
    $.fn.showNameDialog = function(){
      var it = $("div#entername");
      it.show();
      it.removeClass("animated fadeOutUp");
      it.removeClass("animated fadeInDown").addClass("animated fadeInDown");
      $(".gray-box").fadeIn();
      $("input#name").focus();
      it.find('#namesubmit').click(function(){
        if($("input#name").val().length > 3){
        $.ajax({
          url : baseUrl + "/test/setName?name=" + $("input#name").val(),
          success : function(data) {
            if(data.status == 200){
              window.location.replace(baseUrl + "/fa/sales/room?pid=" + '<?php echo Session::get("room"); ?>');
            }
          }
        });
        }else{
          alert("نام شما حداقل میتواند سه حرف باشد");
        }
      });
      $("input#name").keypress(function(event) {
        if(event.which == 13){
          if($("input#name").val().length > 3){
          $.ajax({
            url : baseUrl + "/test/setName?name=" + $("input#name").val(),
            success : function(data) {
              if(data.status == 200){
                window.location.replace(baseUrl + "fa/sales/room?pid=" + '<?php echo Session::get("room"); ?>');
              }
            }
          });
          }else{
            alert("نام شما حداقل میتواند سه حرف باشد");
          }
        }
      });
    }
    $.fn.hideAttachDialog = function(){
      var it = $("div#attachment");
      it.removeClass("animated fadeInDown");
      it.removeClass("animated fadeOutUp").addClass("animated fadeOutUp");
      $(".gray-box").fadeOut(600,function(){
        it.hide();
      });
    }
    $.fn.showAttachDialog = function(){
      var it = $("div#attachment");
      it.show();
      it.removeClass("animated fadeOutUp");
      it.removeClass("animated fadeInDown").addClass("animated fadeInDown");
      $(".gray-box").fadeIn();

      $('#imageGallery1').lightSlider({
            gallery:false,
            item:1,
            loop:true,
            adaptiveHeight:true,
            slideMargin:0,
            enableDrag: true,
            currentPagerPosition:'left' 
        });


      $(".dialog > div > img").off().click(function(){
        $(this).hideAttachDialog();
      });
    }
var timeout;
var isTyping = false;
var marr = ['1','2'];
var lastPrice = 100;
$("#btn-chat").click(function(){
  $.ajax({
    url : baseUrl + "/salesroom/getMessages:admin/setReaded/until?id=" + lastId,
    success : function(data) {
      if(data.status == 200){
        $("#chat-unreaded").html(0).fadeOut();
      }
    }
  });
  $(document).showChatDialog();
});
$("#extra").click(function(){
  $(document).showAttachDialog();
});
function issetArray(str, arr) {
  var i;
  for(i = 0; i < arr.length; i++){
    if(arr[i] == str)
      return true;
  }
  return false;
}
setInterval(function(){
  $.ajax({
    url : baseUrl + "/test/getType",
    success : function(data) {
      /*if(status.indexOf("Is not typing")  == -1 && data.indexOf("Is not typing")  == -1){
        status = data;
        console.log(data);
      }else{
        console.log('not going');
      }*/
      var i;
      var str = data.split("|");
      for (var i = 0; i < 20; i++) {
        if(!issetArray(i.toString(), marr))
          $("#id-" + i).find(".profile-info").find("span").fadeOut();
      }
      marr = [];
      for(i = 0;i < str.length;i++){
        if(str[i] != null && str[i] != ""){
          arr = str[i].split("#");
          marr[i] = arr[0].toString();
          $("#id-" + arr[0]).find(".profile-info").find("span").fadeIn();
          $("#id-" + arr[0]).find(".profile-info").find("span").html(Lang.get('salesroom.istyping') + arr[1].substr(0,25));
        }
      }
    }
  });
},2000);


setInterval(function(){
  $.ajax({
    url : baseUrl + "/test/getPrice",
    success : function(data) {
      if(data.status == 200){
        if(parseFloat(data.price) != lastPrice){
          lastPrice = parseFloat(data.price);
          $(".price-header > h3").typed({
            strings: [Lang.get('salesroom.lastprice') + " " + parseFloat(data.price) + "  " + Lang.get('salesroom.toman') + " " +  Lang.get('salesroom.from') + "  " + data.name],
            typeSpeed: 100,
            backDelay: 5000,
            showCursor: false
          });
        }
      }else{
        if(lastPrice != 0){
        lastPrice = 0;
        $(".price-header > h3").typed({
            strings: [Lang.get('salesroom.nothing')],
            typeSpeed: 100,
            backDelay: 5000,
            showCursor: false
          });
        }
      }
    }
  });
},3000);
function setTyping (arg) {
  var str;
  if(arg)
    str = "true*" + $(".money").val();
  else
    str = "false*";
  $.ajax({
    url : baseUrl + "/test/type?type=" + str
  });
}
$(document).ready(function(){
  $('.money').mask('0,000,000,000,000.00', {reverse: true});
  $('.money').on('input',function(event) {
    event.preventDefault();
    if(!isTyping){
      isTyping = true;
    }
    setTyping(true);
    clearTimeout(timeout);
    timeout = setTimeout(function(){
      isTyping = false;
      setTyping(false);
    },1000);
    if($(this).val() != "" && $(this).val() !=  null){
    }
  });
  $(".send").click(function(event) {
    sendPrice();
  });

  function showThat(){
    swal({
      title: $("input#price-input").val() + " " + "تومان",
      html : true,
      html: "آیا مطمین به ثبت چنین قیمتی هستید؟",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'بله بفرست',
      cancelButtonText: 'خیر'
    }).then(function() {
      if(val != null && val != "" && val != " " && verify){
      $.ajax({
        url : baseUrl + "/test/setPrice?price=" + val,
        success : function(data) {
          if(data.status == 200){
            $("input#price-input").val('');
          }else{
            swal({title:"Error",text:"Your price must be more than latest price!",confirmButtonColor: "#c9302c"});
          }
        }
      });
      }else{
        swal({title:"Error",text:"Please enter a price to send!",confirmButtonColor: "#c9302c"});
      }
    });
  }
  function sendPrice(){
    verify = true;
    val = $("input#price-input").val();
    if(parseFloat(val.replace(',', '')) > (lastPrice * 4)){
      showThat();
    }else{
    if(val != null && val != "" && val != " " && verify){
      $.ajax({
        url : baseUrl + "/test/setPrice?price=" + val,
        success : function(data) {
          if(data.status == 200){
            $("input#price-input").val('');
          }else{
            swal({title:"Error",text:"Your price must be more than latest price!",confirmButtonColor: "#c9302c"});
          }
        }
      });
    }else{
      swal({title:"Error",text:"Please enter a price to send!",confirmButtonColor: "#c9302c"});
    }
    }
  }
  $("input#price-input").keypress(function(event) {
    if(event.which == 13){
      sendPrice();
    }
  });

  $("input#msg-input").keypress(function(event){
    if(event.which == 13){
      if($(this).val().trim() != "" && $(this).val() != null){
        sendChatMsg();
      }else{
        swal({title:"Error",text:"please enter text to send",confirmButtonColor: "#c9302c"});
      }
    }
  });
  $("#chat-send-btn").click(function(){
    if($(this).val().trim() != "" && $(this).val() != null){
      sendChatMsg();
    }else{
      swal({title:"Error",text:"please enter text to send",confirmButtonColor: "#c9302c"});
    }
  });
  function sendChatMsg(){
    $.ajax({
      url : baseUrl + "/salesroom/sendMessage:admin/msg?content=" + $("input#msg-input").val(),
      success : function(data) {
        $("input#msg-input").val(null);
        if(data.status != 200){
          swal({title:"Error",text:"You cant send message",confirmButtonColor: "#c9302c"});
        }
      }
    });
  }

  $('#imageGallery').lightSlider({
            gallery:false,
            item:1,
            loop:true,
            slideMargin:0,
            enableDrag: true,
            currentPagerPosition:'left' 
        });
 /* $('#imageGallery1').lightSlider({
            gallery:false,
            item:1,
            loop:true,
            adaptiveHeight:true,
            slideMargin:0,
            enableDrag: true,
            currentPagerPosition:'left' 
        });*/


  $('#content-content').slimScroll({
    height: '150px',
    color: '#297dce',
    opacity : 1,
    alwaysVisible : true
  });
  $('#chat-content').slimScroll({
    height: '350px',
    color: '#297dce',
    opacity : 1
  });

  @if(!$belong)
    $(document).showNameDialog();
  @endif
  $('#content-content').bind('scroll',chk_scroll);
  function chk_scroll(e){
    var elem = $(e.currentTarget);
    if (elem[0].scrollHeight - elem.scrollTop() == elem.outerHeight())
    {
        console.log("bottom");
    }
  }


setInterval(function(){
  $.ajax({
    url : baseUrl + "/salesroom/getMessages:admin/after?id=" + lastId,
    success : function(data) {
      if(data[data.length - 1] != undefined) lastId = data[data.length - 1].id;
      var unreaded = parseInt($("#chat-unreaded").html());
      for (var i = 0; i < data.length; i++) {
        if(data[i].sender == yourId){
          var alone = "alone";
          //if(data[i + 1] != undefined && data[i + 1].sender != yourId && ) alone = "alone";
          $("#chat-content").append('<div class="me ' + alone + '" id="' + data[i].id + '"><span>' + data[i].content + '</span></div>');
          $("#chat-content").find("div#" + data[i].id).hide().fadeIn();
          $("#chat-content").scrollTop(1000 * 1000);
        }else{
          if(data[i].read == "0")unreaded++;
          $("#chat-content").append('<div class="he ' + alone + '" id="' + data[i].id + '"><span>' + data[i].content + '</span></div>');
          $("#chat-content").find("div#" + data[i].id).hide().fadeIn();
        }
      }
      if(unreaded != 0)
        $("#chat-unreaded").html(unreaded).fadeIn();
      else
        $("#chat-unreaded").html(0).fadeOut();
      if(isChatShow){
        $.ajax({
          url : baseUrl + "/salesroom/getMessages:admin/setReaded/until?id=" + lastId,
          success : function(data) {
            if(data.status == 200){
              $("#chat-unreaded").html(0).fadeOut();
            }
          }
        });
      }
    }
  });
},3000);


});
</script>