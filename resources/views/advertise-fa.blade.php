<?php if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) ob_start('ob_gzhandler'); else ob_start(); 
?>
@include('js-localization::head')
<!DOCTYPE html>
<html lang="en">
  <head>
    @yield('js-localization.head')
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <title>Dahlia</title>
    <style type="text/css">
   @font-face {
  font-family: 'Lato';
  font-style: normal;
  src: local('Lato Regular'), local('Lato-Regular'), url({{asset('asset/font/IRANSansWeb_Light.woff2')}}) format('woff2');
}
/* latin */
@font-face {
  font-family: 'Lato';
  font-style: normal;
  src: local('Lato Regular'), local('Lato-Regular'), url({{asset('asset/font/IRANSansWeb_Light.woff2')}}) format('woff2');
}
/* latin-ext */
@font-face {
  font-family: 'Yanone Kaffeesatz';
  font-style: normal;
  src: local('Yanone Kaffeesatz Regular'), local('YanoneKaffeesatz-Regular'), url({{asset('asset/font/IRANSansWeb_Light.woff2')}}) format('woff2');
}
/* latin */
@font-face {
  font-family: 'Yanone Kaffeesatz';
  font-style: normal;
  src: local('Yanone Kaffeesatz Regular'), local('YanoneKaffeesatz-Regular'), url({{asset('asset/font/IRANSansWeb_Light.woff2')}}) format('woff2');
}
#cartShop{
  float:left;
  position:relative;
  left:30px;
}
#cartShop{
  position:absolute;
  top:7px;
  left: 130px;
  background: #1565c0;
  text-decoration:none;
  padding: 10px 50px 10px 20px;
  transition: 0.3s;
}
#cartShop:hover{
  background: #0d47a1;
}
#cartShop > a{
  color: #fff;
  position: relative;
  display: block;
  width: 100%;
  text-decoration: none;
}



#bar-profile{
  position:absolute;
  top:7px;
  left: 20px;
  background: #1565c0;
  text-decoration:none;
  padding: 10px 50px 10px 20px;
  transition: 0.3s;
}
#bar-profile:hover{
  background: #0d47a1;
}
#bar-profile > a{
  color: #fff;
  position: relative;
  display: block;
  width: 100%;
  text-decoration: none;
}




#cartShop > div{
  display: none;
  position:absolute;
  top:55px;
  background: #fff;
  width:250px;
  border:1px #297dce solid;
  box-shadow: 0px 0px 3px #555;
  border-radius: 2px;
  left:0px;
}
#cartShop > div > h6{
  color: #333;
  text-align: center;
}
#cartShop > div > ul{
  margin: 0px;
  padding: 0px 10px 5px 10px;
}
#cartShop > div > ul > li{
  text-align: right;
  color: #555;
  display: block;
  width: 100%;
  padding: 3px 0px;
}
#cartShop > div > ul > li > span{
  color: #4caf50;
  float: left;
}
#cartShop > div:before{
  display: block;
  content: ' ';
  position: absolute;
  left: 45px;
  top: -10px;
  width: 0; 
  height: 0;
  border-left: 10px solid transparent;
  border-right: 10px solid transparent;
  border-bottom: 10px solid #297dce;
}
    </style>
  <script type="text/javascript">
  var baseUrl = "{{substr(asset(''), 0, strlen(asset('')) - 1)}}";
  var timer;
  var times = 90;
  var slider = null;
  var isSliderLoaded = false;
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-80283379-1']);
_gaq.push(['_setDomainName', 'none']);
_gaq.push(['_setAllowLinker', 'true']);
_gaq.push(['_trackPageview']);

(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>
  <link rel="stylesheet" href="{{asset('asset/css/bootstrap.min.css')}}">
  <script src="{{asset('asset/js/jquery.min.js')}}"></script>
  <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>-->
  <script src="{{asset('asset/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('asset/js/sweetalert2.min.js')}}"></script>
  <link rel="stylesheet" href="{{asset('asset/css/animate.css')}}">
  <!--<script src="//oss.maxcdn.com/jquery.form/3.50/jquery.form.min.js"></script>-->
  <link rel="stylesheet" href="{{asset('asset/css/sweetalert2.min.css')}}">
  <link rel="stylesheet" href="{{asset('asset/css/main-fa.css')}}">
  <link rel="stylesheet" href="{{asset('asset/css/lightslider.min.css')}}">
  <script type="text/javascript" src="{{asset('asset/js/lightslider.min.js')}}"></script>
  <!--<script src="{{asset('asset/js/dialogbox.js')}}"></script>-->
  <script type="text/javascript" src="{{asset('asset/js/main.js')}}"></script>
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/flaticon.css')}}"> 
  <script type="text/javascript" src="{{asset('asset/js/jquery.timer.js')}}"></script>
  </head>
  <body dir="rtl">

<nav class="navbar navbar-inverse navbar-fixed-top" style="background:#297dce;border-color:#297dce;width:100%;margin:0px;">
  <div class="container-fluid">
    <div class="navbar-header" style="float:right;">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#" style="color:#fff;">@lang('main.dahlia')</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav" style="float:right;">
        <li><a href="#" style="color:#fff;">@lang('main.tickets')</a></li>
        <li><a href="#" style="color:#fff;">@lang('main.support')</a></li>
        <li><a href="/fa/sales" style="color:#fff;">@lang('main.sales')</a></li>
        <li class="active"><a href="{{asset('/fa/')}}">@lang('main.home')</a></li>
      </ul>
      <div id="bar-profile">
        <a href="#" id="open-profile">
        <span>
        کاربر
        </span>
        <img src="{{asset('asset/image/icon/user-pro.png')}}" width="38" height="38" style="position:absolute;right:-50px;top:-10px;background:#0d47a1;border:0px;padding:7px;">
        </a>
      </div>
      <div id="cartShop">
        <a href="#" id="open-cart">
        <span>
        رایگان
        </span>
        <img src="{{asset('asset/image/icon/online-shop.png')}}" width="38" height="38" style="position:absolute;right:-50px;top:-10px;background:#0d47a1;border:0px;padding:7px;">
        </a>
        <div>
          <h6>
          سبد خرید
          </h6>

          <ul>
            <hr style="margin:2px 5px;">
            <li id="p-all">قیمت کل <span>۰ تومان</span></li>
          </ul>
        </div>
      </div>
      <ul class="nav navbar-nav navbar-right">
        <!--<li><a href="#"><span class="glyphicon glyphicon-star"></span> Sign Up</a></li-->
        
      </ul>
    </div>
  </div>
</nav>
    <div class="container">
        <div class="col-md-6 col-sm-4 col-xs-12" id="secondrow">
        <div class="col-md-12 col-sm-12 col-xs-12" id="imageuploadbox">
          <h3 style="margin-top:5px;">
          @lang('advertise.imageuploadtitle')
          </h3>
          <p>
          @lang('advertise.imageuploadcontent')
          </p>
          <div class="row">
            <div class="col-md-4 col-sm-12 col-xs-4">
              <div id="image-holder" class="image-holder deactive">
                <input id="fileselector1" name="photo" type="file" style="display:none;">
                <img class="img-responsive" src="{{asset('asset/image/icon/photo.png')}}"/>
              </div>
            </div>
            <div class="col-md-4 col-sm-12 col-xs-4">
              <div id="image-holder" class="image-holder deactive">
                <input id="fileselector2" name="photo" type="file" style="display:none;">
                <img class="img-responsive" src="{{asset('asset/image/icon/photo.png')}}" />
              </div>
            </div>
            <div class="col-md-4 col-sm-12 col-xs-4">
              <div id="image-holder" class="image-holder deactive"> 
                <input id="fileselector3" name="photo" type="file" style="display:none;">
                <img class="img-responsive" src="{{asset('asset/image/icon/photo.png')}}" />
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12" style="padding:0px;">



          <div class="form-group" style="margin-bottom:5px;">
              <label for="loc">@lang('advertise.advertiselocation')</label>
              <div class="alert alert-warning" style="margin-bottom:5px;border-radius:0px;">
                <strong>@lang('advertise.warning')</strong>
                @lang('advertise.locationwarning')
              </div>
              <div class="row" id="location">
              <div class="row-height">
                <div class="col-md-8 col-sm-8 col-xs-12 col-md-height col-sm-height col-top">
                <div class="inside">
                  <div class="well well-sm inside-full-height" id="locationlist" style="border-radius:0px;">
                      <span>@lang('advertise.locationtext')</span>
                  </div>
                </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12 col-md-height col-sm-height col-top">
                <div class="inside">
                  <div class="form-group inside-full-height" style="padding:0px;margin:0px;" id="locationtoolbox">
                    <button type="button" class="btn btn-labeled btn-primary col-sm-12 col-xs-12" id="addcity" style="border-radius:0px;"><span class="btn-label"><i>+</i></span>@lang('advertise.addcity')<span class="badge">@lang('advertise.free')</span></button>
                    <button type="button" class="btn btn-labeled btn-primary col-sm-12 col-xs-12" id="addstate" style="border-radius:0px;"><span class="btn-label"><i>+</i></span>@lang('advertise.addstate')<span class="badge">1 @lang('advertise.toman')</span></button>
                    <button type="button" class="btn btn-labeled btn-primary col-sm-12 col-xs-12" id="addcountry" style="border-radius:0px;"><span class="btn-label"><i>+</i></span>@lang('advertise.addcountry')<span class="badge">5 @lang('advertise.toman')</span></button>
                    <button type="button" class="btn btn-labeled btn-primary col-sm-12 col-xs-12" id="addcontinent" style="border-radius:0px;"><span class="btn-label"><i>+</i></span>@lang('advertise.addcontinent')<span class="badge">15 @lang('advertise.toman')</span></button>
                  </div>
                </div>
                </div>
              </div>
              </div>
            </div>


        </div>
      </div>

      <div class="row" id="advertise-row">
        <div class="col-md-6 col-sm-8 col-xs-12">
          <form role="form" method="get" enctype="multipart/form-data" action="http://localhost/blog/public/postadd" id="advertise">
            <div class="form-group">
              <div class="m-group">
                <label>@lang('advertise.subject')</label> 
                <input type="text" placeholder="@lang('advertise.subjectrule')" id="subject">
                <span class="highlight"></span>
                <span class="bar" data-color="#ff0000"></span>
              </div>
              <span style="color:green;position:absolute;bottom:-9px;left:10px;background:#fff;border-radius:10px;font-size:13px;padding:0px 5px;font-family:'Lato';box-shadow:0px 0px 3px #fff;">200</span>
            </div>
            <span class="error" style="color:#ff0000;font-size:small;position:relative;top:-5px;right:10px;display:none;">@lang('advertise.subjectrule')</span>
            <div class="form-group">
              <div class="m-group">
                <label>@lang('advertise.content')</label> 
                <textarea rows="3" placeholder="@lang('advertise.contentrule')" id="content"></textarea>
                <span class="highlight"></span>
                <span class="bar"></span>
              </div>
              <span style="color:green;position:absolute;bottom:-9px;left:10px;background:#fff;border-radius:10px;font-size:13px;padding:0px 5px;font-family:'Lato';box-shadow:0px 0px 3px #fff;">3000</span>
            </div>
            <span class="error" style="color:#ff0000;font-size:small;position:relative;top:-5px;right:10px;display:none;">@lang('advertise.contentrule')</span>
            <div class="form-group">
              <button type="button" id="selectInform" class="btn btn-dialog">@lang('advertise.settinginform')<img class="img-responsive" src="{{asset('asset/image/icon/down-black.png')}}"/></button>
            </div>
            <div class="form-group">
              <label>@lang('advertise.enableticket'):</label>
              <div class="material-switch pull-left">
                <input id="ticketenable" name="ticketenable" type="checkbox" value="enable"/>
                <label for="ticketenable" class="label-primary"></label>
              </div>
            </div>
            <div class="form-group">
              <label>@lang('advertise.urgentenable'):</label>
              <div class="material-switch pull-left">
                <input id="staradvertise" name="staradvertise" type="checkbox"/>
                <label for="staradvertise" class="label-primary"></label>
              </div>
            </div>
            <div class="form-group">
              <button type="button" id="selectCategory" class="btn btn-dialog"><span>@lang('advertise.categorietoshow')</span><img class="img-responsive" src="{{asset('asset/image/icon/down-black.png')}}"/></button>
            </div>
            <div class="form-group" id="price">
                <label for="price">@lang('advertise.price'):</label>
                <div class="input-group" style="z-index:1;">
                <input value="" class="form-control" id="price" name="price" type="text" style="font-family:Lato;" placeholder="@lang('advertise.price')">
                <div class="input-group-btn">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"> <span id="moneysymbol" style="font-family:Lato;">@lang('advertise.toman')</span>
                      <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu" style="">
                        <li data-value="1"><a href="#" onclick="$('#moneysymbol').html('$');$('#moneysymbol1').html('$');$('input#moneytype').val('dollar');">@lang('advertise.dollar')</a>
                        </li>
                        <li data-value="2"><a href="#" onclick="$('#moneysymbol').html('&euro;');$('#moneysymbol1').html('&euro;');$('input#moneytype').val('euro');">@lang('advertise.euro')</a>
                        </li>
                        <li data-value="3"><a href="#" onclick="$('#moneysymbol').html('@lang('advertise.toman')');$('#moneysymbol1').html('@lang('advertise.toman')');$('input#moneytype').val('toman');">@lang('advertise.toman')</a>
                        </li>
                    </ul>
                  </div>
                </div>
            </div>
            <div class="form-group" id="trust">
                <label for="trust">@lang('advertise.trust'):</label>
                <div class="input-group" style="z-index:0;">
                <input value="" class="form-control" id="trust" name="trust" type="text" style="font-family:Lato;" placeholder="@lang('advertise.trust')">
                <div class="input-group-btn">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"> <span id="moneysymbol1" style="font-family:Lato;">@lang('advertise.toman')</span>
                      <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu" style="">
                        <li data-value="1"><a href="#" onclick="$('#moneysymbol').html('$');$('#moneysymbol1').html('$');$('input#moneytype').val('dollar');">@lang('advertise.dollar')</a>
                        </li>
                        <li data-value="2"><a href="#" onclick="$('#moneysymbol').html('&euro;');$('#moneysymbol1').html('&euro;');$('input#moneytype').val('euro');">@lang('advertise.euro')</a>
                        </li>
                        <li data-value="3"><a href="#" onclick="$('#moneysymbol').html('@lang('advertise.toman')');$('#moneysymbol1').html('@lang('advertise.toman')');$('input#moneytype').val('toman');">@lang('advertise.toman')</a>
                        </li>
                    </ul>
                  </div>
                </div>
            </div>
            <div class="form-group" id="gender">
              <label for="trust">@lang('advertise.gender'):</label>
              <label class="radio-inline"><input type="radio" name="optradio" id="malegender">@lang('advertise.male')</label>
              <label class="radio-inline"><input type="radio" name="optradio" id="femalegender">@lang('advertise.female')</label>
            </div>
            <div class="form-group">
              <button type="button" id="selectadvertisetime" class="btn btn-dialog">@lang('advertise.settime')<img class="img-responsive" src="{{asset('asset/image/icon/down-black.png')}}"/></button>
            </div>
            <input name="_token" value="{!! csrf_token() !!}"  type="hidden" />
            <input name="locationlength" id="locationlength" type="hidden" value="0" />
            <input name="category" id="categoryinput" type="hidden" value="0" />
            <input name="moneytype" id="moneytype" type="hidden" value="dollar" />
            <input name="rq" id="rq" type="hidden" value="" />
            <input name="pic1" id="pic1" type="hidden" value="0" />
            <input name="pic2" id="pic2" type="hidden" value="0" />
            <input name="pic3" id="pic3" type="hidden" value="0" />
          </form>
        </div>


      </div>
      <div style="width:50%;text-align:center;"><button class="btn btn-primary" style="font-size:large;border-radius:0px;padding:7px 60px;" id="submitmainform">@lang('advertise.submit')</button></div>
    </div>
    </div>



    <!--DEMO01-->
    <div id="StateModal" class="dialogLocation">
        <!--THIS IS IMPORTANT! to close the modal, the class name has to match the name given on the ID  class="close-animatedModal" -->
        <div class="close-animatedModal"> 
          <div class="closebt">
            <img class="" src="{{asset('asset/image/icon/error.svg')}}">
          </div>
        </div>
        <div class="modal-content">
          <div class="col-sm-12" id="row-map-container">
            <div class="row">
              <div class="col-sm-6 col-xs-12" id="list">
              </div>
              <div class="col-sm-6 col-xs-12" id="mmap">
                <img src-svg="" src-dir="{{asset('asset/image/svg/country/')}}" class="svg img-responsive" id="mapsvg" src-list="state/ir.json">
                <span><img class="spinner" src="{{asset('asset/image/icon/loader.png')}}"><span style="vertical-align:middle;">@lang('advertise.loadingmap')</span></span>
              </div>
            </div>
          </div>
        </div>
    </div>

    <div id="ContinentModal" class="dialogLocation">
        <!--THIS IS IMPORTANT! to close the modal, the class name has to match the name given on the ID  class="close-animatedModal" -->
        <div class="close-animatedModal"> 
          <div class="closebt">
            <img class="" src="{{asset('asset/image/icon/error.svg')}}">
          </div>
        </div>
        <div class="modal-content">
          <div class="col-sm-12" id="row-map-container">
            <div class="row">
              <div class="col-sm-3 col-xs-12" id="list">
              </div>
              <div class="col-sm-9 col-xs-12" id="mmap">
                <img src-svg="{{asset('asset/image/svg/world/continent.svg')}}" class="svg img-responsive" id="mapsvg" src-list="continent/all.json">
                <span><img class="spinner" src="{{asset('asset/image/icon/loader.png')}}"><span style="vertical-align:middle;">@lang('advertise.loadingmap')</span></span>
              </div>
            </div>
          </div>
        </div>
    </div>
    <div id="CountryModal" class="dialogLocation">
        <!--THIS IS IMPORTANT! to close the modal, the class name has to match the name given on the ID  class="close-animatedModal" -->
        <div class="close-animatedModal">
          <div class="closebt">
            <img class="" src="{{asset('asset/image/icon/error.svg')}}">
          </div>
        </div>
        <div class="modal-content">
          <div class="col-sm-12" id="row-map-container">
            <div class="row">
              <div class="col-sm-6 col-xs-12" id="list">
              </div>
              <div class="col-sm-6 col-xs-12" id="mmap">
                <img src-svg="" src-dir="{{asset('asset/image/svg/continents/')}}" class="svg img-responsive" id="mapsvg" src-list="">
                <span><img class="spinner" src="{{asset('asset/image/icon/loader.png')}}"><span style="vertical-align:middle;">@lang('advertise.loadingmap')</span></span>
              </div>
            </div>
          </div>
        </div>
    </div>
    <div class="gray-back"></div>
    <div id="category" class="col-md-4 col-sm-8 col-xs-10 col-md-offset-3 col-sm-offset-4 col-xs-offset-1">
      <div id="category-header">
        <h4>
          <img src="{{asset('asset/image/icon/back-red.png')}}"><span>@lang('advertise.selectcategorie'):</span>
        </h4>
        <img src="{{asset('asset/image/icon/error.png')}}" width="25" height="25">
      </div>
      <div id="category-content">
        <ul>
        </ul>
        <img class="spinner" src="{{asset('asset/image/icon/loader-black.png')}}">
      </div>
    </div>
    <div id="email-inform" class="col-md-4 col-sm-8 col-xs-10 col-md-offset-3 col-sm-offset-4 col-xs-offset-1">
    <div style="position:relative;width:100%;height:100%;">
    <h3 style="width:100%;text-align:center;">email@mail.com</h2>
      <div style="width:100%;position:absolute;bottom:0px;">
        <a href="#" target="_blank"><div class="col-md-4 col-sm-4 col-xs-4 hotmail-social"><img src="{{asset('asset/image/icon/social/hotmail.png')}}" width="40" height="40"><h6 style="color:#fff;font-family:'Lato';font-weight:300;margin:2px;">Hotmail</h6></div></a>
        <a href="#" target="_blank"><div class="col-md-4 col-sm-4 col-xs-4 gmail-social"><img src="{{asset('asset/image/icon/social/gmail.png')}}" width="40" height="40"><h6 style="color:#fff;font-family:'Lato';font-weight:300;margin:2px;">Gmail</h6></div></a>
        <a href="#" target="_blank"><div class="col-md-4 col-sm-4 col-xs-4 yahoo-social"><img src="{{asset('asset/image/icon/social/yahoo.png')}}" width="40" height="40"><h6 style="color:#fff;font-family:'Lato';font-weight:300;margin:2px;">Yahoo</h6></div></a>
      </div>
    </div>
    </div>
    <div id="telegram-inform" class="col-md-4 col-sm-8 col-xs-10 col-md-offset-3 col-sm-offset-4 col-xs-offset-1">
      <div style="position:relative;width:100%;height:100%;">
        <img src="{{asset('asset/image/icon/social/telegram-logo.png')}}" width="80" style="display: block;margin:0 auto;">
        <h3 style="width:100%;text-align:center;">@Username</h3>
        <a class="btn" target="_blank" href="https://telegram.me/user">@lang('advertise.sendmsg')</a>
      </div>
    </div>
    <div id="inform" class="col-md-4 col-sm-8 col-xs-10 col-md-offset-3 col-sm-offset-4 col-xs-offset-1">
      <img src="{{asset('asset/image/icon/error.png')}}" width="25" height="25">
      <div class="cont">
      <div class="form-group">
        <div class="m-group">
          <label>@lang('advertise.phone')</label> 
          <input type="text" placeholder="+00123456789" id="phone" name="phone" style="direction:ltr;">
          <span class="highlight"></span>
          <span class="bar"></span>
        </div>
      </div>
      <span class="error" style="color:#ff0000;font-size:small;position:relative;right:10px;display:none;">@lang('advertise.phonerule')</span>
      <div class="form-group">
        <div class="m-group">
          <label>@lang('advertise.email')</label> 
          <input type="text" placeholder="email@example.com" id="email" name="email" style="direction:ltr;">
          <span class="highlight"></span>
          <span class="bar"></span>
        </div>
      </div>
      <span class="error" style="color:#ff0000;font-size:small;position:relative;right:10px;display:none;">@lang('advertise.emailrule')</span>
      <div class="form-group">
        <div class="m-group">
          <label>@lang('advertise.telusername')</label> 
          <input type="text" placeholder="@telegram_username" id="telegram" name="telegram" style="direction:ltr;">
          <span class="highlight"></span>
          <span class="bar"></span>
        </div>
      </div>
      <span class="error" style="color:#ff0000;font-size:small;position:relative;right:10px;display:none;">@lang('advertise.telrule')</span>
      <div style="width:100%;text-align:center;font-size:large;margin-top:10px;"><button class="btn btn-primary" style="padding:7px 30px;">@lang('advertise.submit')</button></div>
      </div>
    </div>
    
    <div id="advertise-time" class="col-md-4 col-sm-8 col-xs-10 col-md-offset-3 col-sm-offset-4 col-xs-offset-1">
      <img src="{{asset('asset/image/icon/error.png')}}" width="25" height="25">
      <div class="cont">
      <div class="radio">
        <label><input type="radio" name="timeadvertise" value="default" checked="checked">@lang('advertise.defaulttime')</label>
      </div>
      <div class="radio">
        <label><input type="radio" name="timeadvertise" value="custom">@lang('advertise.timecustom')</label>
      </div>
      <div class="form-group">
      <label for="email">@lang('advertise.timeenterdaynum'):</label>
      <div class = "input-group">
         <input type = "text" class = "form-control" id="numday" name="numday">
         <span class = "input-group-addon" style="font-family:Lato;border-radius:5px 0px 0px 5px;border-right:0;border-left:1px #ccc solid;">@lang('advertise.price') : 0$</span>
      </div>
      </div>
      <div class="radio">
        <label><input type="radio" name="timeadvertise" value="pack">@lang('advertise.timepack')</label>
      </div>
      <div class="form-group">
      <label for="sel1">@lang('advertise.timeselectpack'):</label>
      <select class="form-control" id="timepack">
        <option value="golden">@lang('advertise.timegolden')</option>
        <option value="silver">@lang('advertise.timesilver')</option>
      </select>
      </div>
      <div style="width:100%;text-align:center;font-size:large;"><button class="btn btn-primary" style="padding:7px 30px;">@lang('advertise.submit')</button></div>
      </div>
    </div>



    <div id="login" style="overflow:scroll;">
      <div class="close-animatedModal">
        <div class="closebt">
            <img class="" src="{{asset('asset/image/icon/error.svg')}}">
        </div>
      </div>
      <div id="cont">
        <div class="row" style="width:100%;margin:0px;display:block;">
          <div class="col-md-4 col-sm-6 col-xs-10" style="min-width:250px;max-width:400px;margin:0 auto;display:block;position:absolute;left:0px;right:0px;top:20px;">
            <h1 style="width:100%;text-align:center;">@lang('advertise.dahlia')</h1>
            <div style="width:100%;box-shadow:0px 1px 2px #808080;background:#fff;height:100%;padding:30px 20px 0px 20px;border-top:3px #297dce solid;overflow:hidden;margin-bottom:30px;position:relative;">
            <div id="timer" style="position:absolute;top:0px;left:0px;right:0px;margin:0 auto;width:60px;display:none;"><h4 style="background:#297dce;padding:0px 0px 5px 0px;border-radius:0px 0px 10px 10px;color:#fff;margin:0px;text-align:center;">1:30</h4></div>
              <img src="{{asset('asset/image/icon/social/user.png')}}" class="img-responsive user" width="100">
              <div id="welcome-header" style="text-align:center;display:none;">
                <h4>@lang('advertise.loginwelcome') <b>alimohammadi340@gmail.com</b>!</h4>
              </div>
              <div class="receivemail">
                @lang('advertise.loginsendverify')<br><b>+989162886638</b>
                <img src="{{asset('asset/image/icon/smartphone.png')}}" class="img-responsive" width="55">
              </div>
              <div class="setpass" style="text-align:center;display:none;">
              @lang('advertise.loginthanks')
              </div>
              <div class="form-group">
                <div class="m-group">
                <!--label>Subject</label--> 
                <input type="text" placeholder="@lang('advertise.enterphone')" id="phone" style="direction:ltr;">
                <span class="highlight"></span>
                <span class="bar" data-color="#ff0000"></span>
                </div>
              </div>
              <span class="error" style="color:#ff0000;font-size:small;position:relative;top:-5px;right:10px;display:none;">@lang('advertise.phonerule')</span>
              <div class="form-group" style="display:none;">
                <div class="m-group">
                <!--label>Subject</label--> 
                <input type="text" placeholder="@lang('advertise.entercode')" id="code" style="text-align:center;">
                <span class="highlight"></span>
                <span class="bar" data-color="#ff0000"></span>
                </div>
              </div>
              <span class="error" style="color:#ff0000;font-size:small;position:relative;top:-5px;right:10px;display:none;">@lang('advertise.coderule')</span>
              <div class="form-group" style="display:none;">
                <div class="m-group">
                <!--label>Subject</label--> 
                <input type="password" placeholder="Enter password" id="password" style="text-align:left;">
                <span class="highlight"></span>
                <span class="bar" data-color="#ff0000"></span>
                </div>
              </div>
              <div class="form-group" style="display:none;">
                <div class="m-group">
                <!--label>Subject</label--> 
                <input type="password" placeholder="Enter password again" id="passwordverify" style="text-align:left;">
                <span class="highlight"></span>
                <span class="bar" data-color="#ff0000"></span>
                </div>
              </div>
              <button class="btn btn-primary btn-login" id="login-next">@lang('advertise.next')</button>
            </div>
          </div>
        </div>
      </div>
    </div>



    <div id="preview" style="overflow:scroll;">
      <div class="close-animatedModal">
        <div class="closebt">
            <img class="" src="{{asset('asset/image/icon/error.svg')}}">
        </div>
      </div>
      <div id="cont" class="col-md-12 col-sm-12 col-xs-12">
        <div class="row" style="padding: 0px  1.5% 0px 1.5%;"><div class="col-md-12 col-sm-12 col-xs-12" style="height:50px;background:#297dce;margin:0px 0px 10px 0px;">
          <h2 style="color:#fff;padding:12px 5px;margin:0px;font-size:16px;" id="preview-category">
            Electronics > Moiles > Samsung
          </h2>
        </div></div>
        <div class="row">
          <div class="col-md-4 col-md-offset-0 col-sm-offset-0 col-sm-4 col-xs-12">
            <div id="profilebox" class="col-md-12 col-sm-12 col-xs-12">
            <center>
              <img src="{{asset('asset/image/icon/social/user.png')}}" class="img-responsive" width="90">
              <h3>@lang('advertise.ownerinform')</h3>
              <button type="button" class="btn btn-labeled btn-primary col-sm-12 col-xs-12" style="border-radius:0px;position:relative;" id="prev-get-phone"><span class="btn-label"><img src="{{asset('asset/image/icon/call.png')}}"></span><span id="prev-get-phone-span">@lang('advertise.call')</span></button>
              <button type="button" class="btn btn-labeled btn-primary col-sm-12 col-xs-12" style="border-radius:0px;position:relative;" id="prev-get-mail"><span class="btn-label"><img src="{{asset('asset/image/icon/email.png')}}"></span>@lang('advertise.sendmail')</button>
              <button type="button" class="btn btn-labeled btn-primary col-sm-12 col-xs-12" style="border-radius:0px;position:relative;" id="prev-get-telegram"><span class="btn-label"><img src="{{asset('asset/image/icon/social/telegram.png')}}" width="24" height="24"></span>@lang('advertise.telusername')</button>
              <button type="button" class="btn btn-labeled btn-primary col-sm-12 col-xs-12" style="border-radius:0px;position:relative;"><span class="btn-label"><img src="{{asset('asset/image/icon/mail.png')}}"></span>@lang('advertise.sendticket')</button>
            </center>
            </div>
            <div id="confirm-box">
            <hr style="height:2px;">
              <div class="form-group">
                <label style="display:inline;">@lang('advertise.enableedit')</label>
                <div class="material-switch pull-left">
                  <input id="ticketenable3" name="someSwitchOption003" type="checkbox"/>
                  <label for="ticketenable3" class="label-primary"></label>
                </div>
              </div>
              <br>
              <div class="form-group">
                <button class="btn btn-success col-md-12 col-sm-12 col-xs-12" id="confirm-prev">@lang('advertise.confirm')</button>
              </div>
            </div>
          </div>


          <div class="col-md-8 col-sm-8 col-xs-12" id="preview-main">
            <div class="main" style="background:#fff;padding:3px;direction:ltr;" id="preview-gallery">
              <ul id="imageGallery" style="display:block;">
                <!--li data-thumb="{{asset("asset/image/2.jpg")}}" data-src="{{asset("asset/image/2.jpg")}}">
                  <img src="{{asset("asset/image/2.jpg")}}" height="300" style="display:block;margin:0px auto;"/>
                </li>
                <li data-thumb="{{asset("asset/image/3.jpg")}}" data-src="{{asset("asset/image/3.jpg")}}">
                  <img src="{{asset("asset/image/3.jpg")}}" height="300" style="display:block;margin:0px auto;"/>
                </li>
                <li data-thumb="{{asset("asset/image/4.jpg")}}" data-src="{{asset("asset/image/4.jpg")}}">
                  <img src="{{asset("asset/image/4.jpg")}}" height="300" style="display:block;margin:0px auto;"/>
                </li-->
              </ul>
            </div>
          

          <div style="background:#fff;" id="preview-content">
            <h1 style="padding:3px 5px;margin-top:0px;">
            <b>
            the titles
            </b>
            </h1>
            <p style="padding:3px 10px;">the text texttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttext</p>
        </div>
        <div style="background:#fff;padding:10px 30px;margin-bottom:10px;" id="preview-req">
          <div class="row" id="preview-price">
            <span style="color:#297dce;float:right;">@lang('advertise.price')</span>
            <span style="color:#13126E;float:left;" class="val">65511$</span>
          </div>
          <hr style="border-color:#cacaca;margin:5px;">
          <div class="row" id="preview-trust">
            <span style="color:#297dce;float:right;">@lang('advertise.trust')</span>
            <span style="color:#13126E;float:left;" class="val">65511$</span>
          </div>
        </div>
          </div>



        </div>
      </div>
    </div>

</body>
</html>