<?php if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) ob_start('ob_gzhandler'); else ob_start(); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <title>Dahlia</title>
    <style type="text/css">
          @font-face {
  font-family: 'Lato';
  font-style: normal;
  font-weight: 400;
  src: local('Lato Regular'), local('Lato-Regular'), url({{asset('asset/font/UyBMtLsHKBKXelqf4x7VRQ.woff2')}}) format('woff2');
  unicode-range: U+0100-024F, U+1E00-1EFF, U+20A0-20AB, U+20AD-20CF, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Lato';
  font-style: normal;
  font-weight: 400;
  src: local('Lato Regular'), local('Lato-Regular'), url({{asset('asset/font/1YwB1sO8YE1Lyjf12WNiUA.woff2')}}) format('woff2');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215, U+E0FF, U+EFFD, U+F000;
}
/* latin-ext */
@font-face {
  font-family: 'Yanone Kaffeesatz';
  font-style: normal;
  font-weight: 400;
  src: local('Yanone Kaffeesatz Regular'), local('YanoneKaffeesatz-Regular'), url({{asset('asset/font/YDAoLskQQ5MOAgvHUQCcLV83L2yn_om9bG0a6EHWBso.woff2')}}) format('woff2');
  unicode-range: U+0100-024F, U+1E00-1EFF, U+20A0-20AB, U+20AD-20CF, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Yanone Kaffeesatz';
  font-style: normal;
  font-weight: 400;
  src: local('Yanone Kaffeesatz Regular'), local('YanoneKaffeesatz-Regular'), url({{asset('asset/font/YDAoLskQQ5MOAgvHUQCcLfGwxTS8d1Q9KiDNCMKLFUM.woff2')}}) format('woff2');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215, U+E0FF, U+EFFD, U+F000;
}

*{
  font-family: 'Yanone Kaffeesatz', sans-serif;
  font-weight: 100;
  letter-spacing: 1px;
}
  nav *{
  color: #fff;
}
body {
  padding-top: 50px;
  font-family: 'Yanone Kaffeesatz', sans-serif;
  font-weight: 100;
  letter-spacing: 1px;
  font-size: large;
  background: #ffffff;

}
span,label,div,input,button,textarea{
  letter-spacing: 1px;
  font-family: 'Yanone Kaffeesatz', sans-serif;
  font-weight: 100;
  font-size:large;

}
  div#login{
    position: fixed;
    z-index: 3001;
    background: #eaeaea;
    width: 100%;
    height: 100%;
    top: 0; left: 0;
    display: block;
    transition: 0.3s;
    overflow: hidden;
  }
  div#login .close-animatedModal{
    padding-top: 10px;
    margin: 0px;
  }
  div#login .closebt{
    padding: 0px;
    margin: 0px auto;
  }
  div#login > #cont{
    position: relative;
    padding: 10px  0% 0px 0%;
  }
  div#login img.user{
    display:block;
    margin:0px auto 30px auto;
  }
  div#login .btn-login{
    margin: 15px 0px;
    text-align: center;
    width:100%;
    background: #297dce;
    border-color:#297dce;
    border-radius: 2px;
    font-size: large;
  }
  div#login .btn-login:hover{
    background: #2676C3;
    border-color:#2676C3;
  }
  div#login .m-group{ 
    margin-top: 10px;
  }
  div#login input#code{
    text-align: center;
  }
  div#login .receivemail{
    position: relative;
    text-align: left;
    vertical-align: middle;
    display: none;
  }
  div#login .receivemail img{
    position: absolute;
    top:0px;
    right:0px;
  }
  #prev-get-phone-span{
    font-family: Lato;
  }
  .form-group{
  position: relative;
  margin-bottom: 3px;
}


.m-group input         {
  padding:6px 5px 6px 10px;
  display:block;
  width:100%;
  border:none;
  border-bottom:1px solid #757575;
  background: transparent;
}
.m-group input:focus     { outline:none; }


.m-group textarea         {
  padding:8px 5px 8px 10px;
  display:block;
  width:100%;
  border:none;
  border-bottom:1px solid #757575;
  background: transparent;
}
.m-group textarea:focus     { outline:none; }
/* LABEL ======================================= */
.m-group label          {
  color:#5264AE; 
  font-weight:normal;
  position:absolute;
  pointer-events:none;
  left:0px;
  top:-19px;
}


/* BOTTOM BARS ================================= */
.m-group .bar  { position:relative; display:block; width:100%; }
.m-group .bar:before,.m-group .bar:after   {
  content:'';
  height:2px; 
  width:0;
  bottom:0px; 
  position:absolute;
  background:#297dce;
  transition:0.5s ease all; 
  -moz-transition:0.5s ease all; 
  -webkit-transition:0.5s ease all;
}
.m-group .bar:before {
  left:50%;
}
.m-group .bar:after {
  right:50%; 
}

/* active state */
.m-group input:focus ~ .bar:before,.m-group input:focus ~ .bar:after {
  width:50%;
}
.m-group textarea:focus ~ .bar:before,.m-group textarea:focus ~ .bar:after {
  width:50%;
}
    </style>
  <script type="text/javascript">
  var baseUrl = "{{substr(asset(''), 0, strlen(asset('')) - 1)}}";
  var timer;
  var times = 90;
  var slider = null;
  var isSliderLoaded = false;
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-80283379-1']);
_gaq.push(['_setDomainName', 'none']);
_gaq.push(['_setAllowLinker', 'true']);
_gaq.push(['_trackPageview']);

(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>
  <link rel="stylesheet" href="{{asset('asset/css/bootstrap.min.css')}}">
  </head>
  <body>

<nav class="navbar navbar-inverse navbar-fixed-top" style="background:#297dce;border-color:#297dce;box-shadow: 0px 0px 3px #000;">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#" style="color:#fff;">Dahlia</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="{{asset('')}}">Home</a></li>
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="color:#fff;">Main page<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Profile</a></li>
            <li><a href="#">Profile</a></li>
            <li><a href="#">Support</a></li>
          </ul>
        </li>
        <li><a href="#" style="color:#fff;">Tickets</a></li>
        <li><a href="#" style="color:#fff;">Support</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-star"></span> Sign Up</a></li>
        <li><a href="#"><span class="glyphicon glyphicon-plus"></span> Login</a></li>
      </ul>
    </div>
  </div>
</nav>
    <div class="container">



      <div id="login" style="overflow:scroll;">
      <div class="close-animatedModal">
        <div class="closebt">
        </div>
      </div>
      <div id="cont">
        <div class="row" style="width:100%;margin:0px;display:block;">
          <div class="col-md-4 col-sm-6 col-xs-10" style="min-width:250px;max-width:400px;margin:0 auto;display:block;position:absolute;left:0px;right:0px;top:20px;">
            <h1 style="width:100%;text-align:center;">Dahlia</h1>
            <div style="width:100%;box-shadow:0px 1px 2px #808080;background:#fff;height:100%;padding:30px 20px 0px 20px;border-top:3px #297dce solid;overflow:hidden;margin-bottom:30px;position:relative;">
            <img src="{{asset('asset/image/icon/social/user.png')}}" class="img-responsive user" width="100">
              <div class="setpass" style="text-align:center;display:block;font-size:large;">
                please enter a password for tour account
              </div>
              <div class="form-group" style="display:block;">
                <div class="m-group">
                <!--label>Subject</label--> 
                <input type="password" placeholder="Enter password" id="password">
                <span class="highlight"></span>
                <span class="bar" data-color="#ff0000"></span>
                </div>
              </div>
              <div class="form-group" style="display:block;">
                <div class="m-group">
                <!--label>Subject</label--> 
                <input type="password" placeholder="Enter password again" id="passwordverify">
                <span class="highlight"></span>
                <span class="bar" data-color="#ff0000"></span>
                </div>
              </div>
              <button class="btn btn-primary btn-login" id="login-next">Submit</button>
            </div>
          </div>
        </div>
      </div>
    </div>



    </div>

  </body>
  <!--<link href="https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Lato:200?text=123456789$" rel="stylesheet"> -->
</html>

      <?php
        ob_end_flush();
      ?>