
<?php if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) ob_start('ob_gzhandler'); else ob_start(); 
use Session As Session;
if(isset($_GET['pid'])){
  Session::put("room",$_GET['pid']);
}else{
  Session::put("room","0");
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--meta http-equiv="refresh" content="1"-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <title>Dahlia - Sales Room</title>
  <script type="text/javascript">
  var timer;
  var times = 30;
  var slider = null;
  var isSliderLoaded = false;
/*var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-80283379-1']);
_gaq.push(['_setDomainName', 'none']);
_gaq.push(['_setAllowLinker', 'true']);
_gaq.push(['_trackPageview']);

(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();*/
</script>
  <link rel="stylesheet" href="{{asset('asset/css/bootstrap.min.css')}}">
  <script src="{{asset('asset/js/jquery.min.js')}}"></script>
  <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>-->
  <script src="{{asset('asset/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('asset/js/sweetalert2.min.js')}}"></script>
  <link rel="stylesheet" href="{{asset('asset/css/animate.css')}}">
  <!--<script src="//oss.maxcdn.com/jquery.form/3.50/jquery.form.min.js"></script>-->
  <link rel="stylesheet" href="{{asset('asset/css/sweetalert2.min.css')}}">
  <link rel="stylesheet" href="{{asset('asset/css/lightslider.min.css')}}">
  <script type="text/javascript" src="{{asset('asset/js/lightslider.min.js')}}"></script>
  <!--<script src="{{asset('asset/js/dialogbox.js')}}"></script>-->
  <script type="text/javascript" src="{{asset('asset/js/typed.js')}}"></script>
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/flaticon.css')}}"> 
  <!--script type="text/javascript" src="{{asset('asset/js/jquery.timer.js')}}"></script-->
  <!--link rel="stylesheet" href="{{asset('asset/css/main.css')}}"-->
  <!--link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css"-->
  <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet"> 
<!-- Latest compiled and minified JavaScript -->
<script type="text/javascript" src="{{asset('asset/js/jquery.mask.min.js')}}"></script>
<script type="text/javascript" src="{{asset('asset/js/jquery.slimscroll.min.js')}}"></script>
  <style type="text/css">



  @font-face {
  font-family: 'Lato';
  font-style: normal;
  font-weight: 400;
  src: local('Lato Regular'), local('Lato-Regular'), url({{asset('')}}/asset/font/UyBMtLsHKBKXelqf4x7VRQ.woff2) format('woff2');
  unicode-range: U+0100-024F, U+1E00-1EFF, U+20A0-20AB, U+20AD-20CF, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Lato';
  font-style: normal;
  font-weight: 400;
  src: local('Lato Regular'), local('Lato-Regular'), url({{asset('')}}asset/font/1YwB1sO8YE1Lyjf12WNiUA.woff2) format('woff2');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215, U+E0FF, U+EFFD, U+F000;
}
/* latin-ext */
@font-face {
  font-family: 'Yanone Kaffeesatz';
  font-style: normal;
  font-weight: 400;
  src: local('Yanone Kaffeesatz Regular'), local('YanoneKaffeesatz-Regular'), url({{asset('')}}asset/font/YDAoLskQQ5MOAgvHUQCcLV83L2yn_om9bG0a6EHWBso.woff2) format('woff2');
  unicode-range: U+0100-024F, U+1E00-1EFF, U+20A0-20AB, U+20AD-20CF, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Yanone Kaffeesatz';
  font-style: normal;
  font-weight: 400;
  src: local('Yanone Kaffeesatz Regular'), local('YanoneKaffeesatz-Regular'), url({{asset('')}}asset/font/YDAoLskQQ5MOAgvHUQCcLfGwxTS8d1Q9KiDNCMKLFUM.woff2) format('woff2');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215, U+E0FF, U+EFFD, U+F000;
}
  button,input,select,.form-control,*,.btn{
    border-radius: 0px;
    letter-spacing: 1px;
  font-family: 'Yanone Kaffeesatz', sans-serif;
  }
  nav{
    box-shadow: 0px 0px 3px #000;
    letter-spacing: 1px;
  font-family: 'Yanone Kaffeesatz', sans-serif;
  }
  body{
    width: 100%;
    height: 100%;
    padding: 0px;
    margin: 0px;
    overflow-x: hidden;
    letter-spacing: 1px;
  font-family: 'Yanone Kaffeesatz', sans-serif;
  font-size: large;
  background: #f0f0f0;
  position: absolute;
  }
    .row{
      margin: 0px;
      padding: 0px;
    }
    #main{
      width:75%;
      position:absolute;
      top:0;
      left:25%;
      bottom:0;
      background:url({{asset("asset/image/background/salesroom-2-min.png")}});
      background-size: 100% 100%;
      background-repeat: no-repeat;
      z-index:900;
    }
    @media (max-width: 700px) {
      #users{
        display: none;
      }
      #main{
        left:0px;
        width:100%;
      }
    }
    #floataction{
      position:fixed;bottom:30px;right:30px;background:#297dce;border-radius:30px;width:60px;height:60px;
      transition: 0.3s;
      box-shadow: 0px 0px 8px #202020;
    }
    #floataction:hover{
      background:#1D5C98;
    }
    .container{
      width:100%;
      height: 100%;
      margin:0 auto;
      margin:0px;
      padding: 50px 0px 0px 0px;
    }
    .btn-login{
      background:#297dce;
      display:block;
      width:100%;
      text-align:center;
      transition: 0.3s;
    }
    .btn-login:hover{
    background: #226CB3;
    border-color:#226CB3;
    }
    #child{
      transition: 0.3s;
      position: relative;
      margin-right: 0px;
      padding-right: 0px;
      padding: 0px;
      margin-bottom: 10px;
      /*margin-bottom:5px;
      border-top: 2px #297dce solid;
      border-bottom:1px #cacaca solid;*/
    }
    #child .row{
      position:relative;width:98%;height:460px;
      background: #fff;
      transition: 0.5s;
      overflow-x: hidden;
      box-shadow:0px 1px 2px #808080;
    }
    #child:hover .row{
      /*border-bottom: 2px #297dce solid;*/
    }
    #child #pic{
      transition: 0.3s;
    }
    #child #prop{
      padding-left: 5px;
      position: relative;
      overflow: hidden;
    }
    #child #prop h3{
      color:#297dce;margin-top:10px;margin-bottom:0px;transition: 0.3s;
    }
    #child:hover #prop h3{
      color:#FA7B05;
    }
    #child:hover #pic{
      -webkit-filter: contrast(120%); /* Chrome, Safari, Opera */
      filter: contrast(120%);
    }
    #child:hover{
      border-color:#297dce;
    }
    .btn-order{
      margin:auto -3px;
      display: inline-block;
      transition: 0.3s;
      border-color: #eaeaea;
    }
    .btn-order:hover{
      background: #eaeaea;
      border-color: #eaeaea;
    }
    .btn-order-active{
      background: #297dce;
      color: #fff;
      border-color: #297dce;
      padding:5px 2px 4px 2px;
    }
    .btn-order-active:hover{
      background: #297dce;
      color: #fff;
      border-color: #297dce;
    }
    #order-box button:first-child{
      border-radius: 5px 0px 0px 5px;
    }
    #order-box button:last-child{
      border-radius: 0px 5px 5px 0px;
    }
    .bottom-bar-input{
      position: absolute;
      bottom: 0px;
      left: 0px;
      width:100%;
      border-top: 1px #d0d0d0 solid;
      z-index: 1001;
    }
    .price-header{
      position: absolute;
      top: 4px;
      margin: 0 auto;
      left:0px;
      right:0px;
      width:100%;
      text-align: center;
    }
    #content{
      position: absolute;
      top: 45px;
      margin: 0 auto;
      left:0px;
      right:0px;
      width:95%;
      padding: 0px 5px;
      background: rgba(256 ,256 ,256 ,0.8);
      box-shadow:0px 1px 10px #555;
      border-top: 2px #297dce solid;
      border-radius: 0px 0px 2px 2px;
      color: #000;
    }
    .price-header h3{
      margin:0px;
      padding: 10px 40px;
      text-align: center;
      width: 100%;
      display: inline;
      background: #fff;
      border-radius: 0px 0px 8px 8px;
      /*box-shadow: 0px 0px 3px blue;
      border-bottom: 2px #297dce solid;*/
      box-shadow: 0px 1px 8px #297dce;
    }
    .price-input{
      border: 0px;
      width: 100%;
      padding: 8px 30px;
      font-size: large;
      color: #303030;
      font-family: 'Ubuntu', sans-serif;
      vertical-align: middle;
      outline: 0;
    }
    .send{
      position: absolute;
      top: 0px;
      right: 0px;
      bottom: 0px;
      border: 0px;
      background: #fff;
      font-size: larger;
      color: #297dce;
      padding: 3px 20px;
      vertical-align: middle;
      display: block;
      transition: 0.3s;
    }
    .send:hover{
      /*color:#FA7B05;*/
      background: #f0f0f0;
    }
    .send:focus{
      color:#FA7B05;
      /*background: #f0f0f0;*/
    }
    .user-row{
      width:100%;
      position: relative;
      padding:3px 10px;
      transition:0.3s;
      cursor:pointer;
    }
    .user-row:hover{
      background:#eaeaea;
    }
    .user-row .profile-pic{
      width:50px;
      height: 50px;
      display: inline-block;
      background: #ff9800;
      border-radius: 50%;
      position: relative;
      top:2px;
    }
    .user-row:nth-child(18n + 1) .profile-pic{background:#ff9800;}
    .user-row:nth-child(18n + 2) .profile-pic{background:#f44336;}
    .user-row:nth-child(18n + 3) .profile-pic{background:#673ab7;}
    .user-row:nth-child(18n + 4) .profile-pic{background:#03a9f4;}
    .user-row:nth-child(18n + 5) .profile-pic{background:#4caf50;}
    .user-row:nth-child(18n + 6) .profile-pic{background:#ffeb3b;}
    .user-row:nth-child(18n + 7) .profile-pic{background:#ff5722;}
    .user-row:nth-child(18n + 8) .profile-pic{background:#e91e63;}
    .user-row:nth-child(18n + 9) .profile-pic{background:#3f51b5;}
    .user-row:nth-child(18n + 10) .profile-pic{background:#00bcd4;}
    .user-row:nth-child(18n + 11) .profile-pic{background:#8bc34a;}
    .user-row:nth-child(18n + 12) .profile-pic{background:#ffc107;}
    .user-row:nth-child(18n + 13) .profile-pic{background:#795548;}
    .user-row:nth-child(18n + 14) .profile-pic{background:#9c27b0;}
    .user-row:nth-child(18n + 15) .profile-pic{background:#2196f3;}
    .user-row:nth-child(18n + 16) .profile-pic{background:#009688;}
    .user-row:nth-child(18n + 17) .profile-pic{background:#cddc39;}
    .user-row .profile-pic h2{
      position: absolute;
      top: 50%;
      transform: translateY(-50%);
      color: #fff;
      padding: 0px;
      margin: 0px;
      left: 0px;
      right: 0px;
      text-align: center;
    }
    .user-row .profile-info{
      position: absolute;
      top: 50%;
      right:0px;
      left:80px;
      transform: translateY(-50%);
    }
    .user-row .profile-info h3{
      margin: 0px;
    }
    .user-row .profile-info span{
      position: relative;
      display: inline-block;
      padding: 0px 5px;
      font-size: small;
      color: #297dce;
      font-weight: bold;
    }
    #content-content{
      position: relative;
      padding: 5px 10px;
    }
    span#extra{
      padding:2px 20px;
      color:#fff;
      position:absolute;
      top:2px;
      right:10px;
      background:#297dce;
      border-radius:5px;
      transition: 0.3s;
      cursor: pointer;
    }
    span#extra:hover{
      background: #ff9800;
    }
    .gray-box{
      position: fixed;
      display: block;
      content: ' ';
      width: 100%;
      height: 100%;
      top:0px;
      left:0px;
      background: rgba(0,0,0,0.6);
      z-index: 3000;
      display: none;
    }
    #attachment{
      z-index: 3500;
      position: fixed;
      top: 50%;
      width: 100%;
      transform: translateY(-50%);
      display: none;
    }
    #chat{
      z-index: 3500;
      position: fixed;
      top: 50%;
      width: 100%;
      transform: translateY(-50%);
      display: none;
    }
    #chat > div{
      background: #fff;
      padding: 0px;
      /*box-shadow: 0px 0px 1px #eaeaea;*/
    }
    #attachment > div{
      background: #fff;
      /*box-shadow: 0px 0px 1px #eaeaea;*/
      padding: 5px 10px;
      border-top: 3px #297dce solid;
    }
    .dialog > div > img{
      position: absolute;
      top: -12px;
      right: -10px;
      cursor: pointer;
    }
    .chat-content{
      padding: 2px 0px;
    }
    .chat-content > div{
      width:100%;
      padding: 2px 10px;
      position: relative;
      display: block;
    }
    .chat-content > div.me{
      float: right;
    }
    .chat-content > div.he{
      float: left;
    }
    .chat-content > div.me > span{
      display: block;
      padding: 5px 10px;
      background: #90caf9;
      max-width: 60%;
      float: right;
      border-radius: 2px;
      position: relative;
      border-bottom: 2px #2196f3 solid;
    }
    .chat-content > div.he > span{
      display: block;
      padding: 5px 10px;
      background: #eeeeee;
      max-width: 60%;
      float: left;
      border-radius: 2px;
      position: relative;
      border-bottom: 2px #9e9e9e solid;
    }
    .chat-content > div.me.alone > span:before{
      display: block;
      content: ' ';
      position: absolute;
      right: -5px;
      top: 4px;
      width: 0; 
      height: 0;
      border-top: 5px solid transparent;
      border-bottom: 5px solid transparent;
      border-left: 5px solid #90caf9;
    }
    .chat-content > div.he.alone > span:before{
      display: block;
      content: ' ';
      position: absolute;
      left: -5px;
      top: 4px;
      width: 0; 
      height: 0;
      border-top: 5px solid transparent;
      border-bottom: 5px solid transparent;
      border-right: 5px solid #eeeeee;
    }
  </style>
  </head>
  <body>
<div class="gray-box">
  
</div>
<div id="attachment" class="dialog">
  <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12 col-xs-offset-0">
  <img src="{{asset('asset/image/icon/error.png')}}" width="25" height="25">
     <ul id="imageGallery1" style="display:block;width:100%;">
                <li data-thumb="{{asset("asset/image/2.jpg")}}" data-src="{{asset("asset/image/2.jpg")}}">
                  <img src="{{asset("asset/image/2.jpg")}}" height="250" style="display:block;margin:0px auto;"/>
                </li>
                <li data-thumb="{{asset("asset/image/3.jpg")}}" data-src="{{asset("asset/image/3.jpg")}}">
                  <img src="{{asset("asset/image/3.jpg")}}" height="300" style="display:block;margin:0px auto;"/>
                </li>
                <li data-thumb="{{asset("asset/image/4.jpg")}}" data-src="{{asset("asset/image/4.jpg")}}">
                  <img src="{{asset("asset/image/4.jpg")}}" height="250" style="display:block;margin:0px auto;"/>
                </li>
                </ul>

  </div>
</div>

<div id="chat" class="dialog">
  <div class="col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2 col-xs-12 col-xs-offset-0">
    <img src="{{asset('asset/image/icon/error.png')}}" width="25" height="25">
    <div class="chat-box" style="overflow:hidden;">
      <div class="chat-header">
        <span style="background:#297dce;font-size:larger;display:block;width:100%;color:#fff;height:46px;padding:3px 0px;">
          <span style="padding:0px 10px;vertical-align:middle;">Chat with seller</span>
        </span>
      </div>
      <div class="chat-content" style="height:350px;background:url({{asset("asset/image/background/salesroom-1.jpeg")}});;width:100%;">
        <div class="me alone">
          <span>Hello i have a question</span>
        </div>
        <div class="me">
          <span>Have you any time to answer me?</span>
        </div>
        <div class="he alone">
          <span>Yeah of course!</span>
        </div>
        <div class="he">
          <span>Tell me your question</span>
        </div>
        <div class="he">
          <span>I'll become happy to help you!</span>
        </div>
        <div class="me alone">
          <span>Just i wanna know the price :-/</span>
        </div>
      </div>
      <div class="chat-footer" style="background:#eaeaea;box-shadow:0px -1px 1px #297dce;position:relative;">
        <input type="text" name="msg" placeholder="Enter text" style="border:none;outline:0;padding:7px 10px;background:#eaeaea;width:100%;">
        <button type="button" style="position:absolute;right:0px;top:0px;background:#297dce;padding:8px 20px;border:none;outline:0;color:#fff;">Send</button>
      </div>
    </div>
  </div>
</div>
<nav class="navbar navbar-inverse navbar-fixed-top" style="background:#297dce;border-color:#297dce;width:100%;margin:0px;">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#" style="color:#fff;">Dahlia</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href="#" style="color:#fff;">Home</a></li>
        <li><a href="#" style="color:#fff;position:relative;">Chat With Seller
        <span class="badge" style="background:#f00;font-family:'Ubuntu';position:absolute;top:4px;right:3px;">5</span>
        </a></li>
      </ul>
      <span style="vertical-align:center;color:#fff;margin:5px 10px;position:relative;top:12px;">Active Users (10)
      </span>
      <ul class="nav navbar-nav navbar-right">
        <!--<li><a href="#"><span class="glyphicon glyphicon-star"></span> Sign Up</a></li>
        <li><a href="#"><span class="glyphicon glyphicon-plus"></span> Login</a></li>-->
      </ul>
    </div>
  </div>
</nav>
    <div class="container">
      <div style="position:relative;width:100%;height:100%;">
        <div style="width:25%;position:absolute;top:0;left:0;bottom:0;background:#fff;box-shadow:0px 2px 3px #505050;z-index:1000;padding-top:8px;overflow:hidden;" id="users">
        @foreach($sales as $sale)
          <div class="user-row" id="id-{{$sale->Id}}">
            <div class="profile-pic">
              <h2>{{substr(strtoupper($sale->Name),0,2)}}</h2>
            </div>
            <div class="profile-info">
              <h3>{{$sale->Name}}</h3>
              <span style="display:none;">Is Typing</span>
            </div>
          </div>
          @endforeach
        </div>
        <div id="main">
          <div style="width:100%;height:100%;position:relative;">
            <div class="price-header">
              <h3 style="font-family: 'Ubuntu', sans-serif;font-weight:400;letter-spacing: 0px;font-size:smaller;">Nothing yet</h3>
            </div>
            <div id="content">
              <div style="position:relative;">
                <span id="extra">Extra</span>
                <h2 style="margin-top:10px;">Adele , Hello lyrics</h2> 
              </div>
              <hr style="margin:5px 0px;border-color:#cacaca;">
              <div id="content-content">
              <p style="font-size:large;line-height:25px;">
              hello from the outside i must've call a thousand times to tell you i'm sorry for everything that i'm done but when i call you , never seem to be home hello from the outside at least i can say that i've tried to tell ypu i'm sorry for breaking your heart but it don't matter it clearly doesn't tear you a part any moreee :-(
              hello from the outside i must've call a thousand times to tell you i'm sorry for everything that i'm done but when i call you , never seem to be home hello from the outside at least i can say that i've tried to tell ypu i'm sorry for breaking your heart but it don't matter it clearly doesn't tear you a part any moreee :-(
              hello from the outside i must've call a thousand times to tell you i'm sorry for everything that i'm done but when i call you , never seem to be home hello from the outside at least i can say that i've tried to tell ypu i'm sorry for breaking your heart but it don't matter it clearly doesn't tear you a part any moreee :-(
              hello from the outside i must've call a thousand times to tell you i'm sorry for everything that i'm done but when i call you , never seem to be home hello from the outside at least i can say that i've tried to tell ypu i'm sorry for breaking your heart but it don't matter it clearly doesn't tear you a part any moreee :-(
              </p>
              </div>
              <br>
              <ul id="imageGallery" style="display:block;width:100%;">
                <li data-thumb="{{asset("asset/image/2.jpg")}}" data-src="{{asset("asset/image/2.jpg")}}">
                  <img src="{{asset("asset/image/2.jpg")}}" height="250" style="display:block;margin:0px auto;"/>
                </li>
                <li data-thumb="{{asset("asset/image/3.jpg")}}" data-src="{{asset("asset/image/3.jpg")}}">
                  <img src="{{asset("asset/image/3.jpg")}}" height="250" style="display:block;margin:0px auto;"/>
                </li>
                <li data-thumb="{{asset("asset/image/4.jpg")}}" data-src="{{asset("asset/image/4.jpg")}}">
                  <img src="{{asset("asset/image/4.jpg")}}" height="250" style="display:block;margin:0px auto;"/>
                </li>
                </ul>



            </div>
            <div style="position:absolute;bottom:40px;display:block;width:100%;">
              <div style="position:relative;width:100%;display:block;">
                              
              </div>
            </div>
            <div id="input" class="bottom-bar-input">
              <div style="position:relative;width:100%;height:100%;">
                <input type="text" name="text" class="price-input money" placeholder="Enter Your Price (Must be more than last price) " id="price-input">
                <button type="button" class="send">Send</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</body>
</html>
<script type="text/javascript">
var baseUrl = "{{substr(asset(''), 0, strlen(asset('')) - 1)}}";
var timeout;
var isTyping = false;
var marr = ['1','2'];
var lastPrice = 100;
function issetArray(str, arr) {
  var i;
  for(i = 0; i < arr.length; i++){
    if(arr[i] == str)
      return true;
  }
  return false;
}
setInterval(function(){
  $.ajax({
    url : baseUrl + "test/getType",
    success : function(data) {
      /*if(status.indexOf("Is not typing")  == -1 && data.indexOf("Is not typing")  == -1){
        status = data;
        console.log(data);
      }else{
        console.log('not going');
      }*/
      var i;
      var str = data.split("|");
      for (var i = 0; i < 20; i++) {
        if(!issetArray(i.toString(), marr))
          $("#id-" + i).find(".profile-info").find("span").fadeOut();
      }
      marr = [];
      for(i = 0;i < str.length;i++){
        if(str[i] != null && str[i] != ""){
          arr = str[i].split("#");
          marr[i] = arr[0].toString();
          $("#id-" + arr[0]).find(".profile-info").find("span").fadeIn();
          $("#id-" + arr[0]).find(".profile-info").find("span").html("Is Typing " + arr[1].substr(0,25));
        }
      }
    }
  });
},2000);


setInterval(function(){
  $.ajax({
    url : baseUrl + "test/getPrice",
    success : function(data) {
      if(data.status == 200){
        if(parseFloat(data.price) != lastPrice){
          lastPrice = parseFloat(data.price);
          $(".price-header > h3").typed({
            strings: ["Last price " + parseFloat(data.price) + "$ from " + data.name],
            typeSpeed: 100,
            backDelay: 5000,
            showCursor: false
          });
        }
      }else{
        if(lastPrice != 0){
        lastPrice = 0;
        $(".price-header > h3").typed({
            strings: ["There is nothing to show"],
            typeSpeed: 100,
            backDelay: 5000,
            showCursor: false
          });
        }
      }
    }
  });
},3000);
function setTyping (arg) {
  var str;
  if(arg)
    str = "true*" + $(".money").val();
  else
    str = "false*";
  $.ajax({
    url : baseUrl + "test/type?type=" + str
  });
}
$(document).ready(function(){
  $('.money').mask('0,000,000,000,000.00', {reverse: true});
  $('.money').on('input',function(event) {
    event.preventDefault();
    if(!isTyping){
      isTyping = true;
    }
    setTyping(true);
    clearTimeout(timeout);
    timeout = setTimeout(function(){
      isTyping = false;
      setTyping(false);
    },1000);
    if($(this).val() != "" && $(this).val() !=  null){
    }
  });
  $(".send").click(function(event) {
    sendPrice();
  });

  function showThat(){
    swal({
      title: $("input#price-input").val() + "$",
      html : true,
      html: "Are you sure to send it? to agree click send it or press enter",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, send it!'
    }).then(function() {
      if(val != null && val != "" && val != " " && verify){
      $.ajax({
        url : baseUrl + "test/setPrice?price=" + val,
        success : function(data) {
          if(data.status == 200){
            $("input#price-input").val('');
          }else{
            swal({title:"Error",text:"Your price must be more than latest price!",confirmButtonColor: "#c9302c"});
          }
        }
      });
      }else{
        swal({title:"Error",text:"Please enter a price to send!",confirmButtonColor: "#c9302c"});
      }
    });
  }
  function sendPrice(){
    verify = true;
    val = $("input#price-input").val();
    if(parseFloat(val.replace(',', '')) > (lastPrice * 4)){
      showThat();
    }else{
    if(val != null && val != "" && val != " " && verify){
      $.ajax({
        url : baseUrl + "test/setPrice?price=" + val,
        success : function(data) {
          if(data.status == 200){
            $("input#price-input").val('');
          }else{
            swal({title:"Error",text:"Your price must be more than latest price!",confirmButtonColor: "#c9302c"});
          }
        }
      });
    }else{
      swal({title:"Error",text:"Please enter a price to send!",confirmButtonColor: "#c9302c"});
    }
    }
  }
  $("input#price-input").keypress(function(event) {
    if(event.which == 13){
      sendPrice();
    }
  });


  $('#imageGallery').lightSlider({
            gallery:false,
            item:1,
            loop:true,
            slideMargin:0,
            enableDrag: true,
            currentPagerPosition:'left' 
        });
  $('#imageGallery1').lightSlider({
            gallery:false,
            item:1,
            loop:true,
            adaptiveHeight:true,
            slideMargin:0,
            enableDrag: true,
            currentPagerPosition:'left' 
        });


  $('#content-content').slimScroll({
    height: '150px',
    color: '#297dce',
    opacity : 1,
    alwaysVisible : true
  });

  $('#content-content').bind('scroll',chk_scroll);
  function chk_scroll(e){
    var elem = $(e.currentTarget);
    if (elem[0].scrollHeight - elem.scrollTop() == elem.outerHeight())
    {
        console.log("bottom");
    }
  }

});
</script>