<?php if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) ob_start('ob_gzhandler'); else ob_start(); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <title>Dahlia</title>
  <script type="text/javascript">
  var baseUrl = "{{substr(asset(''), 0, strlen(asset('')) - 1)}}";
  var timer;
  var times = 90;
  var slider = null;
  var isSliderLoaded = false;
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-80283379-1']);
_gaq.push(['_setDomainName', 'none']);
_gaq.push(['_setAllowLinker', 'true']);
_gaq.push(['_trackPageview']);

(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>
  <style type="text/css">
        @font-face {
  font-family: 'Lato';
  font-style: normal;
  font-weight: 400;
  src: local('Lato Regular'), local('Lato-Regular'), url({{asset('asset/font/UyBMtLsHKBKXelqf4x7VRQ.woff2')}}) format('woff2');
  unicode-range: U+0100-024F, U+1E00-1EFF, U+20A0-20AB, U+20AD-20CF, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Lato';
  font-style: normal;
  font-weight: 400;
  src: local('Lato Regular'), local('Lato-Regular'), url({{asset('asset/font/1YwB1sO8YE1Lyjf12WNiUA.woff2')}}) format('woff2');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215, U+E0FF, U+EFFD, U+F000;
}
/* latin-ext */
@font-face {
  font-family: 'Yanone Kaffeesatz';
  font-style: normal;
  font-weight: 400;
  src: local('Yanone Kaffeesatz Regular'), local('YanoneKaffeesatz-Regular'), url({{asset('asset/font/YDAoLskQQ5MOAgvHUQCcLV83L2yn_om9bG0a6EHWBso.woff2')}}) format('woff2');
  unicode-range: U+0100-024F, U+1E00-1EFF, U+20A0-20AB, U+20AD-20CF, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Yanone Kaffeesatz';
  font-style: normal;
  font-weight: 400;
  src: local('Yanone Kaffeesatz Regular'), local('YanoneKaffeesatz-Regular'), url({{asset('asset/font/YDAoLskQQ5MOAgvHUQCcLfGwxTS8d1Q9KiDNCMKLFUM.woff2')}}) format('woff2');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215, U+E0FF, U+EFFD, U+F000;
}
  button,input,select,.form-control,*,.btn{
    border-radius: 0px;
    letter-spacing: 1px;
  font-family: 'Yanone Kaffeesatz', sans-serif;
  }
  nav{
    box-shadow: 0px 0px 3px #000;
    letter-spacing: 1px;
  font-family: 'Yanone Kaffeesatz', sans-serif;
  }
  </style>
  <link rel="stylesheet" href="{{asset('asset/css/bootstrap.min.css')}}">
  <script src="{{asset('asset/js/jquery.min.js')}}"></script>
  <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>-->
  <script src="{{asset('asset/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('asset/js/sweetalert2.min.js')}}"></script>
  <link rel="stylesheet" href="{{asset('asset/css/animate.css')}}">
  <!--<script src="//oss.maxcdn.com/jquery.form/3.50/jquery.form.min.js"></script>-->
  <link rel="stylesheet" href="{{asset('asset/css/sweetalert2.min.css')}}">
  <link rel="stylesheet" href="{{asset('asset/css/view.main.css')}}">
  <link rel="stylesheet" href="{{asset('asset/css/lightslider.min.css')}}">
  <script type="text/javascript" src="{{asset('asset/js/lightslider.min.js')}}"></script>
  <!--<script src="{{asset('asset/js/dialogbox.js')}}"></script>-->
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/flaticon.css')}}"> 
  <script type="text/javascript" src="{{asset('asset/js/jquery.timer.js')}}"></script>
  </head>
  <body>

<nav class="navbar navbar-inverse navbar-fixed-top" style="background:#297dce;border-color:#297dce;box-shadow: 0px 0px 3px #000;">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#" style="color:#fff;">Dahlia</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="{{asset('')}}">Home</a></li>
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="color:#fff;">Main page<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Profile</a></li>
            <li><a href="#">Profile</a></li>
            <li><a href="#">Support</a></li>
          </ul>
        </li>
        <li><a href="#" style="color:#fff;">Tickets</a></li>
        <li><a href="#" style="color:#fff;">Support</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#" style="color:#fff;"><span class="glyphicon glyphicon-star"></span> Sign Up</a></li>
        <li><a href="#" style="color:#fff;"><span class="glyphicon glyphicon-plus"></span> Login</a></li>
      </ul>
    </div>
  </div>
</nav>
    <div class="container" style="padding-top:10px;">


    <div class="row" style="padding:2px 1.3% 0px 1.3%;margin-bottom:10px;margin-top:5px;">
      <img src="{{asset('asset/image/icon/home-1.png')}}" width="24" style="vertical-align:middle;">
      <img src="{{asset('asset/image/icon/next.png')}}" width="15" style="vertical-align:middle;margin:0px 4px;">
      <span style="color:#297dce;vertical-align:middle;">Asia</span>
      <img src="{{asset('asset/image/icon/next.png')}}" width="15" style="vertical-align:middle;margin:0px 4px;">
      <span style="color:#297dce;vertical-align:middle;">Iran</span>
      <img src="{{asset('asset/image/icon/next.png')}}" width="15" style="vertical-align:middle;margin:0px 4px;">
      <span style="color:#297dce;vertical-align:middle;">Isfahan</span>
      <div style="float:right;margin-top:-4px;" id="order-box">
      <img src="{{asset('asset/image/icon/like-4.png')}}" width="25" style="vertical-align:middle;margin:0px 4px;">
      <img src="{{asset('asset/image/icon/share.png')}}" width="25" style="vertical-align:middle;margin:0px 4px;">
      </div>
      <hr style="margin:5px 0px 0px 0px;padding:0px;">
    </div>


        <div class="row">
          <div class="col-md-8 col-sm-8 col-xs-12" id="preview-main">
            <div class="main" style="background:#eaeaea;padding:3px;border-radius:5px;" id="preview-gallery">
              <ul id="imageGallery" style="display:block;">
                <?php
                if(strpos($adv->Pic, ",") !== false){
                  $var = explode(",",$adv->Pic);
                  foreach ($var as $key => $value) {
                ?>
                <li data-thumb="https://drive.google.com/uc?export=view&id={{$value}}" data-src="https://drive.google.com/uc?export=view&id={{$value}}">
                  <img src="https://drive.google.com/uc?export=view&id={{$value}}" height="300" style="display:block;margin:0px auto;"/>
                </li>
                <?php
                  }
                }else{
                ?>
                <li data-thumb="https://drive.google.com/uc?export=view&id={{$adv->Pic}}" data-src="https://drive.google.com/uc?export=view&id={{$adv->Pic}}">
                  <img src="https://drive.google.com/uc?export=view&id={{$adv->Pic}}" height="300" style="display:block;margin:0px auto;"/>
                </li>
                <?php
                }
                ?>
              </ul>
            </div>
          

          <div style="background:#fff;margin-top:10px;" id="preview-content">
            <h1 style="padding:3px 5px;margin-top:0px;">
            <b>
            {{$adv->Sub}}
            </b>
            </h1>
            <div style="position:relative;width:48%;display:inline-block;">
              <span style="color:#297dce;font-size:large;">Price</span>
              <span style="color:#13126E;position:absolute;right:0px;" class="val">65511$</span>
              <hr style="margin:5px 0px 0px 0px;">
            </div>
            <div style="position:relative;width:48%;display:block;">
              <span style="color:#297dce;font-size:large;">Trust</span>
              <span style="color:#13126E;position:absolute;right:0px;" class="val">65511$</span>
              <hr style="margin:5px 0px 0px 0px;">
            </div>
            <div style="padding-left:10px;padding:5px 0px;">
              <img src="{{asset('asset/image/icon/clock.png')}}" width="18" style="vertical-align:middle;">
              <span style="vertical-align:middle;padding:1px 3px;color:#606060;">Just a minute ago</span>
            </div>
            <hr style="margin:0px;">
            <p style="padding:8px 10px;">
              {{$adv->Content}}
            </p>
          </div>
          <div class="alert alert-warning" style="position:relative;top:10px;">
            <strong>Warning!</strong> Please dont trust every advertise and never pay online
          </div>
          </div>
          <div style="position:absolute;right:75px;width:28%;" id="profilebox-parent">
            <div id="profilebox" class="col-md-12 col-sm-12 col-xs-12" style="background:#eaeaea;padding:10px 10px 5px 5px;border:1px #eaeaea solid;margin-bottom:20px;border-radius:5px;">
            <center>
              <img src="{{asset('asset/image/icon/social/user.png')}}" class="img-responsive" width="90">
              <h3>Owner Information</h3>
              <button type="button" class="btn btn-labeled btn-primary col-sm-12 col-xs-12" style="border-radius:0px;position:relative;margin:3px 3px;border:none;padding:7px 0px;" id="prev-get-phone"><span class="btn-label"><img src="{{asset('asset/image/icon/call.png')}}"></span><span id="prev-get-phone-span">Call</span></button>
              <button type="button" class="btn btn-labeled btn-primary col-sm-12 col-xs-12" style="border-radius:0px;position:relative;margin:3px 3px;border:none;padding:7px 0px;" id="prev-get-mail"><span class="btn-label"><img src="{{asset('asset/image/icon/email.png')}}"></span>Send Mail</button>
              <button type="button" class="btn btn-labeled btn-primary col-sm-12 col-xs-12" style="border-radius:0px;position:relative;margin:3px 3px;border:none;padding:7px 0px;" id="prev-get-telegram"><span class="btn-label"><img src="{{asset('asset/image/icon/social/telegram.png')}}" width="24" height="24"></span>Telegram</button>
              <button type="button" class="btn btn-labeled btn-primary col-sm-12 col-xs-12" style="border-radius:0px;position:relative;margin:3px 3px;border:none;padding:7px 0px;"><span class="btn-label"><img src="{{asset('asset/image/icon/mail.png')}}"></span>Send Ticket</button>
            </center>
            </div>
            
            <div style="background:#fff;margin-bottom:20px;" id="preview-req">
          <div class="row" id="preview-price">
          </div>
          <hr style="margin:0px;">

            <div style="padding-left:10px;position:relative;padding:5px 0px;">
              <img src="{{asset('asset/image/icon/placeholder-1.png')}}" width="18" style="vertical-align:middle;">
              <span style="vertical-align:middle;padding:1px 3px;color:#606060;">Location(s) : Iran, Isfahan, Tiran</span>
              <button class="btn btn-order-active" style="position:absolute;right:0px;top:0px;padding:10px 5px 11px 5px;">
                <img src="{{asset('asset/image/icon/next-1.png')}}" width="15" style="vertical-align:middle;display:block;transform:rotate(90deg);">
              </button>
            </div>
            
          <hr style="margin:0px;">


            
        </div>
          </div>
        </div>
      </div>
  </body>
  <!--<link href="https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Lato:200?text=123456789$" rel="stylesheet"> -->
</html>
<script type="text/javascript">
  $('#imageGallery').lightSlider({
            gallery:true,
            item:1,
            loop:false,
            thumbItem:6,
            slideMargin:0,
            enableDrag: false,
            currentPagerPosition:'left',
            onSliderLoad: function(el) {
               el.lightGallery({
                    selector: '#imageGallery .lslide'
                });
            }  
        });

  $(window).bind('scroll',chk_scroll);
  function chk_scroll(e){
    if(window.scrollY > 53){
      $("#profilebox-parent").css("position","fixed");
      $("#profilebox-parent").css("top","60px");
    }else{
      $("#profilebox-parent").css("position","absolute");
      $("#profilebox-parent").css("top","111px");
    }
  }
</script>

      <?php
        ob_end_flush();
      ?>