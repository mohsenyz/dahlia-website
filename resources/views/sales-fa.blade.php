
<?php if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) ob_start('ob_gzhandler'); else ob_start(); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <title>Dahlia - Sales</title>
  <script type="text/javascript">
  var baseUrl = "{{substr(asset(''), 0, strlen(asset('')) - 1)}}";
  var timer;
  var times = 30;
  var slider = null;
  var isSliderLoaded = false;
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-80283379-1']);
_gaq.push(['_setDomainName', 'none']);
_gaq.push(['_setAllowLinker', 'true']);
_gaq.push(['_trackPageview']);

(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>
  <link rel="stylesheet" href="{{asset('asset/css/bootstrap.min.css')}}">
  <script src="{{asset('asset/js/jquery.min.js')}}"></script>
  <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>-->
  <script src="{{asset('asset/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('asset/js/sweetalert2.min.js')}}"></script>
  <link rel="stylesheet" href="{{asset('asset/css/animate.css')}}">
  <!--<script src="//oss.maxcdn.com/jquery.form/3.50/jquery.form.min.js"></script>-->
  <link rel="stylesheet" href="{{asset('asset/css/sweetalert2.min.css')}}">
  <!--link rel="stylesheet" href="{{asset('asset/css/lightslider.min.css')}}"-->
  <!--script type="text/javascript" src="{{asset('asset/js/lightslider.min.js')}}"></script-->
  <!--<script src="{{asset('asset/js/dialogbox.js')}}"></script>-->
  <script type="text/javascript" src="{{asset('asset/js/typed.js')}}"></script>
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/flaticon.css')}}"> 
  <!--script type="text/javascript" src="{{asset('asset/js/jquery.timer.js')}}"></script-->
  <!--link rel="stylesheet" href="{{asset('asset/css/main.css')}}"-->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
  <style type="text/css">
  @font-face {
  font-family: 'Lato';
  font-style: normal;
  src: local('Lato Regular'), local('Lato-Regular'), url({{asset('asset/font/IRANSansWeb_Light.woff2')}}) format('woff2');
}
/* latin */
@font-face {
  font-family: 'Lato';
  font-style: normal;
  src: local('Lato Regular'), local('Lato-Regular'), url({{asset('asset/font/IRANSansWeb_Light.woff2')}}) format('woff2');
}
/* latin-ext */
@font-face {
  font-family: 'Yanone Kaffeesatz';
  font-style: normal;
  src: local('Yanone Kaffeesatz Regular'), local('YanoneKaffeesatz-Regular'), url({{asset('asset/font/IRANSansWeb_Light.woff2')}}) format('woff2');
}
/* latin */
@font-face {
  font-family: 'Yanone Kaffeesatz';
  font-style: normal;
  src: local('Yanone Kaffeesatz Regular'), local('YanoneKaffeesatz-Regular'), url({{asset('asset/font/IRANSansWeb_Light.woff2')}}) format('woff2');
}
  button,input,select,.form-control,*,.btn{
    border-radius: 0px;
  font-family: 'Yanone Kaffeesatz', sans-serif;
  }
  nav{
    box-shadow: 0px 0px 3px #000;
  font-family: 'Yanone Kaffeesatz', sans-serif;
  }
  body{
    padding: 0px;
    margin: 0px;
    overflow-x: hidden;
  font-family: 'Yanone Kaffeesatz', sans-serif;
  font-size: normal;
  background: #f0f0f0;
  }
    .row{
      margin: 0px;
      padding: 0px;
    }
    #floataction{
      position:fixed;bottom:30px;left:30px;background:#297dce;border-radius:30px;width:60px;height:60px;
      transition: 0.3s;
      box-shadow: 0px 0px 8px #202020;
    }
    #floataction:hover{
      background:#1D5C98;
    }
    .container{
      width:100%;
      margin:0 auto;
      margin:0px;
      padding: 50px 0px 0px 0px;
    }
    .btn-login{
      background:#297dce;
      display:block;
      width:100%;
      text-align:center;
      transition: 0.3s;
    }
    .btn-login:hover{
    background: #226CB3;
    border-color:#226CB3;
    }
    #child{
      transition: 0.3s;
      position: relative;
      margin-right: 0px;
      padding-right: 0px;
      padding: 0px;
      margin-bottom: 10px;
      float: right;
      /*margin-bottom:5px;
      border-top: 2px #297dce solid;
      border-bottom:1px #cacaca solid;*/
    }
    #child .row{
      position:relative;width:98%;height:460px;
      background: #fff;
      transition: 0.5s;
      overflow-x: hidden;
      box-shadow:0px 1px 2px #808080;
    }
    #child:hover .row{
      /*border-bottom: 2px #297dce solid;*/
    }
    #child #pic{
      transition: 0.3s;
    }
    #child #prop{
      padding-left: 5px;
      position: relative;
      overflow: hidden;
    }
    #child #prop h4{
      color:#297dce;margin-top:10px;margin-bottom:0px;transition: 0.3s;
    }
    #child:hover #prop h4{
      color:#FA7B05;
    }
    #child:hover #pic{
      -webkit-filter: contrast(120%); /* Chrome, Safari, Opera */
      filter: contrast(120%);
    }
    #child:hover{
      border-color:#297dce;
    }
    .btn-order{
      margin:auto -3px;
      display: inline-block;
      transition: 0.3s;
      border-color: #eaeaea;
    }
    .btn-order:hover{
      background: #eaeaea;
      border-color: #eaeaea;
    }
    .btn-order-active{
      background: #297dce;
      color: #fff;
      border-color: #297dce;
      padding:5px 2px 4px 2px;
    }
    .btn-order-active:hover{
      background: #297dce;
      color: #fff;
      border-color: #297dce;
    }
    #order-box button:first-child{
      border-radius: 5px 0px 0px 5px;
    }
    #order-box button:last-child{
      border-radius: 0px 5px 5px 0px;
    }
    @keyframes alert {
    from {background-color: rgba(256,0,0,0.6);}
    to {background-color: rgba(256,0,0,0.9);}
    }
    @-webkit-keyframes alert {
    from {background-color: rgba(256,0,0,0.6);}
    to {background-color: rgba(256,0,0,0.9);}
    }

    #bar-profile{
  position:absolute;
  top:7px;
  left: 20px;
  background: #1565c0;
  text-decoration:none;
  padding: 10px 50px 10px 20px;
  transition: 0.3s;
}
#bar-profile:hover{
  background: #0d47a1;
}
#bar-profile > a{
  color: #fff;
  position: relative;
  display: block;
  width: 100%;
  text-decoration: none;
}
  </style>
  </head>
  <body dir="rtl">

<nav class="navbar navbar-inverse navbar-fixed-top" style="background:#297dce;border-color:#297dce;width:100%;margin:0px;">
  <div class="container-fluid">
    <div class="navbar-header" style="float:right;">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#" style="color:#fff;">@lang('main.dahlia')</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav" style="float:right;">
        <li><a href="/fa/sales/room/admin?pid=1452" style="color:#fff;">دمو پنل ادمین</a></li>
        <li><a href="/fa/sales/room?pid=1452" style="color:#fff;">دمو حراجی</a></li>
        <li><a href="/fa/sales" style="color:#fff;">@lang('main.sales')</a></li>
        <li class="active"><a href="{{asset('/fa/')}}">@lang('main.home')</a></li>
      </ul>
      <div id="bar-profile">
        <a href="#" id="open-cart">
        <span>
        کاربر
        </span>
        <img src="{{asset('asset/image/icon/user-pro.png')}}" width="40" height="40" style="position:absolute;right:-50px;top:-10px;background:#0d47a1;border:0px;padding:7px;">
        </a>
      </div>
      <ul class="nav navbar-nav navbar-right">
        <!--<li><a href="#"><span class="glyphicon glyphicon-star"></span> Sign Up</a></li>
        <li><a href="#"><span class="glyphicon glyphicon-plus"></span> Login</a></li>-->
      </ul>
    </div>
  </div>
</nav>
    <div class="container">
    <div class="row" style="padding:15px 5% 0px 5%;background:#f0f0f0;margin:0px;margin-bottom:5px;">
      <div class="form-group col-md-2" style="padding:0px;margin-right:8.333333%;">
        <button type="button" class="btn btn-primary btn-login" style="margin-right:-8px;">@lang('main.search')</button>
      </div>
      <div class="form-group col-md-4" style="margin:0px;padding:0px;">
        <button class="btn btn-default" style="display:block;width:100%;text-align:right;" id="selectState">@lang('main.location')</button>
      </div>
      <div class="form-group col-md-5" style="margin:0px;padding:0px;">
        <input type="text" name="" class="form-control" placeholder="@lang('main.search')" style="font-size:normal;">
      </div>
    </div>
    <div class="row" style="padding:0px 5%;">
    <?php
    if(count($adv) == 0){
    ?>
    <h3 style="width:100%;text-align:center;">@lang('sales.nothing')</h3>
    <?php
    }
    ?>
    @foreach ($adv as $advs)
    <a href="#" style="text-decoration:none;">
    <?php
            $pic = "";
            if(strpos($advs->Pic, ",") !== false){
              $pics = explode(",", $advs->Pic);
              foreach ($pics as $key => $value) {
                if ($value != "0") {
                  $pic = $value;
                }
              }
            }else{
              $pic = $advs->Pic;
            }
          ?>
      <div class="col-md-4" id="child">
        <div class="row">
          <div class="col-md-12" style="padding:0px;height:300px;overflow:hidden;border-bottom:#297dce 3px solid;position:relative;">
            <img src="/image/s/{{$pic}}.jpg" class="img-responsive" style="width:100%;min-height:300px;"/>
            <span style="position:absolute;bottom:0px;display:block;left:0px;text-align:center;padding:3px 0px;background:rgba(30,30,30,0.6);color:#fff;width:100%;" class="typed">@lang('sales.lastprice'): 600 @lang('sales.toman')</span>
          </div>
          <div class="col-md-12" id="prop">
            <h4>{{$advs->Sub}}</h4>
            <hr style="width:10000%;padding:0px 0px;margin:4px 0px 0px 0px;">
            <h5 style="color:#505050;margin-top:0px auto;margin-left:0px;vertical-align:center;margin-top:0px;margin-bottom:0px;padding:3px 0px;margin-right:-10px;position:relative;">
              <img src="{{asset('asset/image/icon/placeholder-1.png')}}" width="24" style="vertical-align:middle;">
              <span style="vertical-align:middle;color:#303030;">@lang('sales.salesavailable') : @lang('main.' . $advs->City)</span>
              <button class="btn btn-order-active" style="position:absolute;right:0px;top:0px;">
                <img src="{{asset('asset/image/icon/next-1.png')}}" width="15" style="vertical-align:middle;padding:3px 0px 2px 0px;display:block;">
              </button>
            </h5>
            <hr style="width:10000%;padding:0px 0px;margin:0px 0px 4px 0px;">
            <h5 style="color:#404040;max-height:340px;overflow:hidden;">{{$advs->Content}}</h5>
          </div>
          <img src="{{asset('asset/image/icon/ribbon-2.png')}}" width="30" style="position:absolute;left:5px;top:0px;">
          <div style="position:absolute;bottom:0px;width:100%;background:#fff;box-shadow:0px -10px 10px #fff;">
            <h5 style="width:100%;text-align:center;margin:0px 0px 10px 0px;"><span style="vertical-align:bottom;"></span><img src="{{asset('asset/image/icon/down-arrow.png')}}" width="12" style="vertical-align:middle;margin-left:5px;" />@lang('sales.more')</h5>
          </div>
          <img src="{{asset('asset/image/icon/share.png')}}" width="25" style="position:absolute;left:5px;bottom:3px;background:#fff;">
        </div>
      </div>
      </a>
      @endforeach

    </div>


    </div>
    <div id="floataction" style="">
      <a href="{{asset('/advertise/new')}}"><img src="{{asset('asset/image/icon/add.png')}}" width="60"  style="padding:20px;" /></a>
    </div>
</body>
</html>
<script type="text/javascript">
  $(document).ready(function(){
    function foo(){
      $(".typed").typed({
        strings: ["آخرین قیمت  ۲۳۲۴۴ تومان توسط mohammad","تعداد افراد حاضر در حراجی  ۲ نفر"],
        typeSpeed: 100,
        backDelay: 5000,
        callback: function() {
          setTimeout(function(){ foo(); }, 5000);
        }
      });
    }
    foo();
  });
</script>