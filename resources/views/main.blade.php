
<?php if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) ob_start('ob_gzhandler'); else ob_start(); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <title>Dahlia</title>
  <script type="text/javascript">
  var baseUrl = "{{substr(asset(''), 0, strlen(asset('')) - 1)}}";
  var timer;
  var times = 30;
  var slider = null;
  var isSliderLoaded = false;
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-80283379-1']);
_gaq.push(['_setDomainName', 'none']);
_gaq.push(['_setAllowLinker', 'true']);
_gaq.push(['_trackPageview']);

(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>
  <link rel="stylesheet" href="{{asset('asset/css/bootstrap.min.css')}}">
  <script src="{{asset('asset/js/jquery.min.js')}}"></script>
  <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>-->
  <script src="{{asset('asset/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('asset/js/sweetalert2.min.js')}}"></script>
  <link rel="stylesheet" href="{{asset('asset/css/animate.css')}}">
  <!--<script src="//oss.maxcdn.com/jquery.form/3.50/jquery.form.min.js"></script>-->
  <link rel="stylesheet" href="{{asset('asset/css/sweetalert2.min.css')}}">
  <!--link rel="stylesheet" href="{{asset('asset/css/lightslider.min.css')}}"-->
  <!--script type="text/javascript" src="{{asset('asset/js/lightslider.min.js')}}"></script-->
  <!--<script src="{{asset('asset/js/dialogbox.js')}}"></script>-->
  <!--script type="text/javascript" src="{{asset('asset/js/main.js')}}"></script-->
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/flaticon.css')}}"> 
  <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet"> 
  <!--script type="text/javascript" src="{{asset('asset/js/jquery.timer.js')}}"></script-->
  <!--link rel="stylesheet" href="{{asset('asset/css/main.css')}}"-->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
  <style type="text/css">
  @font-face {
  font-family: 'Lato';
  font-style: normal;
  font-weight: 400;
  src: local('Lato Regular'), local('Lato-Regular'), url({{asset('asset/font/UyBMtLsHKBKXelqf4x7VRQ.woff2')}}) format('woff2');
  unicode-range: U+0100-024F, U+1E00-1EFF, U+20A0-20AB, U+20AD-20CF, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Lato';
  font-style: normal;
  font-weight: 400;
  src: local('Lato Regular'), local('Lato-Regular'), url({{asset('asset/font/1YwB1sO8YE1Lyjf12WNiUA.woff2')}}) format('woff2');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215, U+E0FF, U+EFFD, U+F000;
}
/* latin-ext */
@font-face {
  font-family: 'Yanone Kaffeesatz';
  font-style: normal;
  font-weight: 400;
  src: local('Yanone Kaffeesatz Regular'), local('YanoneKaffeesatz-Regular'), url({{asset('asset/font/YDAoLskQQ5MOAgvHUQCcLV83L2yn_om9bG0a6EHWBso.woff2')}}) format('woff2');
  unicode-range: U+0100-024F, U+1E00-1EFF, U+20A0-20AB, U+20AD-20CF, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Yanone Kaffeesatz';
  font-style: normal;
  font-weight: 400;
  src: local('Yanone Kaffeesatz Regular'), local('YanoneKaffeesatz-Regular'), url({{asset('asset/font/YDAoLskQQ5MOAgvHUQCcLfGwxTS8d1Q9KiDNCMKLFUM.woff2')}}) format('woff2');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215, U+E0FF, U+EFFD, U+F000;
}
  button,input,select,.form-control,*,.btn{
    border-radius: 0px;
    letter-spacing: 1px;
  font-family: 'Yanone Kaffeesatz', sans-serif;
  }
  nav{
    box-shadow: 0px 0px 3px #000;
    letter-spacing: 1px;
  font-family: 'Yanone Kaffeesatz', sans-serif;
  }
  body{
    padding: 0px;
    margin: 0px;
    overflow-x: hidden;
    letter-spacing: 1px;
  font-family: 'Yanone Kaffeesatz', sans-serif;
  font-size: large;
  }
    .row{
      margin: 0px;
      padding: 0px;
    }
    #floataction{
      position:fixed;bottom:30px;right:30px;background:#297dce;border-radius:30px;width:60px;height:60px;
      transition: 0.3s;
      box-shadow: 0px 0px 8px #202020;
    }
    #floataction:hover{
      background:#1D5C98;
    }
    .container{
      width:100%;
      margin:0 auto;
      margin:0px;
      padding: 50px 0px 0px 0px;
    }
    .btn-login{
      background:#297dce;
      display:block;
      width:100%;
      text-align:center;
      transition: 0.3s;
    }
    .btn-login:hover{
    background: #226CB3;
    border-color:#226CB3;
    }
    #child{
      transition: 0.3s;
      position: relative;
      margin-right: 0px;
      padding-right: 0px;
      padding: 0px;
      margin-bottom: 10px;
      
      /*margin-bottom:5px;
      border-top: 2px #297dce solid;
      border-bottom:1px #cacaca solid;*/
    }
    #child .row{
      position:relative;width:98%;height:149px;border-bottom:2px #cacaca solid;
      transition: 0.5s;
      overflow: hidden;
    }
    #child:hover .row{
      border-bottom: 2px #297dce solid;
    }
    #child #pic{
      transition: 0.3s;
    }
    #child #prop{
      padding-left: 5px;
      height: 100%;
      position: relative;
    }
    #child #prop h3{
      color:#297dce;margin-top:10px;margin-bottom:0px;transition: 0.3s;
    }
    #child:hover #prop h3{
      color:#FA7B05;
    }
    #child:hover #pic{
      -webkit-filter: contrast(120%); /* Chrome, Safari, Opera */
      filter: contrast(120%);
    }
    #child:hover{
      border-color:#297dce;
    }
    .btn-order{
      margin:auto -3px;
      display: inline-block;
      transition: 0.3s;
      border-color: #eaeaea;
    }
    .btn-order:hover{
      background: #eaeaea;
      border-color: #eaeaea;
    }
    .btn-order-active{
      background: #297dce;
      color: #fff;
      border-color: #297dce;
    }
    .btn-order-active:hover{
      background: #297dce;
      color: #fff;
      border-color: #297dce;
    }
    #order-box button:first-child{
      border-radius: 5px 0px 0px 5px;
    }
    #order-box button:last-child{
      border-radius: 0px 5px 5px 0px;
    }
  </style>
  </head>
  <body>

<nav class="navbar navbar-inverse navbar-fixed-top" style="background:#297dce;border-color:#297dce;width:100%;margin:0px;">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#" style="color:#fff;">@lang('main.dahlia')</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="{{asset('')}}">@lang('main.home')</a></li>
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="color:#fff;">@lang('main.mainpage')<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Profile</a></li>
            <li><a href="#">Profile</a></li>
            <li><a href="#">@lang('main.support')</a></li>
          </ul>
        </li>
        <li><a href="#" style="color:#fff;">@lang('main.tickets')</a></li>
        <li><a href="#" style="color:#fff;">@lang('main.support')</a></li>
        <li><a href="/sales" style="color:#fff;">@lang('main.sales')</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <!--<li><a href="#"><span class="glyphicon glyphicon-star"></span> Sign Up</a></li>
        <li><a href="#"><span class="glyphicon glyphicon-plus"></span> Login</a></li>-->
      </ul>
    </div>
  </div>
</nav>
    <div class="container">
    <div class="row" style="padding:15px 5% 0px 5%;background:#eaeaea;margin:0px;margin-bottom:5px;">
      <div class="form-group col-md-3" style="margin:0px;padding:0px;">
        <input type="text" name="" class="form-control" placeholder="Search" style="font-size:large;">
      </div>
      <div class="form-group col-md-3" style="margin:0px;padding:0px;">
        <select class="form-control selectpicker" id="sel1" style="font-size:large;">
          <option style="font-size:large;">@lang('main.categorie')</option>
          <option>2</option>
          <option>3</option>
          <option>4</option>
        </select>
      </div>
      <div class="form-group col-md-3" style="margin:0px;padding:0px;">
        <select class="form-control selectpicker" id="sel1" style="font-size:large;">
          <option>@lang('main.location')</option>
          <option>2</option>
          <option>3</option>
          <option>4</option>
        </select>
      </div>
      <div class="form-group col-md-1 col-md-offset-1" style="padding:0px;">
        <button type="button" class="btn btn-primary btn-login" style="background:#fb8c00;border-color:#fb8c00;">@lang('main.morefilter')</button>
      </div>
      <div class="form-group col-md-1" style="padding:0px;">
        <button type="button" class="btn btn-primary btn-login">@lang('main.search')</button>
      </div>
    </div>
    <div class="row" style="padding:2px 5% 0px 5%;margin-bottom:20px;">
      <img src="{{asset('asset/image/icon/home-1.png')}}" width="24" style="vertical-align:middle;">
      <img src="{{asset('asset/image/icon/next.png')}}" width="15" style="vertical-align:middle;margin:0px 4px;">
      <span style="color:#297dce;vertical-align:middle;">@lang('main.asia')</span>
      <img src="{{asset('asset/image/icon/next.png')}}" width="15" style="vertical-align:middle;margin:0px 4px;">
      <span style="color:#297dce;vertical-align:middle;">@lang('main.iran')</span>
      <img src="{{asset('asset/image/icon/next.png')}}" width="15" style="vertical-align:middle;margin:0px 4px;">
      <span style="color:#297dce;vertical-align:middle;">@lang('main.isfahan')</span>
      <img src="{{asset('asset/image/icon/next.png')}}" width="15" style="vertical-align:middle;margin:0px 4px;">
      <span style="color:#297dce;vertical-align:middle;">@lang('main.all')</span>
      <div style="float:right;margin-top:-4px;" id="order-box">
        <button class="btn btn-default btn-order btn-order-active">@lang('main.urgent')</button>
        <button class="btn btn-default btn-order">@lang('main.latest')</button>
        <button class="btn btn-default btn-order">@lang('main.cheap')</button>
        <button class="btn btn-default btn-order">@lang('main.expensive')</button>
      </div>
      <hr style="margin:5px 0px 0px 0px;padding:0px;">
    </div>
    <div class="row" style="padding:0px 5%;">
    <?php
    if(count($adv) == 0){
    ?>
    <h3 style="width:100%;text-align:center;">@lang('main.nothing')</h3>
    <?php
    }
    ?>
    @foreach ($adv as $advs)
    <a href="{{asset('/view/' . $advs->Id)}}">
      <div class="col-md-4" id="child">
        <div class="row">
          <div class="col-md-2 col-sm-2 col-xs-2" style="display: inline-block;float:left;background:#eaeaea;width:150px;height:150px;padding:0px;margin-top:-2px;" id="pic">
            <img src="https://drive.google.com/uc?export=view&id={{$advs->Pic}}" width="150"  height="150" />
          </div>
          <div class="col-md-6 col-sm-6 col-xs-6" id="prop">
            <h3>{{$advs->Sub}}</h3>
            <h4 style="color:#505050;margin-top:5px;margin-left:10px;">{{$advs->City}}</h4>
            <h4 style="position:absolute;color:#454545;bottom:0px;left:15px;margin-bottom:5px;" class="marquee">
              <hr style="width:1000%;padding:0px 0px;margin:0px -15px;padding:1px 0px;">
              <span style="display:inline-block;margin-right:10px;">@lang('main.price'): <span style="color:#297dce;">{{$advs->Price}}</span>$</span>
              <!--span style="display:inline-block;">Trust: <span style="color:#297dce;">{{$advs->Price}}</span>$</span-->
            </h4>
          </div>
          
        </div>
      </div>
      </a>
      @endforeach
      

    </div>


    </div>
    <div id="floataction" style="">
      <a href="{{asset('/advertise/new')}}"><img src="{{asset('asset/image/icon/add.png')}}" width="60"  style="padding:20px;" /></a>
    </div>
</body>
</html>
<script type="text/javascript">
</script>