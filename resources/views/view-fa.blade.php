<?php if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) ob_start('ob_gzhandler'); else ob_start();
use App\Http\Controllers\NumberFormatting as NumberFormatting; 
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <title>Dahlia</title>
  <script type="text/javascript">
  var baseUrl = "{{substr(asset(''), 0, strlen(asset('')) - 1)}}";
  var timer;
  var times = 90;
  var slider = null;
  var isSliderLoaded = false;
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-80283379-1']);
_gaq.push(['_setDomainName', 'none']);
_gaq.push(['_setAllowLinker', 'true']);
_gaq.push(['_trackPageview']);

(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>
  <link rel="stylesheet" href="{{asset('asset/css/bootstrap.min.css')}}">
  <script src="{{asset('asset/js/jquery.min.js')}}"></script>
  <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>-->
  <script src="{{asset('asset/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('asset/js/sweetalert2.min.js')}}"></script>
  <link rel="stylesheet" href="{{asset('asset/css/animate.css')}}">
  <!--<script src="//oss.maxcdn.com/jquery.form/3.50/jquery.form.min.js"></script>-->
  <link rel="stylesheet" href="{{asset('asset/css/sweetalert2.min.css')}}">
  <link rel="stylesheet" href="{{asset('asset/css/view-fa.main.css')}}">
  <link rel="stylesheet" href="{{asset('asset/css/lightslider.min.css')}}">
  <script type="text/javascript" src="{{asset('asset/js/lightslider.min.js')}}"></script>
  <!--<script src="{{asset('asset/js/dialogbox.js')}}"></script>-->
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/flaticon.css')}}"> 
  <script type="text/javascript" src="{{asset('asset/js/jquery.timer.js')}}"></script>
    <style type="text/css">
    @font-face {
  font-family: 'Lato';
  font-style: normal;
  src: local('Lato Regular'), local('Lato-Regular'), url({{asset('asset/font/IRANSansWeb_Light.woff2')}}) format('woff2');
}
/* latin */
@font-face {
  font-family: 'Lato';
  font-style: normal;
  src: local('Lato Regular'), local('Lato-Regular'), url({{asset('asset/font/IRANSansWeb_Light.woff2')}}) format('woff2');
}
/* latin-ext */
@font-face {
  font-family: 'Yanone Kaffeesatz';
  font-style: normal;
  src: local('Yanone Kaffeesatz Regular'), local('YanoneKaffeesatz-Regular'), url({{asset('asset/font/IRANSansWeb_Light.woff2')}}) format('woff2');
}
/* latin */
@font-face {
  font-family: 'Yanone Kaffeesatz';
  font-style: normal;
  src: local('Yanone Kaffeesatz Regular'), local('YanoneKaffeesatz-Regular'), url({{asset('asset/font/IRANSansWeb_Light.woff2')}}) format('woff2');
}
  button,input,select,.form-control,*,.btn{
    border-radius: 0px;
  font-family: 'Yanone Kaffeesatz', sans-serif;
  }
  nav{
    box-shadow: 0px 0px 3px #000;
  font-family: 'Yanone Kaffeesatz', sans-serif;
  font-size: small;
  }
  body,*{
    letter-spacing: 0px;
  }
  .gray-back{
    width: 100%;
    height: 100%;
    position: fixed;
    top: 0px;
    left: 0px;
    z-index: 999;
    background: #000;
    opacity: 0.7;
    display: none;
  }
  #email-inform{
  position: fixed;
  z-index: 3002;
  background: #fff;
  margin: auto;
  padding:15px 0px 0px 0px;
  box-shadow: 0px 0px 30px #000;
  top: 0; left: 0; bottom: 0; right: 0;
  display: none;
  height: 140px;
}
#telegram-inform{
  position: fixed;
  z-index: 3002;
  background: #fff;
  margin: auto;
  padding:15px 0px 0px 0px;
  box-shadow: 0px 0px 30px #000;
  top: 0; left: 0; bottom: 0; right: 0;
  display: none;
  height: 220px;
}
#email-inform > div > div > a > div{
  text-align: center;
}
#email-inform > div > div > a{
  text-decoration: none;
}
#email-inform > div > div > a > div > img{
  margin: 2px auto;
}
#telegram-inform a{
  background:#006df0;display:block;margin:0px auto;color:#fff;padding:15px 25px;text-align:center;width:100%;position:absolute;bottom:0px;font-size:large;
  transition: 0.3s;
}
#telegram-inform a:hover{
  background: #0033cc;
}
.hotmail-social{
  background:#006df0;
  transition: 0.3s;
}
.gmail-social{
  background:#E94132;
  transition: 0.3s;
}
.yahoo-social{
  background:#933EC5;
  transition: 0.3s;
}

.hotmail-social:hover{
  background:#1060BF;
}
.gmail-social:hover{
  background:#DD2818;
}
.yahoo-social:hover{
  background:#7F2AB2;
}
#prev-get-phone-span{
  direction: ltr;
}
  </style>
  </head>
  <body dir="rtl">
<nav class="navbar navbar-inverse navbar-fixed-top" style="background:#297dce;border-color:#297dce;width:100%;margin:0px;">
  <div class="container-fluid">
    <div class="navbar-header" style="float:right;">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#" style="color:#fff;">@lang('main.dahlia')</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav" style="float:right;">
        <li><a href="#" style="color:#fff;">@lang('main.tickets')</a></li>
        <li><a href="#" style="color:#fff;">@lang('main.support')</a></li>
        <li><a href="/fa/sales" style="color:#fff;">@lang('main.sales')</a></li>
        <li class="active"><a href="{{asset('/fa/')}}">@lang('main.home')</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <!--<li><a href="#"><span class="glyphicon glyphicon-star"></span> Sign Up</a></li>
        <li><a href="#"><span class="glyphicon glyphicon-plus"></span> Login</a></li>-->
      </ul>
    </div>
  </div>
</nav>
<div class="container" style="padding-top:10px;">


    <div class="row" style="padding:2px 1.3% 0px 1.3%;margin-bottom:10px;margin-top:5px;font-size:small;">
      <img src="{{asset('asset/image/icon/home-1.png')}}" width="24" style="vertical-align:middle;">
      <img src="{{asset('asset/image/icon/next.png')}}" width="15" style="vertical-align:middle;margin:0px 4px;transform:rotate(180deg);">
      <span style="color:#297dce;vertical-align:middle;">@lang('main.asia')</span>
      <img src="{{asset('asset/image/icon/next.png')}}" width="15" style="vertical-align:middle;margin:0px 4px;transform:rotate(180deg);">
      <span style="color:#297dce;vertical-align:middle;">@lang('main.iran')</span>
      <img src="{{asset('asset/image/icon/next.png')}}" width="15" style="vertical-align:middle;margin:0px 4px;transform:rotate(180deg);">
      <span style="color:#297dce;vertical-align:middle;">@lang('main.isfahan')</span>
      <div style="float:left;margin-top:-4px;" id="order-box">
      <img src="{{asset('asset/image/icon/like-4.png')}}" width="25" style="vertical-align:middle;margin:0px 4px;">
      <img src="{{asset('asset/image/icon/share.png')}}" width="25" style="vertical-align:middle;margin:0px 4px;">
      </div>
      <hr style="margin:5px 0px 0px 0px;padding:0px;">
    </div>


        <div class="row">
                    <div class="col-md-8 col-sm-8 col-xs-12" id="preview-main" style="float:right;">
            <?php if($adv->Pic != "0,0,0"){?>
            <div class="main" style="background:#eaeaea;padding:3px;border-radius:5px;direction:ltr;" id="preview-gallery">
              <ul id="imageGallery" style="display:block;list-style:none;">
                <?php
                if(strpos($adv->Pic, ",") !== false){
                  $var = explode(",",$adv->Pic);
                  foreach ($var as $key => $value) {
                    if($value != "0"){
                ?>
                <li data-thumb="/image/s/{{$value}}.jpg">
                  <img src="/image/s/{{$value}}.jpg" height="300" style="display:block;margin:0px auto;"/>
                </li>
                <?php
                    }
                  }
                }else{
                ?>
                <li data-thumb="/image/s/{{$value}}.jpg" data-src="/image/s/{{$value}}.jpg">
                  <img src="/image/s/{{$value}}.jpg" height="300" style="display:block;margin:0px auto;"/>
                </li>
                <?php
                }
                ?>
              </ul>
            </div>
            <?php } ?>
          

          <div style="background:#fff;margin-top:10px;" id="preview-content">
            <h2 style="padding:3px 5px;margin-top:0px;">
            <b>
            {{$adv->Sub}}
            </b>
            </h2>
            <div style="position:relative;width:48%;display:inline-block;">
              <span style="color:#297dce;font-size:large;">@lang('advertise.price')</span>
              <span style="color:#13126E;position:absolute;left:0px;" class="val">{{NumberFormatting::Arabic_w2e($adv->Price)}}<span style="padding:0px 10px;">@lang('advertise.toman')</span></span>
              <hr style="margin:5px 0px 0px 0px;">
            </div>
            <div style="position:relative;width:48%;display:none;">
              <span style="color:#297dce;font-size:large;">@lang('advertise.trust')</span>
              <span style="color:#13126E;position:absolute;left:0px;" class="val">۲۳۲۴۴<span style="padding:0px 10px;">@lang('advertise.toman')</span></span>
              <hr style="margin:5px 0px 0px 0px;">
            </div>
            <div style="padding-left:10px;padding:5px 0px;">
              <img src="{{asset('asset/image/icon/clock.png')}}" width="16" style="vertical-align:middle;">
              <span style="vertical-align:middle;padding:1px 3px;color:#606060;font-size:smaller;">لحظاتی پیش</span>
            </div>
            <hr style="margin:0px;">
            <p style="padding:8px 10px;">
              {{$adv->Content}}
            </p>
          </div>
          <div class="alert alert-warning" style="position:relative;top:10px;">
            <strong>هشدار!</strong> لطفا به آگهی ها اطمینان نکرده و پرداخت وجه را به صورت حضوری انجام دهید
          </div>
          </div>


          <div style="position:absolute;left:75px;width:28%;" id="profilebox-parent">
            <div id="profilebox" class="col-md-12 col-sm-12 col-xs-12" style="background:#eaeaea;padding:10px 10px 5px 5px;border:1px #eaeaea solid;margin-bottom:20px;border-radius:5px;">
            <center>
              <img src="{{asset('asset/image/icon/social/user.png')}}" class="img-responsive" width="90">
              <h3>@lang('advertise.ownerinform')</h3>
              <button type="button" class="btn btn-labeled btn-primary col-sm-12 col-xs-12" style="border-radius:0px;position:relative;margin:3px 3px;border:none;padding:7px 0px;direction:ltr;" id="prev-get-phone"><span class="btn-label"><img src="{{asset('asset/image/icon/call.png')}}"></span><span id="prev-get-phone-span" style="direction:ltr;">@lang('advertise.call')</span></button>
              <button type="button" class="btn btn-labeled btn-primary col-sm-12 col-xs-12" style="border-radius:0px;position:relative;margin:3px 3px;border:none;padding:7px 0px;" id="prev-get-mail"><span class="btn-label"><img src="{{asset('asset/image/icon/email.png')}}"></span>@lang('advertise.sendmail')</button>
              <button type="button" class="btn btn-labeled btn-primary col-sm-12 col-xs-12" style="border-radius:0px;position:relative;margin:3px 3px;border:none;padding:7px 0px;" id="prev-get-telegram"><span class="btn-label"><img src="{{asset('asset/image/icon/social/telegram.png')}}" width="24" height="24"></span>@lang('advertise.telusername')</button>
              <button type="button" class="btn btn-labeled btn-primary col-sm-12 col-xs-12" style="border-radius:0px;position:relative;margin:3px 3px;border:none;padding:7px 0px;"><span class="btn-label"><img src="{{asset('asset/image/icon/mail.png')}}"></span>@lang('advertise.sendticket')</button>
            </center>
            </div>
            
            <div style="background:#fff;margin-bottom:20px;" id="preview-req">
          <div class="row" id="preview-price">
          </div>
          <hr style="margin:0px;">

            <div style="padding-left:10px;position:relative;padding:5px 0px;">
              <img src="{{asset('asset/image/icon/placeholder-1.png')}}" width="18" style="vertical-align:middle;">
              <span style="vertical-align:middle;padding:1px 3px;color:#606060;">مکان ها : ایران ،اصفهان</span>
              <button class="btn btn-order-active" style="position:absolute;left:0px;top:0px;padding:10px 2px 10px 2px;">
                <img src="{{asset('asset/image/icon/next-1.png')}}" width="15" style="vertical-align:middle;display:block;transform:rotate(90deg);">
              </button>
            </div>
            
          <hr style="margin:0px;">


            
        </div>
          </div>


        </div>
      </div>


      <div id="email-inform" class="col-md-4 col-sm-8 col-xs-10 col-md-offset-3 col-sm-offset-4 col-xs-offset-1">
    <div style="position:relative;width:100%;height:100%;">
    <h3 style="width:100%;text-align:center;">email@mail.com</h2>
      <div style="width:100%;position:absolute;bottom:0px;">
        <a href="#" target="_blank"><div class="col-md-4 col-sm-4 col-xs-4 hotmail-social"><img src="{{asset('asset/image/icon/social/hotmail.png')}}" width="40" height="40"><h6 style="color:#fff;font-family:'Lato';font-weight:300;margin:2px;">Hotmail</h6></div></a>
        <a href="#" target="_blank"><div class="col-md-4 col-sm-4 col-xs-4 gmail-social"><img src="{{asset('asset/image/icon/social/gmail.png')}}" width="40" height="40"><h6 style="color:#fff;font-family:'Lato';font-weight:300;margin:2px;">Gmail</h6></div></a>
        <a href="#" target="_blank"><div class="col-md-4 col-sm-4 col-xs-4 yahoo-social"><img src="{{asset('asset/image/icon/social/yahoo.png')}}" width="40" height="40"><h6 style="color:#fff;font-family:'Lato';font-weight:300;margin:2px;">Yahoo</h6></div></a>
      </div>
    </div>
    </div>
    <div id="telegram-inform" class="col-md-4 col-sm-8 col-xs-10 col-md-offset-3 col-sm-offset-4 col-xs-offset-1">
      <div style="position:relative;width:100%;height:100%;">
        <img src="{{asset('asset/image/icon/social/telegram-logo.png')}}" width="80" style="display: block;margin:0 auto;">
        <h3 style="width:100%;text-align:center;direction:ltr;">@Username</h3>
        <a class="btn" target="_blank" href="https://telegram.me/user">@lang('advertise.sendmsg')</a>
      </div>
    </div>


    <div class="gray-back"></div>


  </body>
  <!--<link href="https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Lato:200?text=123456789$" rel="stylesheet"> -->
</html>
<script type="text/javascript">
(function($) {
  $.fn.showEmailComposeDialog = function(){
    var it = $("div#email-inform");
    it.show();
    var email = "{{$adv->email}}";
    it.find(".gmail-social").parent().attr("href","https://mail.google.com/mail/?view=cm&fs=1&to=" + email);
    it.find(".hotmail-social").parent().attr("href","mailto:" + email);
    it.find(".yahoo-social").parent().attr("href","http://compose.mail.yahoo.com/?to=" + email);
    it.find("h3").html(email);
    it.removeClass("animated fadeOutUp");
    it.removeClass("animated fadeInDown").addClass("animated fadeInDown");
    $(".gray-back").css("z-index","3001");
    
    $(".gray-back").fadeIn();
    $(".gray-back").off().click(function(){
      $(this).hideEmailComposeDialog();
    });
  }

  $.fn.hideEmailComposeDialog = function(){
    var it = $("div#email-inform");
    it.removeClass("animated fadeInDown");
    it.removeClass("animated fadeOutUp").addClass("animated fadeOutUp");
    $(".gray-back").fadeOut(1000,function(){
      it.hide();
    });
    $(".gray-back").css("z-index","999");
  }

  $.fn.showTelegramComposeDialog = function(){
    var it = $("div#telegram-inform");
    it.show();
    var telegram = "{{$adv->telegram}}";
    telegram_o = telegram.substring(1);
    it.find("h3").html(telegram);
    it.find("a").attr("href","https://telegram.me/" + telegram_o);
    it.removeClass("animated fadeOutUp");
    it.removeClass("animated fadeInDown").addClass("animated fadeInDown");
    $(".gray-back").css("z-index","3001");
    $(".gray-back").fadeIn();
    $(".gray-back").off().click(function(){
      $(this).hideTelegramComposeDialog();
    });
  }

  $.fn.hideTelegramComposeDialog = function(){
    var it = $("div#telegram-inform");
    it.removeClass("animated fadeInDown");
    it.removeClass("animated fadeOutUp").addClass("animated fadeOutUp");
    $(".gray-back").fadeOut(1000,function(){
      it.hide();
    });
    $(".gray-back").css("z-index","999");
  }
}(jQuery));

$(document).ready(function() {
  $("#prev-get-telegram").click(function () {
    $(this).showTelegramComposeDialog();
  });
  $("#prev-get-mail").click(function () {
    $(this).showEmailComposeDialog();
  });
  $("#prev-get-phone").click(function() {
    $("#prev-get-phone-span").html("{{$adv->Phone}}");
  });
  $('#imageGallery').lightSlider({
            gallery:true,
            item:1,
            loop:false,
            thumbItem:6,
            slideMargin:0,
            enableDrag: false,
            currentPagerPosition:'left',
            onSliderLoad: function(el) {
               el.lightGallery({
                    selector: '#imageGallery .lslide'
                });
            }  
        });

  $(window).bind('scroll',chk_scroll);
  function chk_scroll(e){
    if(window.scrollY > 53){
      $("#profilebox-parent").css("position","fixed");
      $("#profilebox-parent").css("top","60px");
    }else{
      $("#profilebox-parent").css("position","absolute");
      $("#profilebox-parent").css("top","108px");
    }
  }
});
</script>