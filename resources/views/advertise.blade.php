<?php if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) ob_start('ob_gzhandler'); else ob_start(); ?>
@include('js-localization::head')
<!DOCTYPE html>
<html lang="en">
  <head>
    @yield('js-localization.head')
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <title>Dahlia</title>
    <style type="text/css">
          @font-face {
  font-family: 'Lato';
  font-style: normal;
  font-weight: 400;
  src: local('Lato Regular'), local('Lato-Regular'), url({{asset('asset/font/UyBMtLsHKBKXelqf4x7VRQ.woff2')}}) format('woff2');
  unicode-range: U+0100-024F, U+1E00-1EFF, U+20A0-20AB, U+20AD-20CF, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Lato';
  font-style: normal;
  font-weight: 400;
  src: local('Lato Regular'), local('Lato-Regular'), url({{asset('asset/font/1YwB1sO8YE1Lyjf12WNiUA.woff2')}}) format('woff2');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215, U+E0FF, U+EFFD, U+F000;
}
/* latin-ext */
@font-face {
  font-family: 'Yanone Kaffeesatz';
  font-style: normal;
  font-weight: 400;
  src: local('Yanone Kaffeesatz Regular'), local('YanoneKaffeesatz-Regular'), url({{asset('asset/font/YDAoLskQQ5MOAgvHUQCcLV83L2yn_om9bG0a6EHWBso.woff2')}}) format('woff2');
  unicode-range: U+0100-024F, U+1E00-1EFF, U+20A0-20AB, U+20AD-20CF, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Yanone Kaffeesatz';
  font-style: normal;
  font-weight: 400;
  src: local('Yanone Kaffeesatz Regular'), local('YanoneKaffeesatz-Regular'), url({{asset('asset/font/YDAoLskQQ5MOAgvHUQCcLfGwxTS8d1Q9KiDNCMKLFUM.woff2')}}) format('woff2');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215, U+E0FF, U+EFFD, U+F000;
}
    </style>
  <script type="text/javascript">
  var baseUrl = "{{substr(asset(''), 0, strlen(asset('')) - 1)}}";
  var timer;
  var times = 90;
  var slider = null;
  var isSliderLoaded = false;
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-80283379-1']);
_gaq.push(['_setDomainName', 'none']);
_gaq.push(['_setAllowLinker', 'true']);
_gaq.push(['_trackPageview']);

(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>
  <link rel="stylesheet" href="{{asset('asset/css/bootstrap.min.css')}}">
  <script src="{{asset('asset/js/jquery.min.js')}}"></script>
  <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>-->
  <script src="{{asset('asset/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('asset/js/sweetalert2.min.js')}}"></script>
  <link rel="stylesheet" href="{{asset('asset/css/animate.css')}}">
  <!--<script src="//oss.maxcdn.com/jquery.form/3.50/jquery.form.min.js"></script>-->
  <link rel="stylesheet" href="{{asset('asset/css/sweetalert2.min.css')}}">
  <link rel="stylesheet" href="{{asset('asset/css/main.css')}}">
  <link rel="stylesheet" href="{{asset('asset/css/lightslider.min.css')}}">
  <script type="text/javascript" src="{{asset('asset/js/lightslider.min.js')}}"></script>
  <!--<script src="{{asset('asset/js/dialogbox.js')}}"></script>-->
  <script type="text/javascript" src="{{asset('asset/js/main.js')}}"></script>
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/flaticon.css')}}"> 
  <script type="text/javascript" src="{{asset('asset/js/jquery.timer.js')}}"></script>
  </head>
  <body>

<nav class="navbar navbar-inverse navbar-fixed-top" style="background:#297dce;border-color:#297dce;box-shadow: 0px 0px 3px #000;">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#" style="color:#fff;">Dahlia</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="{{asset('')}}">Home</a></li>
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="color:#fff;">Main page<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Profile</a></li>
            <li><a href="#">Profile</a></li>
            <li><a href="#">Support</a></li>
          </ul>
        </li>
        <li><a href="#" style="color:#fff;">Tickets</a></li>
        <li><a href="#" style="color:#fff;">Support</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-star"></span> Sign Up</a></li>
        <li><a href="#"><span class="glyphicon glyphicon-plus"></span> Login</a></li>
      </ul>
    </div>
  </div>
</nav>
    <div class="container">
      <div class="row" id="advertise-row">
        <div class="col-md-6 col-sm-8 col-xs-12">
          <form role="form" method="get" enctype="multipart/form-data" action="http://localhost/blog/public/postadd" id="advertise">
            <div class="form-group">
              <div class="m-group">
                <label>Subject</label> 
                <input type="text" placeholder="Enter subject at least 20 character" id="subject">
                <span class="highlight"></span>
                <span class="bar" data-color="#ff0000"></span>
              </div>
              <span style="color:green;position:absolute;bottom:-9px;right:10px;background:#fff;border-radius:10px;font-size:13px;padding:0px 5px;font-family:'Lato';box-shadow:0px 0px 3px #fff;">200</span>
            </div>
            <span class="error" style="color:#ff0000;font-size:small;position:relative;top:-5px;left:10px;display:none;">Subject cant be empty and must be at least 10 character and cant be more than 200</span>
            <div class="form-group">
              <div class="m-group">
                <label>Content</label> 
                <textarea rows="3" placeholder="Enter the content" id="content">hello</textarea>
                <span class="highlight"></span>
                <span class="bar"></span>
              </div>
              <span style="color:green;position:absolute;bottom:-9px;right:10px;background:#fff;border-radius:10px;font-size:13px;padding:0px 5px;font-family:'Lato';box-shadow:0px 0px 3px #fff;">3000</span>
            </div>
            <span class="error" style="color:#ff0000;font-size:small;position:relative;top:-5px;left:10px;display:none;">Content cant be empty and must be at least 20 character and cant be more than 3000 character</span>
            <div class="form-group">
              <button type="button" id="selectInform" class="btn btn-dialog">Setting Information to show<img class="img-responsive" src="{{asset('asset/image/icon/down-black.png')}}"/></button>
            </div>
            <div class="form-group">
              <label>Enable ticket message:</label>
              <div class="material-switch pull-right">
                <input id="ticketenable" name="ticketenable" type="checkbox" value="enable"/>
                <label for="ticketenable" class="label-primary"></label>
              </div>
            </div>
            <div class="form-group">
              <label>Show it as Starred Advertise:</label>
              <div class="material-switch pull-right">
                <input id="staradvertise" name="staradvertise" type="checkbox"/>
                <label for="staradvertise" class="label-primary"></label>
              </div>
            </div>
            <div class="form-group">
              <button type="button" id="selectCategory" class="btn btn-dialog"><span>Select Category to show</span><img class="img-responsive" src="{{asset('asset/image/icon/down-black.png')}}"/></button>
            </div>
            <div class="form-group" id="price">
                <label for="price">Price:</label>
                <div class="input-group" style="z-index:1;">
                <input value="" class="form-control" id="price" name="price" type="text" style="font-family:Lato;" placeholder="Price">
                <div class="input-group-btn">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"> <span id="moneysymbol" style="font-family:Lato;">$</span>
                      <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu" style="">
                        <li data-value="1"><a href="#" onclick="$('#moneysymbol').html('$');$('#moneysymbol1').html('$');$('input#moneytype').val('dollar');">Dollar</a>
                        </li>
                        <li data-value="2"><a href="#" onclick="$('#moneysymbol').html('&euro;');$('#moneysymbol1').html('&euro;');$('input#moneytype').val('euro');">Euro</a>
                        </li>
                        <li data-value="3"><a href="#" onclick="$('#moneysymbol').html('Toman');$('#moneysymbol1').html('Toman');$('input#moneytype').val('toman');">Toman</a>
                        </li>
                    </ul>
                  </div>
                </div>
            </div>
            <div class="form-group" id="trust">
                <label for="trust">Trust:</label>
                <div class="input-group" style="z-index:0;">
                <input value="" class="form-control" id="trust" name="trust" type="text" style="font-family:Lato;" placeholder="Trust">
                <div class="input-group-btn">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"> <span id="moneysymbol1" style="font-family:Lato;">$</span>
                      <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu" style="">
                        <li data-value="1"><a href="#" onclick="$('#moneysymbol').html('$');$('#moneysymbol1').html('$');$('input#moneytype').val('dollar');">Dollar</a>
                        </li>
                        <li data-value="2"><a href="#" onclick="$('#moneysymbol').html('&euro;');$('#moneysymbol1').html('&euro;');$('input#moneytype').val('euro');">Euro</a>
                        </li>
                        <li data-value="3"><a href="#" onclick="$('#moneysymbol').html('Toman');$('#moneysymbol1').html('Toman');$('input#moneytype').val('toman');">Toman</a>
                        </li>
                    </ul>
                  </div>
                </div>
            </div>
            <div class="form-group" id="gender">
              <label for="trust">Gender:</label>
              <label class="radio-inline"><input type="radio" name="optradio" id="malegender">Male</label>
              <label class="radio-inline"><input type="radio" name="optradio" id="femalegender">Female</label>
            </div>
            <div class="form-group">
              <button type="button" id="selectadvertisetime" class="btn btn-dialog">Setting Times to show Advertise (Default 1 week free)<img class="img-responsive" src="{{asset('asset/image/icon/down-black.png')}}"/></button>
            </div>
            <input name="_token" value="{!! csrf_token() !!}"  type="hidden" />
            <input name="locationlength" id="locationlength" type="hidden" value="0" />
            <input name="category" id="categoryinput" type="hidden" value="0" />
            <input name="moneytype" id="moneytype" type="hidden" value="dollar" />
            <input name="rq" id="rq" type="hidden" value="" />
            <input name="pic1" id="pic1" type="hidden" value="0" />
            <input name="pic2" id="pic2" type="hidden" value="0" />
            <input name="pic3" id="pic3" type="hidden" value="0" />
          </form>
        </div>
        <div class="col-md-6 col-sm-4 col-xs-12" id="secondrow">
        <div class="col-md-12 col-sm-12 col-xs-12" id="imageuploadbox">
          <h3>
          Picture of advertise
          </h3>
          <p>
          Please select at least a picture to show on your advertise. It will help another user to find your advertise more fast!
          </p>
          <div class="row">
            <div class="col-md-4 col-sm-12 col-xs-4">
              <div id="image-holder" class="image-holder deactive">
                <input id="fileselector1" name="photo" type="file" style="display:none;">
                <img class="img-responsive" src="{{asset('asset/image/icon/photo.png')}}"/>
              </div>
            </div>
            <div class="col-md-4 col-sm-12 col-xs-4">
              <div id="image-holder" class="image-holder deactive">
                <input id="fileselector2" name="photo" type="file" style="display:none;">
                <img class="img-responsive" src="{{asset('asset/image/icon/photo.png')}}" />
              </div>
            </div>
            <div class="col-md-4 col-sm-12 col-xs-4">
              <div id="image-holder" class="image-holder deactive"> 
                <input id="fileselector3" name="photo" type="file" style="display:none;">
                <img class="img-responsive" src="{{asset('asset/image/icon/photo.png')}}" />
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12" style="padding:0px;">



          <div class="form-group" style="margin-bottom:5px;">
              <label for="loc">Advertise Location(s):</label>
              <div class="alert alert-warning" style="margin-bottom:5px;border-radius:0px;">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Warning!</strong> If you have logged in and have posted an advertise for a special country, You cant post another advertise for another country for free!
              </div>
              <div class="row" id="location">
              <div class="row-height">
                <div class="col-md-8 col-sm-8 col-xs-12 col-md-height col-sm-height col-top">
                <div class="inside">
                  <div class="well well-sm inside-full-height" id="locationlist" style="border-radius:0px;">
                      <span>Please select add new City, State, Country and Continent to add location!</span>
                  </div>
                </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12 col-md-height col-sm-height col-top">
                <div class="inside">
                  <div class="form-group inside-full-height" style="padding:0px;margin:0px;" id="locationtoolbox">
                    <button type="button" class="btn btn-labeled btn-primary col-sm-12 col-xs-12" id="addcity" style="border-radius:0px;"><span class="btn-label"><i>+</i></span>Add new City<span class="badge">free</span></button>
                    <button type="button" class="btn btn-labeled btn-primary col-sm-12 col-xs-12" id="addstate" style="border-radius:0px;"><span class="btn-label"><i>+</i></span>Add new State<span class="badge">1$</span></button>
                    <button type="button" class="btn btn-labeled btn-primary col-sm-12 col-xs-12" id="addcountry" style="border-radius:0px;"><span class="btn-label"><i>+</i></span>Add new Country<span class="badge">5$</span></button>
                    <button type="button" class="btn btn-labeled btn-primary col-sm-12 col-xs-12" id="addcontinent" style="border-radius:0px;"><span class="btn-label"><i>+</i></span>Add new Continent<span class="badge">15$</span></button>
                  </div>
                </div>
                </div>
              </div>
              </div>
            </div>


        </div>
      </div>
      </div>
      <div style="width:50%;text-align:center;"><button class="btn btn-primary" style="font-size:large;border-radius:0px;padding:7px 60px;" id="submitmainform">submit</button></div>
    </div>
    </div>



    <!--DEMO01-->
    <div id="StateModal" class="dialogLocation">
        <!--THIS IS IMPORTANT! to close the modal, the class name has to match the name given on the ID  class="close-animatedModal" -->
        <div class="close-animatedModal"> 
          <div class="closebt">
            <img class="" src="{{asset('asset/image/icon/error.svg')}}">
          </div>
        </div>
        <div class="modal-content">
          <div class="col-sm-12" id="row-map-container">
            <div class="row">
              <div class="col-sm-6 col-xs-12" id="list">
              </div>
              <div class="col-sm-6 col-xs-12" id="mmap">
                <img src-svg="" src-dir="{{asset('asset/image/svg/country/')}}" class="svg img-responsive" id="mapsvg" src-list="state/ir.json">
                <span><img class="spinner" src="{{asset('asset/image/icon/loader.png')}}"><span style="vertical-align:middle;">Loading map</span></span>
              </div>
            </div>
          </div>
        </div>
    </div>

    <div id="ContinentModal" class="dialogLocation">
        <!--THIS IS IMPORTANT! to close the modal, the class name has to match the name given on the ID  class="close-animatedModal" -->
        <div class="close-animatedModal"> 
          <div class="closebt">
            <img class="" src="{{asset('asset/image/icon/error.svg')}}">
          </div>
        </div>
        <div class="modal-content">
          <div class="col-sm-12" id="row-map-container">
            <div class="row">
              <div class="col-sm-3 col-xs-12" id="list">
              </div>
              <div class="col-sm-9 col-xs-12" id="mmap">
                <img src-svg="{{asset('asset/image/svg/world/continent.svg')}}" class="svg img-responsive" id="mapsvg" src-list="continent/all.json">
                <span><img class="spinner" src="{{asset('asset/image/icon/loader.png')}}"><span style="vertical-align:middle;">Loading map</span></span>
              </div>
            </div>
          </div>
        </div>
    </div>
    <div id="CountryModal" class="dialogLocation">
        <!--THIS IS IMPORTANT! to close the modal, the class name has to match the name given on the ID  class="close-animatedModal" -->
        <div class="close-animatedModal">
          <div class="closebt">
            <img class="" src="{{asset('asset/image/icon/error.svg')}}">
          </div>
        </div>
        <div class="modal-content">
          <div class="col-sm-12" id="row-map-container">
            <div class="row">
              <div class="col-sm-6 col-xs-12" id="list">
              </div>
              <div class="col-sm-6 col-xs-12" id="mmap">
                <img src-svg="" src-dir="{{asset('asset/image/svg/continents/')}}" class="svg img-responsive" id="mapsvg" src-list="">
                <span><img class="spinner" src="{{asset('asset/image/icon/loader.png')}}"><span style="vertical-align:middle;">Loading map</span></span>
              </div>
            </div>
          </div>
        </div>
    </div>
    <div class="gray-back"></div>
    <div id="category" class="col-md-4 col-sm-8 col-xs-10 col-md-offset-3 col-sm-offset-4 col-xs-offset-1">
      <div id="category-header">
        <h4>
          <img src="{{asset('asset/image/icon/back-red.png')}}"><span>Select Category:</span>
        </h4>
        <img src="{{asset('asset/image/icon/error.png')}}" width="25" height="25">
      </div>
      <div id="category-content">
        <ul>
        </ul>
        <img class="spinner" src="{{asset('asset/image/icon/loader-black.png')}}">
      </div>
    </div>
    <div id="email-inform" class="col-md-4 col-sm-8 col-xs-10 col-md-offset-3 col-sm-offset-4 col-xs-offset-1">
    <div style="position:relative;width:100%;height:100%;">
    <h3 style="width:100%;text-align:center;">email@mail.com</h2>
      <div style="width:100%;position:absolute;bottom:0px;">
        <a href="#" target="_blank"><div class="col-md-4 col-sm-4 col-xs-4 hotmail-social"><img src="{{asset('asset/image/icon/social/hotmail.png')}}" width="40" height="40"><h6 style="color:#fff;font-family:'Lato';font-weight:300;margin:2px;">Hotmail</h6></div></a>
        <a href="#" target="_blank"><div class="col-md-4 col-sm-4 col-xs-4 gmail-social"><img src="{{asset('asset/image/icon/social/gmail.png')}}" width="40" height="40"><h6 style="color:#fff;font-family:'Lato';font-weight:300;margin:2px;">Gmail</h6></div></a>
        <a href="#" target="_blank"><div class="col-md-4 col-sm-4 col-xs-4 yahoo-social"><img src="{{asset('asset/image/icon/social/yahoo.png')}}" width="40" height="40"><h6 style="color:#fff;font-family:'Lato';font-weight:300;margin:2px;">Yahoo</h6></div></a>
      </div>
    </div>
    </div>
    <div id="telegram-inform" class="col-md-4 col-sm-8 col-xs-10 col-md-offset-3 col-sm-offset-4 col-xs-offset-1">
      <div style="position:relative;width:100%;height:100%;">
        <img src="{{asset('asset/image/icon/social/telegram-logo.png')}}" width="80" style="display: block;margin:0 auto;">
        <h3 style="width:100%;text-align:center;">@Username</h3>
        <a class="btn" target="_blank" href="https://telegram.me/user">Send message</a>
      </div>
    </div>
    <div id="inform" class="col-md-4 col-sm-8 col-xs-10 col-md-offset-3 col-sm-offset-4 col-xs-offset-1">
      <img src="{{asset('asset/image/icon/error.png')}}" width="25" height="25">
      <div class="cont">
      <div class="form-group">
        <div class="m-group">
          <label>Phone</label> 
          <input type="text" placeholder="+00123456789" id="phone" name="phone" style="font-family: 'Ubuntu', sans-serif;">
          <span class="highlight"></span>
          <span class="bar"></span>
        </div>
      </div>
      <span class="error" style="color:#ff0000;font-size:small;position:relative;left:10px;display:none;">Please enter a valide phone number</span>
      <div class="form-group">
        <div class="m-group">
          <label>Email</label> 
          <input type="text" placeholder="email@example.com" id="email" name="email">
          <span class="highlight"></span>
          <span class="bar"></span>
        </div>
      </div>
      <span class="error" style="color:#ff0000;font-size:small;position:relative;left:10px;display:none;">Please enter a valide mail</span>
      <div class="form-group">
        <div class="m-group">
          <label>Telegram username</label> 
          <input type="text" placeholder="@telegram_username" id="telegram" name="telegram">
          <span class="highlight"></span>
          <span class="bar"></span>
        </div>
      </div>
      <span class="error" style="color:#ff0000;font-size:small;position:relative;left:10px;display:none;">Please enter a valide telegram username</span>
      <div style="width:100%;text-align:center;font-size:large;margin-top:10px;"><button class="btn btn-primary" style="padding:7px 30px;">Submit</button></div>
      </div>
    </div>
    
    <div id="advertise-time" class="col-md-4 col-sm-8 col-xs-10 col-md-offset-3 col-sm-offset-4 col-xs-offset-1">
      <img src="{{asset('asset/image/icon/error.png')}}" width="25" height="25">
      <div class="cont">
      <div class="radio">
        <label><input type="radio" name="timeadvertise" value="default" checked="checked">Default (one week free)</label>
      </div>
      <div class="radio">
        <label><input type="radio" name="timeadvertise" value="custom">Custom time</label>
      </div>
      <div class="form-group">
      <label for="email">Enter number of day to show:</label>
      <div class = "input-group">
         <input type = "text" class = "form-control" id="numday" name="numday">
         <span class = "input-group-addon" style="font-family:Lato;">Price : 0$</span>
      </div>
      </div>
      <div class="radio">
        <label><input type="radio" name="timeadvertise" value="pack">Time pack</label>
      </div>
      <div class="form-group">
      <label for="sel1">Select list (select one):</label>
      <select class="form-control" id="timepack">
        <option value="golden">Golden</option>
        <option value="silver">Silver</option>
      </select>
      </div>
      <div style="width:100%;text-align:center;font-size:large;"><button class="btn btn-primary" style="padding:7px 30px;">Submit</button></div>
      </div>
    </div>



    <div id="login" style="overflow:scroll;">
      <div class="close-animatedModal">
        <div class="closebt">
            <img class="" src="{{asset('asset/image/icon/error.svg')}}">
        </div>
      </div>
      <div id="cont">
        <div class="row" style="width:100%;margin:0px;display:block;">
          <div class="col-md-4 col-sm-6 col-xs-10" style="min-width:250px;max-width:400px;margin:0 auto;display:block;position:absolute;left:0px;right:0px;top:20px;">
            <h1 style="width:100%;text-align:center;">Dahlia</h1>
            <div style="width:100%;box-shadow:0px 1px 2px #808080;background:#fff;height:100%;padding:30px 20px 0px 20px;border-top:3px #297dce solid;overflow:hidden;margin-bottom:30px;position:relative;">
            <div id="timer" style="position:absolute;top:0px;left:0px;right:0px;margin:0 auto;width:60px;display:none;"><h4 style="background:#297dce;padding:0px 0px 5px 0px;border-radius:0px 0px 10px 10px;color:#fff;margin:0px;text-align:center;">1:30</h4></div>
              <img src="{{asset('asset/image/icon/social/user.png')}}" class="img-responsive user" width="100">
              <div id="welcome-header" style="text-align:center;display:none;">
                <h4>Welcome <b>alimohammadi340@gmail.com</b>!</h4>
              </div>
              <div class="receivemail">
                We send a verification code to <br><b>+989162886638</b>
                <img src="{{asset('asset/image/icon/smartphone.png')}}" class="img-responsive" width="55">
              </div>
              <div class="setpass" style="text-align:center;display:none;">
                Thanks for registering! Now please enter a password for tour account
                <!--img src="{{asset('asset/image/icon/smartphone.png')}}" class="img-responsive" width="55"-->
              </div>
              <div class="form-group">
                <div class="m-group">
                <!--label>Subject</label--> 
                <input type="text" placeholder="Enter your phone" id="phone" style="font-family: 'Ubuntu', sans-serif;">
                <span class="highlight"></span>
                <span class="bar" data-color="#ff0000"></span>
                </div>
              </div>
              <span class="error" style="color:#ff0000;font-size:small;position:relative;top:-5px;left:10px;display:none;">Please enter a valide phone</span>
              <div class="form-group" style="display:none;">
                <div class="m-group">
                <!--label>Subject</label--> 
                <input type="text" placeholder="Enter verification code" id="code" style="font-family: 'Ubuntu', sans-serif;">
                <span class="highlight"></span>
                <span class="bar" data-color="#ff0000"></span>
                </div>
              </div>
              <span class="error" style="color:#ff0000;font-size:small;position:relative;top:-5px;left:10px;display:none;">Please enter a valide code</span>
              <div class="form-group" style="display:none;">
                <div class="m-group">
                <!--label>Subject</label--> 
                <input type="password" placeholder="Enter password" id="password">
                <span class="highlight"></span>
                <span class="bar" data-color="#ff0000"></span>
                </div>
              </div>
              <div class="form-group" style="display:none;">
                <div class="m-group">
                <!--label>Subject</label--> 
                <input type="password" placeholder="Enter password again" id="passwordverify">
                <span class="highlight"></span>
                <span class="bar" data-color="#ff0000"></span>
                </div>
              </div>
              <button class="btn btn-primary btn-login" id="login-next">Next</button>
            </div>
          </div>
        </div>
      </div>
    </div>



    <div id="preview" style="overflow:scroll;">
      <div class="close-animatedModal">
        <div class="closebt">
            <img class="" src="{{asset('asset/image/icon/error.svg')}}">
        </div>
      </div>
      <div id="cont" class="col-md-12 col-sm-12 col-xs-12">
        <div class="row" style="padding: 0px  1.5% 0px 1.5%;"><div class="col-md-12 col-sm-12 col-xs-12" style="height:50px;background:#297dce;margin:0px 0px 10px 0px;">
          <h3 style="color:#fff;padding:12px 5px;margin:0px;font-size:20px;">
            Electronics > Moiles > Samsung
          </h3>
        </div></div>
        <div class="row">
          <div class="col-md-8 col-sm-8 col-xs-12" id="preview-main">
            <div class="main" style="background:#fff;padding:3px;" id="preview-gallery">
              <ul id="imageGallery" style="display:block;">
                <!--li data-thumb="{{asset("asset/image/2.jpg")}}" data-src="{{asset("asset/image/2.jpg")}}">
                  <img src="{{asset("asset/image/2.jpg")}}" height="300" style="display:block;margin:0px auto;"/>
                </li>
                <li data-thumb="{{asset("asset/image/3.jpg")}}" data-src="{{asset("asset/image/3.jpg")}}">
                  <img src="{{asset("asset/image/3.jpg")}}" height="300" style="display:block;margin:0px auto;"/>
                </li>
                <li data-thumb="{{asset("asset/image/4.jpg")}}" data-src="{{asset("asset/image/4.jpg")}}">
                  <img src="{{asset("asset/image/4.jpg")}}" height="300" style="display:block;margin:0px auto;"/>
                </li-->
              </ul>
            </div>
          

          <div style="background:#fff;" id="preview-content">
            <h1 style="padding:3px 5px;margin-top:0px;">
            <b>
            the titles
            </b>
            </h1>
            <p style="padding:3px 10px;">the text texttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttext</p>
        </div>
        <div style="background:#fff;padding:10px 30px;margin-bottom:10px;" id="preview-req">
          <div class="row" id="preview-price">
            <span style="color:#297dce;float:left;">Price</span>
            <span style="color:#13126E;float:right;" class="val">65511$</span>
          </div>
          <hr style="border-color:#cacaca;margin:5px;">
          <div class="row" id="preview-trust">
            <span style="color:#297dce;float:left;">Trust</span>
            <span style="color:#13126E;float:right;" class="val">65511$</span>
          </div>
        </div>
          </div>
          <div class="col-md-4 col-md-offset-0 col-sm-offset-0 col-sm-4 col-xs-12">
            <div id="profilebox" class="col-md-12 col-sm-12 col-xs-12">
            <center>
              <img src="{{asset('asset/image/icon/social/user.png')}}" class="img-responsive" width="90">
              <h3>Owner Information</h3>
              <button type="button" class="btn btn-labeled btn-primary col-sm-12 col-xs-12" style="border-radius:0px;position:relative;" id="prev-get-phone"><span class="btn-label"><img src="{{asset('asset/image/icon/call.png')}}"></span><span id="prev-get-phone-span">Call</span></button>
              <button type="button" class="btn btn-labeled btn-primary col-sm-12 col-xs-12" style="border-radius:0px;position:relative;" id="prev-get-mail"><span class="btn-label"><img src="{{asset('asset/image/icon/email.png')}}"></span>Send Mail</button>
              <button type="button" class="btn btn-labeled btn-primary col-sm-12 col-xs-12" style="border-radius:0px;position:relative;" id="prev-get-telegram"><span class="btn-label"><img src="{{asset('asset/image/icon/social/telegram.png')}}" width="24" height="24"></span>Telegram</button>
              <button type="button" class="btn btn-labeled btn-primary col-sm-12 col-xs-12" style="border-radius:0px;position:relative;"><span class="btn-label"><img src="{{asset('asset/image/icon/mail.png')}}"></span>Send Ticket</button>
            </center>
            </div>
            <div id="confirm-box">
            <hr style="height:2px;">
              <div class="form-group">
                <label style="display:inline;">Enable editing advertise after confirm (just for 12 hours)</label>
                <div class="material-switch pull-right">
                  <input id="ticketenable3" name="someSwitchOption003" type="checkbox"/>
                  <label for="ticketenable3" class="label-primary"></label>
                </div>
              </div>
              <br>
              <div class="form-group">
                <button class="btn btn-success col-md-12 col-sm-12 col-xs-12" id="confirm-prev">Confirm</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </body>
  <!--<link href="https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Lato:200?text=123456789$" rel="stylesheet"> -->
</html>

      <?php
        ob_end_flush();
      ?>