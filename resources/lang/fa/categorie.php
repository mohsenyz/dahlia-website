<?php
return [
    'appliances' => 'لوازم خانگی',
    'athletic' => 'لوازم ورزشی',
    'business' => 'کسب و کار',
    'electronics' => 'لوازم الکترونیکی',
    'estate' => 'املاک',
    'mobile' => 'موبایل و تبلت',
    'personal' => 'شخصی',
    'services' => 'خدمات',
    'vehicle' => 'وسایل نقلیه',
    'employment' => 'استخدام',
];
