<?php
return [
    'lastprice' => 'اخرین قیمت',
    'salesavailable' => 'مکان های تحت پوشش حراجی',
    'more' => 'بیشتر',
    'toman' => 'تومان',
    'nothing' => 'چیزی برای نمایش وجود ندارد!',
];
