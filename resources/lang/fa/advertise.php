<?php
return [
    'imageuploadtitle' => 'تصاویر آگهی',
    'imageuploadcontent' => 'لطفا برای آگهی خود حداقل یک تصویر انتخاب کنید. این کار به بهتر دیده شدن آگهی شما کمک می کند',
    'advertiselocation' => 'انتخاب محل (های) آگهی',
    'warning' => 'هشدار!',
    'locationwarning' => 'در صورتی که قبلا به صورت رایگان اقدام به ثبت آگهی برای کشور خاصی نموده اید به صورت رایگان تنها قادرید از همان کشور آگهی ثبت کنید در غیر اینصورت مجبور به پرداخت هزینه ی اضافی خواهید شد!',
    'locationtext' => 'برای افزودن محل آگهی بر روی کلید افزودن شهر استان کشور یا قاره کلیک کنید',
    'addcity' => 'افزودن شهر',
    'addstate' => 'افزودن استان',
    'addcountry' => 'افزودن کشور',
    'addcontinent' => 'افزودن قاره',
    'subject' => 'موضوع',
    'subjectrule' => 'موضوع اجباری بوده و باید حداقل ۱۰ کاراکتر و حداکثر ۲۰۰ کاراکتر باشد',
    'content' => 'متن آگهی',
    'contentrule' => 'متن آگهی اجباری بوده و باید حداقل ۲۰ کاراکتر و حداکثر ۳۰۰۰ کاراکتر باشد',
    'settinginform' => 'تنظیم مشخصات برای نشان دادن در آگهی',
    'enableticket' => 'فعال کردن امکان ارتباط با مشتری از طریق تیکت',
    'urgentenable' => 'نمایش اگهی به صورت آگهی فوری',
    'categorietoshow' => 'انتخاب دسته بندی آگهی',
    'price' => 'قیمت',
    'toman' => 'تومان',
    'dollar' => 'دلار',
    'euro' => 'یورو',
    'free' => 'رایگان',
    'trust' => 'ودیعه',
    'gender' => 'جنسیت',
    'male' => 'مرد',
    'female' => 'زن',
    'settime' => 'تنظیم مدت زمان نمایش آگهی (به صورت پیش فرض یک هفته رایگان)',
    'submit' => 'ثبت آگهی',
    'loadingmap' => 'بارگذاری نقشه',
    'selectcategorie' => 'انتخاب دسته بندی',
    'sendmsg' => 'ارسال پیام',
    'phone' => 'تلفن',
    'phonerule' => 'لطفا یک تلفن معتبر وارد کنید',
    'email' => 'پست الکترونیک',
    'emailrule' => 'لطفا یک پست الکترونیک معتبر وارد کنید',
    'telusername' => 'شناسه تلگرام',
    'telrule' => 'لطفا یک شناسه ی معتبر وارد کنید',
    'defaulttime' => 'پیش فرض (یک هفته رایگان)',
    'timecustom' => 'انتخاب مدت زمان سفارشی',
    'timeenterdaynum' => 'مدت روز نمایش آگهی را وارد کنید',
    'timepack' => 'طرح های زمانی',
    'timeselectpack' => 'یک طرح زمانی انتخاب کنید',
    'timegolden' => 'طلایی (یک ماه)',
    'timesilver' => 'نقره ای (سه ماه)',
    'dahlia' => 'داهلیا',
    'loginwelcome' => 'خوش آمدید',
    'loginsendverify' => 'ما یک پیام برای شما ارسال  کردیم',
    'loginthanks' => 'بابت ثبت نام ممنون! اکنون برای حساب خود یک کلمه ی عبور انتخاب کنید',
    'enterphone' => 'لطفا شماره تلفن خود را وارد کنید',
    'entercode' => 'لطفا کد ارسال شده را وارد کنید',
    'coderule' => 'کد شما معتبر نمی باشد',
    'next' => 'مرحله ی بعد',
    'ownerinform' => 'اطلاعات صاحب آگهی',
    'call' => 'تلفن',
    'sendmail' => 'ارسال نامه الکترونیک',
    'telusername' => 'شناسه تلگرام',
    'sendticket' => 'ارسال تیکت',
    'enableedit' => 'فعال سازی امکان تغییر مجدد آگهی قبل از ارسال نهایی (تنها ۱۲ ساعت مهلت تغییر)',
    'confirm' => 'ثبت نهایی',
    'nothing' => 'چیزی برای نمایش وجود ندارد!',
    'maperror' => 'مشکلی در بارگذاری نقشه به وجود آمد',
    'locmoreerror' => 'شما نمی توانید بیش از ده محل اضافه کنید',
    'locduperror' => 'شما نمی توانید محل تکراری اضافه کنید',
    'filereadererror' => 'مرورگر شما قدیمی بوده و از امکان پیش نمایش تصویر قبل از آپلود پشتیبانی نمی کند. لطفا مرورگر خود را ارتقا دهید!',
    'validimage' => 'لطفا یک تصویر معتبر انتخاب کنید',
    'networkerror' => 'امکان برقراری ارتباط با سرور وجود ندارد لطفا اتصالات خود را بازبینی کرده و مجددا امتحان کنید',
    'validinput' => 'برخی از ورودی ها نا معتبر هستند لطفا آن ها را بررسی و تصحیح کنید',
    'error' => 'یک مشکل!',
    'headerselectcategorie' => 'انتخاب دسته بندی',
    'Iran' => 'ایران',
    'remove' => 'حذف',
    'country' => 'کشور',
    'continent' => 'قاره',
    'state' => 'استان',
];
