<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'appliances' => 'Appliances',
    'athletic' => 'Athletic',
    'business' => 'Business',
    'electronics' => 'Electronics',
    'estate' => "Estate",
    'mobile' => 'Mobile',
    'personal' => 'Personal',
    'services' => "Services",
    'vehicle' => "Vehicle",
    'employment' => "Employment",
];
