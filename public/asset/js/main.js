(function($) {
  
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.fn.loadMap = function( selector, func, func1, func2) {

        return this.each( function() {
          //$(this).children(".modal-content").children("#row-map-container").children(".row").children("#mmap").children("#mapsvg").loadSvg(func);
          var $img = $(this).children(".modal-content").children("#row-map-container").children(".row").children("#mmap").children("#mapsvg");
          var $it = $(this);
          var $mmap = $(this).children(".modal-content").children("#row-map-container").children(".row").children("#mmap");
          var $imgLoader = $(this).children(".modal-content").children("#row-map-container").children(".row").children("#mmap").children("span");
          var $list = $(this).children(".modal-content").children("#row-map-container").children(".row").children("#list");
          var list = $img.attr('src-list');
          $('nav').fadeOut(100);
          $(this).show();
          $(this).removeClass('animated fadeOutUp');
          $(this).addClass('animated fadeInDown');
          $(this).children(".close-animatedModal").children(".closebt").click(function(){
            $it.removeClass('animatesd fadeInDown');
            $it.addClass('animated fadeOutUp');
            $('nav').fadeIn(100);
            $("#CountryModal").children(".modal-content").children("#row-map-container").children(".row").children("#mmap").children("#mapsvg").SvgToImg("country");
            $("#StateModal").children(".modal-content").children("#row-map-container").children(".row").children("#mmap").children("#mapsvg").SvgToImg();
          });
          function hide(){
            $it.removeClass('animatesd fadeInDown');
            $it.addClass('animated fadeOutUp');
            $('nav').fadeIn(100);
          }
          if($img.get(0).tagName == "svg"){
            $imgLoader.hide();
          }else{
            $imgLoader.show();
          }
          $.getJSON( baseUrl + "/asset/json/" + list, function( data ) {
            $('nav').fadeOut(100);
            $imgLoader.hide();
            if(data.length < 10){
              func.call();
              $.each( data, function( ID, obj ) {
                $list.append("<div class=\"col-sm-12 col-xs-12  \">" + "<a href=\"#\" id=\"" + obj.ID +"\">" + obj.Title +"</a></div>");
              });
            }else{
              func.call();
              $.each( data, function( ID, obj ) {
                $list.append("<div class=\"col-sm-4 col-xs-6  \">" + "<a href=\"#\" id=\"" + obj.ID +"\">" + obj.Title +"</a></div>");
              });
            }
            //start loading

            $list.children("div").children("a").click(function(){
              hide();
              func2.call(this,$(this).attr('id'),$(this).html());
            });
            $imgLoader.show();
            $img.loadSvg(function(){
            $imgLoader.hide();
            func1.call();
            if(selector == "g"){
              $mmap.find('g').click(function(){
                hide();
                func2.call(this,$(this).attr('id'),$(this).attr('data-original-title'));
              });
              $list.children("div").children("a").click(function(){
                hide();
                //func2.call(this,$(this).attr('id'),$(this).html());
              });
              $mmap.find('g').tooltip({container:'body'});
              $mmap.find('g').hover(function(){
                $(this).tooltip("show");
                $(this).css("fill",$(this).attr('fill-hover'));
                $list.find("a[id='"+$(this).attr('id')+"']").css({'color': '#fff','background': 'blue'});
              },function(){
                $(this).css("fill","#cacaca");
                $list.find("a[id='"+$(this).attr('id')+"']").css({'color': '#000','background': 'none'});
              });
              $list.children("div").children("a").hover(function(){
                $mmap.find("g[id='"+$(this).attr('id')+"']").tooltip({container:'body'}).tooltip('show');
                $mmap.find("g[id='" + $(this).attr('id') + "']").css("fill",$("g[id='" + $(this).attr('id') + "']").attr('fill-hover'));
                $(this).css({'color': '#fff','background': 'blue'});
              },function(){
                $mmap.find("g[id='"+$(this).attr('id')+"']").tooltip({container:'body'}).tooltip('hide');
                $mmap.find("g[id='" + $(this).attr('id') + "']").css("fill",'#cacaca');
                $(this).css({'color': '#000','background': 'none'});
              });
            }else{
              var color = $mmap.find("svg").attr("fill-hover");
              if(color == null){
                color = "#ff0000";
              }
              $mmap.find('path').click(function(){
                hide();
                func2.call(this,$(this).attr('id'),$(this).attr('data-original-title'));
              });
              $list.children("div").children("a").click(function(){
                hide();
                //func2.call(this,$(this).attr('id'),$(this).html());
              });
              $mmap.find('path').tooltip({title:'NoThing!', container:'body'});
              $mmap.find('path').hover(function(){
                $(this).tooltip("show");
                $(this).css("fill",color);
                $list.find("a[id='"+$(this).attr('id')+"']").css({'color': '#fff','background': 'blue'});
              },function(){
                $(this).css("fill","#cacaca");
                $list.find("a[id='"+$(this).attr('id')+"']").css({'color': '#000','background': 'none'});
              });
              $list.children("div").children("a").hover(function(){
                $mmap.find("path[id='"+$(this).attr('id')+"']").tooltip({container:'body'}).tooltip('show');
                $mmap.find("path[id='" + $(this).attr('id') + "']").css("fill",color);
                $(this).css({'color': '#fff','background': 'blue'});
              },function(){
                $mmap.find("path[id='"+$(this).attr('id')+"']").tooltip({container:'body'}).tooltip('hide');
                $mmap.find("path[id='" + $(this).attr('id') + "']").css("fill",'#cacaca');
                $(this).css({'color': '#000','background': 'none'});
              });
            }
          },function(){
            $imgLoader.children("img").hide();
            $imgLoader.children("span").html(Lang.get('advertise.maperror'));
          });
            //inish loadimg

          });
        });
    }
    $.fn.loadSvg = function(func,func1) {
      var $img = $(this);
      var imgID = $img.attr('id');
      var imgClass = $img.attr('class');
      var imgURL = $img.attr('src-svg');
      if($(this).get(0).tagName == "svg"){
        func.call();
        var $imgLoader = $("#ContinentModal").children(".modal-content").children("#row-map-container").children(".row").children("#mmap").children("span").hide();
        alert("tag exist");
      }{
      $.get(imgURL, function(data,status) {
        if(status != "success"){
          func1.call();
        }else{
        var $svg = $(data).find('svg');
        if(typeof imgID !== 'undefined') {
          $svg = $svg.attr('id', imgID);
        }
        if(typeof imgClass !== 'undefined') {
          $svg = $svg.attr('class', imgClass+' replaced-svg');
        }
        $svg = $svg.removeAttr('xmlns:a');
        if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
            $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
        }
        $img.replaceWith($svg);
        func.call();
        }
      });
    }
    }
    $.fn.SvgToImg = function(str) {
      var srclist;
      var srcdir;
      if(str == "country"){
        var srcdir = baseUrl + "/asset/image/svg/continents";
        var img = $("<img>").attr("class",$(this).attr("class")).attr("id",$(this).attr("id")).attr("src-dir",srcdir);
        $(this).parent().prev().html("");
        $(this).replaceWith(img);
      }else if(str == "state"){
        var srcdir = baseUrl + "/asset/image/svg/country";
        var img = $("<img>").attr("class",$(this).attr("class")).attr("id",$(this).attr("id")).attr("src-dir",srcdir);
        $(this).parent().prev().html("");
        $(this).replaceWith(img);
      }else{
        var img = $("<img>").attr("class",$(this).attr("class")).attr("id",$(this).attr("id")).attr("src-svg",baseUrl + "/asset/image/svg/world/continent.svg").attr("src-list","continent/all.json");
        $(this).parent().prev().html("");
        $(this).replaceWith(img);
      }
    }
    $.fn.addLocation = function(str,str1,str2) {
      if($("#locationlist").issetInList(str) != true){
      var lengthobj = $("input#locationlength");
      $(this).children("span").hide();
      var thislen = parseInt(lengthobj.val()) + 1;
      var item = $("<div class=\"form-group locationlistitem " + str1.toLowerCase() + "\">").attr("id","item" + thislen);
      item.append($("<label></label>").html("" + thislen));
      item.append($("<input type=\"hidden\">").attr("name","locationitem" + thislen).attr("value",str));
      item.append($("<label class=\"label label-primary\">" + Lang.get('advertise.' + str1.toLowerCase()) + "</label>"));
      item.append($("<span>" + str2 + "</span>"));
      item.append($("<div>").append("<button type=\"button\" class=\"btn btn-danger\" onclick=\"$(this).parent('div').parent('div').removeLocation();\">" + Lang.get('advertise.remove') + "</button>"));
      lengthobj.val("" + thislen);
      if($("#locationlist").getListLength() != 10){
        $(this).append(item);
      }else{
        //alert("You cant add more than 10 locations");
        swal({title:Lang.get('advertise.error'),text:Lang.get('advertise.locmoreerror'),confirmButtonColor: "#c9302c"});
      }
      item.hide();
      item.fadeIn(300);
    }else{
      swal({title:Lang.get('advertise.error'),text:Lang.get('advertise.locduperror'),confirmButtonColor: "#c9302c"});
    }
    }
    $.fn.removeLocation = function() {
      $(this).nextAll("div.locationlistitem").each(function (index, value) { 
        var id = $(this).attr("id").match(/\d+/)[0];
        id = parseInt(id);
        $(this).attr("id","item" + (id - 1));
        $(this).children("label:first-child").html("" + (id - 1));
        $(this).children("input").attr("name","locationitem" + (id -1));
      });
      var lengthobj = $("input#locationlength");
      lengthobj.val("" + (parseInt(lengthobj.val()) - 1));
      $(this).fadeOut(300,function(){
        if(lengthobj.val() == "0"){
          $(this).prev("span").fadeIn(300);
        }
        $(this).remove();
      });
    }
    $.fn.addStateLocation = function() {
      $(this).tooltip('hide');
      var $imgLoader = $("#ContinentModal").children(".modal-content").children("#row-map-container").children(".row").children("#mmap").children("span");
      $imgLoader.hide();
    if($("#locationlist").getListLength() != 10){
    $("#ContinentModal").loadMap("g",function(){$imgLoader.hide();},function(){},function(str,title){
      $("#ContinentModal").children(".modal-content").children("#row-map-container").children(".row").children("#mmap").children("#mapsvg").SvgToImg("continent");
      var location = str.split('-')[0];
      $("body > .tooltip").css("display","none");
      $("body").removeClass().addClass(location);
      var img0 = $("#CountryModal").children(".modal-content").children("#row-map-container").children(".row").children("#mmap").children("#mapsvg");
      img0.attr("src-svg",img0.attr("src-dir") + "/" + location + ".svg");
      img0.attr("src-list","country" + "/" + location + ".json");
      $("#CountryModal").loadMap("",function(){},function(){},function(str1,title){
        $("#CountryModal").children(".modal-content").children("#row-map-container").children(".row").children("#mmap").children("#mapsvg").SvgToImg("country");
        $("body > .tooltip").css("display","none");
        var location1 = "";
        location1 = location + "," + str1;
        $("body").removeClass();
        //location = location + " > " + str1;
        var img1 = $("#StateModal").children(".modal-content").children("#row-map-container").children(".row").children("#mmap").children("#mapsvg");
        img1.attr("src-svg",img1.attr("src-dir") + "/" + str1 + "-high.svg");
        img1.attr("src-list","state" + "/" + str1 + ".json");
        $("#StateModal").loadMap("",function(){},function(){},function(str2,title){
          //location = location + " > " + str2;
          $("body > .tooltip").css("display","none");
          if($("#locationlist").getListLength() != 10){
            $("#locationlist").addLocation(location1 + "," + str2.split("-")[1],"State",title);
          }else{
            swal({title:Lang.get('advertise.error'),text:Lang.get('advertise.locmaperror'),confirmButtonColor: "#c9302c"});
          }
          $("#StateModal").children(".modal-content").children("#row-map-container").children(".row").children("#mmap").children("#mapsvg").SvgToImg("state");
        });
      });
    });
    }else{
      swal({title:Lang.get('advertise.error'),text:Lang.get('advertise.locmoreerror'),confirmButtonColor: "#c9302c"});
    }
    }
    $.fn.addCountryLocation = function() {
      $(this).tooltip('hide');
      var $imgLoader = $("#ContinentModal").children(".modal-content").children("#row-map-container").children(".row").children("#mmap").children("span");
      $imgLoader.hide();
    if($("#locationlist").getListLength() != 10){
    $("#ContinentModal").loadMap("g",function(){$imgLoader.hide();},function(){},function(str,title){
      $("#ContinentModal").children(".modal-content").children("#row-map-container").children(".row").children("#mmap").children("#mapsvg").SvgToImg("continent");
      var location = str.split('-')[0];
      $("body > .tooltip").css("display","none");
      $("body").removeClass().addClass(location);
      var img0 = $("#CountryModal").children(".modal-content").children("#row-map-container").children(".row").children("#mmap").children("#mapsvg");
      img0.attr("src-svg",img0.attr("src-dir") + "/" + location + ".svg");
      img0.attr("src-list","country" + "/" + location + ".json");
      $("#CountryModal").loadMap("",function(){},function(){},function(str1,title){
        $("#CountryModal").children(".modal-content").children("#row-map-container").children(".row").children("#mmap").children("#mapsvg").SvgToImg("country");
        $("body > .tooltip").css("display","none");
        var location1 = "";
        location1 = location + "," + str1;
        $("body").removeClass();
        if($("#locationlist").getListLength() != 10){
            $("#locationlist").addLocation(location1,"Country",title);
          }else{
            swal({title:Lang.get('advertise.error'),text:Lang.get('advertise.locmoreerror'),confirmButtonColor: "#c9302c"});
        }
      });
    });
    }else{
      swal({title:Lang.get('advertise.error'),text:Lang.get('advertise.locmoreerror'),confirmButtonColor: "#c9302c"});
    }
    }
    $.fn.addContinentLocation = function() {
      $(this).tooltip('hide');
      var $imgLoader = $("#ContinentModal").children(".modal-content").children("#row-map-container").children(".row").children("#mmap").children("span");
      $imgLoader.hide();
    if($("#locationlist").getListLength() != 10){
    $("#ContinentModal").loadMap("g",function(){$imgLoader.hide();},function(){},function(str,title){
      $("#ContinentModal").children(".modal-content").children("#row-map-container").children(".row").children("#mmap").children("#mapsvg").SvgToImg("continent");
      $("body > .tooltip").css("display","none");
      var location = str.split('-')[0];
      $("body").removeClass();
      if($("#locationlist").getListLength() != 10){
        $("#locationlist").addLocation(location,"Continent",title);
      }else{
        swal({title:Lang.get('advertise.error'),text:Lang.get('advertise.locmoreerror'),confirmButtonColor: "#c9302c"});
      }
    });
    }else{
      swal({title:Lang.get('advertise.error'),text:Lang.get('advertise.locmoreerror'),confirmButtonColor: "#c9302c"});
    }
    }
    $.fn.getListLength = function(){
      return $(this).find("div.locationlistitem").length;
    }
    $.fn.issetInList = function(str){
      var a = false;
      $(this).find("div.locationlistitem").each(function (index, value) { 
        if($(this).children("input").val() == str){
          a = true;
        }
      });
      return a;
    }
    $.fn.hsv2rgb = function(h, s, v){
      var rgb, i, data =[];
      if(s === 0){
        rgb = [v,v,v];
      }else{
        h = h /60;
        i = Math.floor(h);
        data = [v * (1 - s), v * (1 - s * (h - i)), v * (1 - s * (1 - (h - i)))];
        switch(i){
          case 0:
          rgb = [v, data[2], data[0]];
          break;
          case 1:
          rgb = [data[1], v, data[0]];
          break;
          case 2:
          rgb = [data[0], v, data[2]];
          break;
          case 3:
          rgb = [data[0], data[1], v];
          break;
          case 4:
          rgb = [data[2], data[0], v];
          default:
          rgb = [v, data[0], data[1]];
          break;
        }
        return '#' + rgb.map(function(x){
          return ("0" + Math.round(x * 255).toString(16)).slice(-2);
        }).join('');
      }
    }
    $.fn.isValid = function(){
      var subject = $(document).find("input#subject");
      var content = $(document).find("textarea#content");
      var phone = $("#inform input#phone");
      var email = $("input#email");
      var telegram = $("input#telegram");
      var price = $("input#price");
      var trust = $("input#trust");
      var locationlist = $("#locationlist");
      var selectInform = $("#selectInform");
      var locationlength = $("input#locationlength");
      var moneytype = $("input#moneytype");
      var category = $("input#categoryinput");
      var categorybutton = $("#selectCategory");
      var phonereg = /^\+([0-9]{8,20})$/;
      var emailreg = /^([a-zA-Z0-9_\.-]+)@([\da-zA-Z\.-]+)\.([a-zA-Z\.]{2,6})$/;
      var telegramreg = /^@([a-zA-Z_0-9]+)$/;
      var intreg = /^([0-9]+)([\.]{0,1})([0-9]*)$/;
      var isValid = true;
      $(price).parent().removeClass("has-error").removeClass("has-warning").removeClass("has-success");
      $(trust).parent().removeClass("has-error").removeClass("has-warning").removeClass("has-success");
      var str = $("input#rq").val();
      var str1 = $("input#rq").val().split('|');
      if($.inArray("price",str1) !== -1 || str == "price"){
        if(!intreg.test(price.val())){
          isValid = false;
          $("div#price").addClass("has-error");
        }
      }
      if($.inArray("trust",str1) !== -1 || str == "trust"){
        if(!intreg.test(trust.val())){
          isValid = false;
          $("div#trust").addClass("has-error");
        }
      }
      locationlist.css("border-color","#cacaca");
      if(locationlength.val() == "0" || locationlength.val() == null){
        isValid = false;
        locationlist.css("border-color","#a94442");
      }
      categorybutton.css("border-color" , "#cacaca");
      if (category.val() == "0" || category.val() == null) {
        isValid = false;
        categorybutton.css("border-color","#a94442");
      }
      subject.blur();
      content.blur();
      phone.blur();
      email.blur();
      telegram.blur();
      price.blur();
      trust.blur();
      /*if(!intreg.test(trust.val())){
        isValid = false;
      }
      if(!intreg.test(price.val())){
        isValid = false;
      }*/
      selectInform.css("border-color" , "#cacaca");
      if(!emailreg.test(email.val())){
        isValid = false;
        selectInform.css("border-color","#a94442");
      }
      if(!phonereg.test(phone.val())){
        isValid = false;
        selectInform.css("border-color","#a94442");
      }
      if(!telegramreg.test(telegram.val())){
        isValid = false;
        selectInform.css("border-color","#a94442");
      }
      if(!((200 - subject.val().length) > 0 && subject.val().length != 0 && subject.val().length >= 10)){
        isValid = false;
      }
      if(!((3000 - content.val().length) > 0 && content.val().length != 0 && content.val().length >= 20)){
        isValid = false;
      }
      return isValid;
    }
    $.fn.liveValidate = function() {
      var subject = $(this).find("input#subject");
      var content = $(this).find("textarea#content");
      var phone = $("input#phone");
      var email = $("input#email");
      var telegram = $("input#telegram");
      var price = $("input#price");
      var trust = $("input#trust");
      var locationlist = $("#locationlist");
      var phonereg = /^\+([0-9]{8,20})$/;
      var emailreg = /^([a-z0-9A-Z_\.-]+)@([\da-zA-Z\.-]+)\.([a-zA-Z\.]{2,6})$/;
      var telegramreg = /^@([a-zA-Z_0-9]+)$/;
      var intreg = /^([0-9]+)([\.]{0,1})([0-9]*)$/;
      price.blur(function(){
        $(this).parent().removeClass("has-error").removeClass("has-warning").removeClass("has-success");
        if(intreg.test($(this).val())){
          $(this).parent().addClass("has-success");
        }else{
          $(this).parent().addClass("has-error");
        }
      });
      trust.blur(function(){
        $(this).parent().removeClass("has-error").removeClass("has-warning").removeClass("has-success");
        if(intreg.test($(this).val())){
          $(this).parent().addClass("has-success");
        }else{
          $(this).parent().addClass("has-error");
        }
      });
      phone.on('input',function(e){
        $(this).parent().parent().next(".error").fadeOut();
      });
      email.on('input',function(e){
        $(this).parent().parent().next(".error").fadeOut();
      });
      telegram.on('input',function(e){
        $(this).parent().parent().next(".error").fadeOut();
      });
      phone.blur(function(){
        $(this).css("border-color","#757575");
        if(phonereg.test($(this).val())){
          $(this).css("border-color","#757575");
          $(this).parent().parent().next(".error").fadeOut();
        }else{
          $(this).css("border-color","#a94442");
          $(this).parent().parent().next(".error").fadeIn();
        }
      });
      email.blur(function(){
        if(emailreg.test($(this).val())){
          $(this).css("border-color","#757575");
          $(this).parent().parent().next(".error").fadeOut();
        }else{
          $(this).css("border-color","#a94442");
          $(this).parent().parent().next(".error").fadeIn();
        }
      });
      telegram.blur(function(){
        if(telegramreg.test($(this).val())){
          $(this).css("border-color","#757575");
          $(this).parent().parent().next(".error").fadeOut();
        }else{
          $(this).css("border-color","#a94442");
          $(this).parent().parent().next(".error").fadeIn();
        }
      });
      subject.blur(function(){
        //$(this).next("span").html(200 - ($(this).val().length));
        $(document).ready(function(){
          $('<style>.m-group .bar:before,.m-group .bar:after{background:#297dce;}</style>').appendTo('head');
        });
        $(this).css("border-color","#757575");
        $(this).prev("label").css("color","#297dce");
        if((200 - $(this).val().length) > 0 && $(this).val().length != 0 && $(this).val().length >= 10){
          $(this).parent().parent().next(".error").fadeOut();
        }else{
          $(this).css("border-color","#a94442");
          $(this).parent().parent().next(".error").fadeIn();
        }
      });
      subject.on('input',function(e){
        if((200 - ($(this).val().length)) >= 0){
          $(this).parent().next("span").html(200 - ($(this).val().length));
          var ival = ($(this).val().length) / 2;
          var r = Math.floor((255 * ival) / 100);
          var g = Math.floor((255 * (100 - ival)) / 100);
          var b = 0;
          $(this).parent().parent().next(".error").fadeOut();
          $(this).parent().next("span").css("text-shadow","none");
          $(this).parent().next("span").css("color","rgb(" + r + "," + g + "," + b + ")");
          $(this).prev("label").css("color","rgb(" + r + "," + g + "," + b + ")");
          $(document).ready(function(){
            $('<style>.m-group .bar:before,.m-group .bar:after{background:' + "rgb(" + r + "," + g + "," + b + ")" + ';}</style>').appendTo('head');
          });
        }else{
          $(this).val($(this).val().substr(0, 200));
          $(this).parent().parent().next(".error").fadeIn();
        }
      });
      subject.focus(function(){
        $(this).parent().next("span").html(200 - ($(this).val().length));
      });

      content.focus(function(){
        $(this).parent().next("span").html(3000 - ($(this).val().length));
      });
      content.blur(function(){
        $(document).ready(function(){
          $('<style>.m-group .bar:before,.m-group .bar:after{background:#297dce;}</style>').appendTo('head');
        });
        $(this).css("border-color","#757575");
        $(this).prev("label").css("color","#297dce");
        if((3000 - $(this).val().length) > 0 && $(this).val().length != 0 && $(this).val().length >= 20){
          $(this).parent().parent().next(".error").fadeOut();
        }else{
          $(this).css("border-color","#a94442");
          $(this).parent().parent().next(".error").fadeIn();
        }
      });
      content.on('input',function(){
        if((3000 - ($(this).val().length)) >= 0){
          $(this).parent().next("span").html(3000 - ($(this).val().length));
          var ival = ($(this).val().length) / 30;
          var r = Math.floor((255 * ival) / 100);
          var g = Math.floor((255 * (100 - ival)) / 100);
          var b = 0;
          $(this).parent().parent().next(".error").fadeOut();
          $(this).parent().next("span").css("text-shadow","none");
          $(this).parent().next("span").css("color","rgb(" + r + "," + g + "," + b + ")");
          $(this).prev("label").css("color","rgb(" + r + "," + g + "," + b + ")");
          $(document).ready(function(){
            $('<style>.m-group .bar:before,.m-group .bar:after{background:' + "rgb(" + r + "," + g + "," + b + ")" + ';}</style>').appendTo('head');
          });
        }else{
          $(this).val($(this).val().substr(0, 3000));
          $(this).parent().parent().next(".error").fadeIn();
        }
      });
    }
    $.fn.uploadPicture = function(func,func1) {
      $(this).off();
      $(this).on('change', function () {
        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.gif|.jpeg)$");
        if (regex.test($(this).val().toLowerCase())) {
        if (typeof (FileReader) != "undefined") {
            var image_holder = $(this).parent();
            var it = $(this);
            var reader = new FileReader();
            reader.onload = function (e) {
              console.log("FileReader");
              it.next().attr("src",e.target.result);
              $("<div><a></a></div>").appendTo(image_holder);
              it.UploadedPicture();
              it.nextAll("div").children("a").off();
              it.nextAll("div").children("a").click(function(){
                $(this).parent().prevAll("input").resetUploadPicture(func1);
              });
              it.parent().off();
              it.parent().hover(function(){
                $(this).children("div").fadeIn(300);
              },function(){
                $(this).children("div").fadeOut(300);
              });
              func.call(this,it);
            }
            reader.readAsDataURL($(this)[0].files[0]);
            if(typeof (it.val()) == "undefined"){
              it.resetUploadPicture();
            }
        } else {
            swal({title:Lang.get('advertise.error'),text:Lang.get('advertise.filereadererror'),confirmButtonColor: "#c9302c"});
        }
      }else{
        swal({title:Lang.get('advertise.error'),text:Lang.get('advertise.validimage'),confirmButtonColor: "#c9302c"});
        $(this).resetUploadPicture();
      }
      });
    }
    $.fn.resetUploadPicture = function(func) {
      var it = $(this);
      $(this).nextAll("img").attr("src",baseUrl + "/asset/image/icon/photo.png").parent().removeClass().addClass("image-holder active");
      $(this).nextAll("div").remove();
      $(this).replaceWith(it.val('').clone(true));
      var newit = $("#" + it.attr('id'));
      newit.setUploadable(function(){},function(){});
      func.call(this,newit);
      console.log("resetUploadPicture");
    }
    $.fn.ActiveUploadPicture = function() {
      console.log("ActiveUploadPicture");
      $(this).parent().removeClass().addClass("image-holder active");
    }
    $.fn.DeactiveUploadPicture = function() {
      $(this).parent().removeClass().addClass("image-holder deactive");
    }
    $.fn.UploadedPicture = function() {
      $(this).parent().removeClass().addClass("image-holder uploaded");
    }
    $.fn.setNotUploadable = function() {
      var it = $(this);
      it.DeactiveUploadPicture();
      it.next("img").off();
      it.off();
    }
    $.fn.setUploadable = function(func,func1) {
      var it = $(this);
      console.log("setUploadable");
      it.ActiveUploadPicture();
      it.next("img").off();
      it.next("img").click(function(){
        it.click();
      });
      it.uploadPicture(func,func1);
    }
    $.fn.uploadImage = function(str){
      console.log("Uploading picture : " + str);
      if (typeof (FormData) != "undefined") {
        var formData = new FormData();
        formData.append('image',$(this)[0].files[0]);
        $.ajax({
          url: baseUrl + "/upload/image",
          type: "POST",
          data: formData,
          contentType: false,
          cache: false,
          processData:false,
          success: function(data){
            console.log("Uploaded : " + str + "Data : " + data);
            $("input#pic" + str).val(data);
          }
        });
      }
    }
    $.fn.hideCategoryDialog = function(){
      var it = $("div#category");
      it.removeClass("animated fadeInDown");
      it.removeClass("animated fadeOutUp").addClass("animated fadeOutUp");
      $(".gray-back").fadeOut(600,function(){
        it.hide();
      });
    }
    $.fn.showCategoryDialog = function(){
      var it = $("div#category");
      it.show();
      it.removeClass("animated fadeOutUp");
      it.removeClass("animated fadeInDown").addClass("animated fadeInDown");
      $(".gray-back").fadeIn();
      var content = it.children("#category-content");
      var header = it.children("#category-header");
      header.children("img").off().click(function(){
        $(this).hideCategoryDialog();
      });
      $(this).loadCategory();
    }
    $.fn.loadCategory = function(){
      var it = $("div#category");
      var back = it.children("#category-header").children("h4").children("img");
      back.hide();
      back.next("span").html(Lang.get('advertise.headerselectcategorie'));
      var content = it.children("#category-content");
      var loader = it.children("#category-content").children("img");
      loader.fadeIn();
      var ul = content.children("ul");
      /*content.children("ul").find("li").fadeOut(300,function(){
        content.children("ul").empty();
      });*/
      $.getJSON( baseUrl + "/getCategory", function( data ) {
        loader.fadeOut();
        content.children("ul").empty();
        $.each( data, function( ID, obj) {
          var u = $("<li>");
          u.append($("<i>").attr("class",obj.Icon));
          u.append($("<span>").html(obj.Data));
          u.append($("<i>").attr("class","flaticon-right-arrow")).attr("id",obj.ID);
          u.appendTo($("div#category > #category-content > ul"));
          u.click(function(){
            //alert($(this).children("span").html() + " " + $(this).attr("id"));
            $(this).loadSubCategory(obj.ID, obj.Data);
            back.fadeIn();
            back.off().click(function(){
              $(this).loadCategory();
            });
            back.next("span").html($(this).children("span").html());
          });
        });
      });
    }
    $.fn.loadSubCategory = function($id, $str){
      var it = $("div#category");
      var content = it.children("#category-content");
      var back = it.children("#category-header").children("h4").children("img");
      back.off().click(function(){
        $(this).loadCategory();
      });
      /*content.children("ul").find("li").fadeOut(300,function(){
        content.children("ul").empty();
      });*/
      var loader = it.children("#category-content").children("img");
      loader.fadeIn();
      $.getJSON( baseUrl + "/getSubCategory/" + $id, function( data ) {
        content.children("ul").empty();

        loader.fadeOut();
        $.each( data, function( ID, obj) {
          var u = $("<li>");
          u.append($("<span>").html(obj.Title));
          u.attr("id",obj.Id);
          if(obj.Requirement != null){
            u.attr("rq",obj.Requirement);
          }else{
            $("input#rq").val("");
          }
          if(obj.Kind != 0){
            u.append($("<i>").attr("class","flaticon-right-arrow"));
          }
          u.appendTo($("div#category > #category-content > ul"));
          u.click(function(){
            if($(this).children("i").length != 0){
              back.next("span").html(obj.Title);
              $(this).loadCategoryKind($id + ',' + obj.Id, $str, obj.Title);
              $("div#price,div#trust,div#gender").hide();
              if($(this).attr('rq') != null){
                var str = $(this).attr('rq');
                $("input#rq").val(str);
                var str1 = $(this).attr('rq').split('|');
                if($.inArray("price",str1) !== -1 || str == "price"){
                  $("div#price").fadeIn();
                }
                if($.inArray("trust",str1) !== -1 || str == "trust"){
                  $("div#trust").fadeIn();
                }
                if($.inArray("gender",str1) !== -1 || str == "gender"){
                  $("div#gender").fadeIn();
                }
              }else{
                $("input#rq").val("");
              }
              back.off().click(function(){
                $(this).loadSubCategory($id,$str);
              });
            }else{
              $(this).hideCategoryDialog();
              $("#selectCategory").children("span").html("<ul><li>" + $str + "</li>" + "<li class=\"flaticon-left-arrow\"></li>" + "<li>" + obj.Title + "</li></ul>");
              $("div#price,div#trust,div#gender").hide();
              $("input#categoryinput").val($id + ',' + obj.Id);
              if($(this).attr('rq') != null){
                var str = $(this).attr('rq');
                $("input#rq").val(str);
                var str1 = $(this).attr('rq').split('|');
                if($.inArray("price",str1) !== -1 || str == "price"){
                  $("div#price").fadeIn();
                }
                if($.inArray("trust",str1) !== -1 || str == "trust"){
                  $("div#trust").fadeIn();
                }
                if($.inArray("gender",str1) !== -1 || str == "gender"){
                  $("div#gender").fadeIn();
                }
              }
            }
          });
        });
      });
    }
    $.fn.loadCategoryKind = function($id , $str ,$str1){
      var it = $("div#category");
      var content = it.children("#category-content");
      var back = it.children("#category-header").children("h4").children("img");
      back.off().click(function(){
        $(this).loadCategory();
      });
      /*content.children("ul").find("li").fadeOut(300,function(){
        content.children("ul").empty();
      });*/
      var loader = it.children("#category-content").children("img");
      loader.fadeIn();
      $.getJSON( baseUrl + "/getKindCategory/" + $id.split(",")[1], function( data ) {
        loader.fadeOut();
        content.children("ul").empty();
        $.each( data, function( ID, obj) {
          var u = $("<li>");
          u.append($("<span>").html(obj.Title));
          u.attr("id",obj.Id);
          u.appendTo($("div#category > #category-content > ul"));
          u.click(function(){
            $("#selectCategory").children("span").html("<ul><li>" + $str + "</li>" + "<li class=\"flaticon-left-arrow\"></li>" + "<li>" + $str1 + "</li>" + "<li class=\"flaticon-left-arrow\"></li>" + "<li>" + obj.Title + "</li></ul>");
            $("input#categoryinput").val($id + ',' + obj.Id);
            $(this).hideCategoryDialog();
          });
        });
      });
    }
    $.fn.hideInformDialog = function(){
      var it = $("div#inform");
      it.removeClass("animated fadeInDown");
      it.removeClass("animated fadeOutUp");
      it.addClass("animated fadeOutUp");
      var phonereg = /^\+([0-9]{8,20})$/;
      var emailreg = /^([a-zA-Z0-9_\.-]+)@([\da-zA-Z\.-]+)\.([a-zA-Z\.]{2,6})$/;
      var telegramreg = /^@([a-zA-Z_0-9]+)$/;
      var phone = $("#inform input#phone");
      var email = $("input#email");
      var telegram = $("input#telegram");
      var selectInform = $("#selectInform");
      selectInform.css("border-color" , "#cacaca");
      if(!emailreg.test(email.val())){
        isValid = false;
        selectInform.css("border-color","#a94442");
      }
      if(!phonereg.test(phone.val())){
        isValid = false;
        selectInform.css("border-color","#a94442");
      }
      if(!telegramreg.test(telegram.val())){
        isValid = false;
        selectInform.css("border-color","#a94442");
      }
      $(".gray-back").fadeOut(600,function(){
        it.hide();
      });
    }
    $.fn.showInformDialog = function(){
      var it = $("div#inform");
      it.show();
      it.removeClass("animated fadeOutUp");
      it.removeClass("animated fadeInDown").addClass("animated fadeInDown");
      $(".gray-back").fadeIn();
      it.children("img").off().click(function(){
        $(this).hideInformDialog();
      });
      it.find("button").click(function(){
        $(this).hideInformDialog();
      });
    }
    //$.fn.ActiveUploadLength(){
    //}
    $.fn.hideAdvertiseTimeDialog = function(){
      var it = $("div#advertise-time");
      it.removeClass("animated fadeInDown");
      it.removeClass("animated fadeOutUp").addClass("animated fadeOutUp");
      $(".gray-back").fadeOut(600,function(){
        it.hide();
      });
    }
    $.fn.showAdvertiseTimeDialog = function(){
      var it = $("div#advertise-time");
      it.show();
      it.removeClass("animated fadeOutUp");
      it.removeClass("animated fadeInDown").addClass("animated fadeInDown");
      $(".gray-back").fadeIn();
      it.children("img").off().click(function(){
        $(this).hideAdvertiseTimeDialog();
      });
      it.find("button").click(function(){
        $(this).hideAdvertiseTimeDialog();
      });
    }

    $.fn.configPreviewSlider = function(){
      slider = $('#imageGallery').lightSlider({
            gallery:true,
            item:1,
            loop:false,
            thumbItem:6,
            slideMargin:0,
            enableDrag: false,
            currentPagerPosition:'left',
            onSliderLoad: function(el) {
               el.lightGallery({
                    selector: '#imageGallery .lslide'
                });
            }  
        });  
    }
    $.fn.showPreviewDialog = function(){
      $('nav').fadeOut(100);
      $("#preview").show();
      $("#preview").scroll(function(event) {
        $(this).scrollLeft(0);
      });
      $("#preview").removeClass('animated fadeOutUp');
      $("#preview").addClass('animated fadeInDown');
      $("#preview").scrollTop(0);
      $("#imageGallery").empty();
      var isGalleryEmpty = true;
      if ($('#fileselector1').get(0).files.length != 0) {
        var newEl = '<li data-thumb="' + $("#fileselector1").next().attr("src") + '" data-src="' + $("#fileselector1").next().attr("src") + '"><img src="' + $("#fileselector1").next().attr("src") + '" height="300" style="display:block;margin:0px auto;"/></li>';
        $('#imageGallery').prepend(newEl);
        isGalleryEmpty = false;
      }
      if ($('#fileselector2').get(0).files.length != 0) {
        var newEl = '<li data-thumb="' + $("#fileselector2").next().attr("src") + '" data-src="' + $("#fileselector2").next().attr("src") + '"><img src="' + $("#fileselector2").next().attr("src") + '" height="300" style="display:block;margin:0px auto;"/></li>';
        $('#imageGallery').prepend(newEl);
        isGalleryEmpty = false;
      }
      if ($('#fileselector3').get(0).files.length != 0) {
        var newEl = '<li data-thumb="' + $("#fileselector3").next().attr("src") + '" data-src="' + $("#fileselector3").next().attr("src") + '"><img src="' + $("#fileselector3").next().attr("src") + '" height="300" style="display:block;margin:0px auto;"/></li>';
        $('#imageGallery').prepend(newEl);
        isGalleryEmpty = false;
      }
      if(isGalleryEmpty){
        $("#preview-gallery").hide();
      }else{
        $("#preview-gallery").show();
      }
      if(!isSliderLoaded){
        $(this).configPreviewSlider();
        isSliderLoaded = true;
      }else{
        slider.destroy();
        $(this).configPreviewSlider();
      }
      slider.refresh();
      $("#preview").children(".close-animatedModal").children(".closebt").click(function(){
        $("#preview").removeClass('animatesd fadeInDown');
        $("#preview").addClass('animated fadeOutUp');
        $('nav').fadeIn(100);
        $("#prev-get-phone-span").html(Lang.get('advertise.call'));
      });
    }

  $.fn.showEmailComposeDialog = function(){
    var it = $("div#email-inform");
    it.show();
    var email = $("#inform input#email").val();
    it.find(".gmail-social").parent().attr("href","https://mail.google.com/mail/?view=cm&fs=1&to=" + email);
    it.find(".hotmail-social").parent().attr("href","mailto:" + email);
    it.find(".yahoo-social").parent().attr("href","http://compose.mail.yahoo.com/?to=" + email);
    it.find("h3").html(email);
    it.removeClass("animated fadeOutUp");
    it.removeClass("animated fadeInDown").addClass("animated fadeInDown");
    $(".gray-back").css("z-index","3001");
    
    $(".gray-back").fadeIn();
    $(".gray-back").off().click(function(){
      $(this).hideEmailComposeDialog();
    });
  }

  $.fn.hideEmailComposeDialog = function(){
    var it = $("div#email-inform");
    it.removeClass("animated fadeInDown");
    it.removeClass("animated fadeOutUp").addClass("animated fadeOutUp");
    $(".gray-back").fadeOut(1000,function(){
      it.hide();
    });
    $(".gray-back").css("z-index","999");
  }

  $.fn.showTelegramComposeDialog = function(){
    var it = $("div#telegram-inform");
    it.show();
    var telegram = $("#inform input#telegram").val();
    telegram_o = telegram.substring(1);
    it.find("h3").html(telegram);
    it.find("a").attr("href","https://telegram.me/" + telegram_o);
    it.removeClass("animated fadeOutUp");
    it.removeClass("animated fadeInDown").addClass("animated fadeInDown");
    $(".gray-back").css("z-index","3001");
    $(".gray-back").fadeIn();
    $(".gray-back").off().click(function(){
      $(this).hideTelegramComposeDialog();
    });
  }

  $.fn.hideTelegramComposeDialog = function(){
    var it = $("div#telegram-inform");
    it.removeClass("animated fadeInDown");
    it.removeClass("animated fadeOutUp").addClass("animated fadeOutUp");
    $(".gray-back").fadeOut(1000,function(){
      it.hide();
    });
    $(".gray-back").css("z-index","999");
  }

  $.fn.showLoginDialog = function(){
    $('nav').fadeOut(100);
    $("#login").show();
    $("#login").removeClass('animated fadeOutUp');
    $("#login").addClass('animated fadeInDown');
    $("#login").scrollTop(0);
    $("#login").children(".close-animatedModal").children(".closebt").click(function(){
      $("#login").removeClass('animatesd fadeInDown');
      $("#login").addClass('animated fadeOutUp');
      $('nav').fadeIn(100);
    });
  }


  $.fn.extend({
    animateCss: function (animationName) {
      var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
      $(this).addClass('animated ' + animationName).one(animationEnd, function() {
        $(this).removeClass('animated ' + animationName);
      });      
    }
  });
  $.fn.showRceiveEmailVerify = function(){
    var phone = $("#login input#phone");
      phone.blur();
      var phonereg = /^\+([0-9]{8,20})$/;
      var emailreg = /^([a-zA-Z0-9_\.-]+)@([\da-zA-Z\.-]+)\.([a-zA-Z\.]{2,6})$/;
      //$("#login").find
    if(phonereg.test(phone.val())){
      $("button#login-next").off().click(function(){
        $(this).verifyEmailCode($("#login input#code").val());
      });
      $("#login input#phone").parent().parent().removeClass("fadeInLeft").show();
      $("#login input#phone").parent().parent().animateCss("fadeOutLeft");
      $("#login input#phone").parent().parent().fadeOut(500,function(){
        $("#timer").fadeIn();
        timer = $.timer(1000, function() {
          times = times - 1;
          if(times == 0){
            timer.stop();
            times = 90;
            $("#timer").find("h4").html("0:00");
            $("#timer").fadeOut();
            //-----
            $("#login input#code").parent().parent().animateCss("fadeOutRight");
            $("#login .receivemail").animateCss("fadeOutRight");
            $("#login input#code").parent().parent().fadeOut();
            $("#login .receivemail").fadeOut(500,function(){
              $("#login input#phone").parent().parent().removeClass("fadeOutLeft").show();
              $("#login input#phone").parent().parent().animateCss("fadeInLeft");
            });

            //-----
          }else{
            if(times >= 60){
              if((times -60) < 10){
                var str = "0";
              }else{
                var str = "";
              }
              $("#timer").find("h4").html("1" + ":" + str + (times - 60));
            }else{
              if(times < 10){
                var str = "0";
              }else{
                var str = "";
              }
              $("#timer").find("h4").html("0" + ":" + str + times);
            }
            $("#timer").find("h4")
          }
        });
        $("#login input#code").parent().parent().removeClass("fadeOutRight").show();
        $("#login .receivemail").removeClass("fadeOutRight").show();
        $("#login input#code").parent().parent().animateCss("fadeInRight");
        $("#login .receivemail").animateCss("fadeInRight");
      });
    }
  }


  $.fn.sendEmailCode = function(email){
    $.ajax({
      url : baseUrl + "/user/phone:saved/register",
      success : function(data) {
        console.log(data);
        if(data.status == 200){
          $(this).showRceiveEmailVerify();
        }
      },
      error : function(){
        swal({title:Lang.get('advertise.error'),text:Lang.get('advertise.networkerror'),confirmButtonColor: "#c9302c"});
      }
    });
  }


  $.fn.verifyEmailCode = function($code){
    $.ajax({
      url : baseUrl + "/user/phone:saved/verify:" + $code,
      success : function(data) {
        console.log(data);
        if(data.status == 200){
          timer.stop();
          $(document).showSetPass();
        }
      },
      error : function(){
        swal({title:Lang.get('advertise.error'),text:Lang.get('advertise.networkerror'),confirmButtonColor: "#c9302c"});
      }
    });
  }

  $.fn.setPassword = function($password){
    $.ajax({
      url : baseUrl + "/user/phone:saved/set/password:" + $password,
      success : function(data) {
        console.log(data);
        if(data.status == 200){
          $(document).uploadAdvertise();
        }
      },
      error : function(){
        swal({title:Lang.get('advertise.error'),text:Lang.get('advertise.networkerror'),confirmButtonColor: "#c9302c"});
      }
    });
  }
  $.fn.loginWithPass = function($password){
    $.ajax({
      type: 'POST',
      url : baseUrl + "/user/login/phone",
      data : {
        password : $password
      },
      success : function(data) {
        console.log(data);
        if(data.status == 200){
          $(document).uploadAdvertise();
        }
      },
      error : function(){
        swal({title:Lang.get('advertise.error'),text:Lang.get('advertise.networkerror'),confirmButtonColor: "#c9302c"});
      }
    });
  }
  $.fn.showEnterPass = function(){
    $("button#login-next").off().click(function(){
      $(this).loginWithPass($("#login input#password").val());
    });
    $("#login input#phone").parent().parent().removeClass("fadeInLeft").show();
    $("#login input#phone").parent().parent().animateCss("fadeOutLeft");
    $("#login input#phone").parent().parent().fadeOut(500,function(){
      $("#login input#password").parent().parent().show().animateCss("fadeInRight");
    });
  }
  $.fn.showSetPass = function(){
    $("#timer").fadeOut();
      $("button#login-next").off().click(function(){
        $(this).setPassword($("#login input#password").val());
      });
      $("#login input#code").parent().parent().removeClass("fadeInRight").fadeOut();
      $("#login input#code").parent().parent().animateCss("fadeOutLeft");
      $("#login .receivemail").removeClass("fadeInRight");
      $("#login .receivemail").animateCss("fadeOutLeft");
      $("#login .receivemail").fadeOut(500,function(){
        $("#login input#password").parent().parent().show().animateCss("fadeInRight");
        $("#login input#passwordverify").parent().parent().show().animateCss("fadeInRight");
        $("#login .setpass").show().animateCss("fadeInRight");
      });
  }

  $.fn.uploadAdvertise = function(){
    var subject = $(document).find("input#subject");
      var content = $(document).find("textarea#content");
      var phone = $("#inform input#phone");
      var email = $("input#email");
      var telegram = $("input#telegram");
      var price = $("input#price");
      var trust = $("input#trust");
      var locationlist = $("#locationlist");
      var selectInform = $("#selectInform");
      var locationlength = $("input#locationlength");
      var moneytype = $("input#moneytype");
      var category = $("input#categoryinput");
      var locations1 = "";
      for (var i = 1; i <= parseInt(locationlength.val()); i++) {
        locations1 += $("input[name='locationitem" + i + "']").val() + "{}";
      }
      var pic1 = $("input#pic1");
      var pic2 = $("input#pic2");
      var pic3 = $("input#pic3");
    $.ajax({
      type: 'POST',
      url : baseUrl + "/test/req",
      data : {
        subject : subject.val(),
        content : content.val(),
        phone : phone.val(),
        email : email.val(),
        telegram : telegram.val(),
        price : price.val(),
        trust : trust.val(),
        locationlength : locationlength.val(),
        category : category.val(),
        pic1 : pic1.val(),
        pic2 : pic2.val(),
        pic3 : pic3.val(),
        locations : locations1
      },
      success : function(data) {
        console.log(data);
        if(data.status == 200){
          window.location.replace(baseUrl + "/fa");
        }
      },
      error : function(){
        swal({title:Lang.get('advertise.error'),text:Lang.get('advertise.networkerror'),confirmButtonColor: "#c9302c"});
      }
    });
  }

}(jQuery));
$(function() {
  $(document).ready(function(){
    $(".dialogLocation").scrollTop(0);
    $("button#addstate").click(function(){
      $(this).addStateLocation();
    });
    $("button#addcontinent").click(function(){
      $(this).addContinentLocation();
    });
    $("button#addcountry").click(function(){
      $(this).addCountryLocation();
    });
    $("form#advertise").liveValidate();
    //fhhf
    $.getJSON("http://ip-api.com/json/?callback=?", function(data) {
      var data;
      data.timezone = data.timezone.toLowerCase();
      d = data.timezone.split('/')[0] + "," + data.countryCode + "," + data.region + "," + data.city;
      $("button#addcity").tooltip({html:true,placement:"left",title:"<a onclick=\"" + "$('#locationlist').addLocation('" + d + "','" + "City" + "','" + data.city + "');\">Add City " + data.city + "</a>",delay: {show: 0, hide: 2000}});
      $("button#addcity").hover(function(){$('button#addstate,button#addcountry,button#addcontinent').tooltip('hide');},function(){});
      d = data.timezone.split('/')[0] + "," + data.countryCode + "," + data.region;
      $("button#addstate").tooltip({html:true,placement:"left",title:"<a onclick=\"" + "$('#locationlist').addLocation('" + d + "','" + "State" + "','" + data.regionName + "');\">Add Region " + data.regionName + "</a>",delay: {show: 0, hide: 2000}});
      $("button#addstate").hover(function(){$('button#addcountry,button#addcontinent,button#addcity').tooltip('hide');},function(){});
      d = data.timezone.split('/')[0] + "," + data.countryCode;
      $("button#addcountry").tooltip({html:true,placement:"left",title:"<a onclick=\"" + "$('#locationlist').addLocation('" + d + "','" + "Country" + "','" + data.country + "');\">Add Country " + data.country + "</a>",delay: {show: 0, hide: 2000}});
      $("button#addcountry").hover(function(){$('button#addstate,button#addcity,button#addcontinent').tooltip('hide');},function(){});
      d = data.timezone.split('/')[0] + ",";
      $("button#addcontinent").tooltip({html:true,placement:"left",title:"<a onclick=\"" + "$('#locationlist').addLocation('" + d + "','" + "Continent" + "','" + data.timezone.split('/')[0] + "');\">Add Continent " + data.timezone.split('/')[0] + "</a>",delay: {show: 0, hide: 2000}});
      $("button#addcontinent").hover(function(){$('button#addstate,button#addcountry,button#addcity').tooltip('hide');},function(){});
    });

    function setEvent1(){
      $("input#fileselector1").setUploadable(function(o){
        o.uploadImage("1");
      },function(o1){
        $("input#pic1").val("0");
        setEvent1();
      });
    }
    function setEvent2(){
      $("input#fileselector2").setUploadable(function(o){
        o.uploadImage("2");
      },function(o1){
        $("input#pic2").val("0");
        setEvent2();
      });
    }
    function setEvent3(){
      $("input#fileselector3").setUploadable(function(o){
        o.uploadImage("3");
      },function(o1){
        $("input#pic3").val("0");
        setEvent3();
      });
    }
    setEvent1();
    setEvent2();
    setEvent3();


    $("button#submitmainform").click(function(){
      var subject = $("input#subject").val();
      var content = $("textarea#content").val();
      var phone = $("#inform input#phone").val();
      var email = $("#inform input#email").val();
      var telegram = $("#inform input#telegram").val();
      var price = $("input#price").val();
      var trust = $("input#trust").val();
      var locationlength = $("input#locationlength").val();
      var categoryinput = $("input#categoryinput").val();
      var moneytype = $("input#moneytype").val();
      var moneyString = Lang.get('advertise.toman');
      if(moneytype == "dollar"){
        moneyString = "$";
      }else if(moneytype == "euro"){
        moneyString = "&euro;";
      }else if(moneytype == "toman"){
        moneyString = Lang.get('advertise.toman');
      }
      var pic1 = $("input#pic1").val();
      var pic2 = $("input#pic2").val();
      var pic3 = $("input#pic3").val();
      var timepack = $("select#timepack").val();
      var numday = $("input#numday").val();
      var ticket =$("input#ticketenable").is(":checked");
      var star =$("input#staradvertise").is(":checked");
      var male =$("input#malegender").is(":checked");
      var female =$("input#femalegender").is(":checked");
      if($(this).isValid()){
        $("h2#preview-category").html($("button#selectCategory span").html());
        $("div#preview-content > h1 > b").html(subject);
        $("div#preview-content > p").html(content);
        var str = $("input#rq").val();
        var str1 = $("input#rq").val().split('|');
        $("div#preview-req").hide();
        $("div#preview-price").hide();
        $("div#preview-trust").hide();
        if($.inArray("price",str1) !== -1 || str == "price"){
          $("div#preview-req").show();
          $("div#preview-price").show();
          $("div#preview-price").children(".val").html(price + " " +  moneyString);
        }
        if($.inArray("trust",str1) !== -1 || str == "trust"){
          $("div#preview-req").show();
          $("div#preview-trust").show();
          $("div#preview-trust").children(".val").html(trust + " " + moneyString);
        }
        $(this).showPreviewDialog();
      }else{
        swal({title:Lang.get('advertise.error'),text:Lang.get('advertise.validinput'),confirmButtonColor: "#c9302c"});
      }
      //$("body").append("<img src=\"" + $("#fileselector1").next("img").attr("src") + "\"/>");
      //$("body").append($("#fileselector1").val() + "!!!!" + $("#fileselector2").val());
    });
    $('div#secondrow > div#imageuploadbox > div.row > div > div').each(function (index, value) { 
      $("div#secondrow > div#imageuploadbox > div.row").height($(this).outerWidth());
    });
    $("#selectInform").click(function(){
      $("body").showInformDialog();
    });
    $("#selectCategory").click(function(){
      $("body").showCategoryDialog();
    });
    $("button#selectInform").click(function(){
      $("body").showInformDialog();
    });
    $("button#selectCategory").on('click',function(){
      $("body").showCategoryDialog();
    });
    $("button#selectadvertisetime").click(function(){
      $("body").showAdvertiseTimeDialog();
    });
    $("#cartShop").click(function(event) {
      $("#cartShop > div").fadeIn();
    });
    $(".container").click(function(event) {
      $("#cartShop > div").fadeOut();
    });
    $('select#timepack').prop('disabled', 'disabled');
    $('input#numday').prop('disabled', 'disabled');
    $('input:radio[name="timeadvertise"]').change(
      function(){
        if ($(this).is(':checked') && $(this).val() == 'custom') {
          $('select#timepack').prop('disabled', false);
          $('input#numday').prop('disabled', false);
          $('select#timepack').prop('disabled', 'disabled');
        }
        if ($(this).is(':checked') && $(this).val() == 'default') {
          $('select#timepack').prop('disabled', false);
          $('input#numday').prop('disabled', false);
          $('input#numday').prop('disabled', 'disabled');
          $('select#timepack').prop('disabled', 'disabled');
        }
        if ($(this).is(':checked') && $(this).val() == 'pack') {
          $('select#timepack').prop('disabled', false);
          $('input#numday').prop('disabled', false);
          $('input#numday').prop('disabled', 'disabled');
        }
    });
    $('div#secondrow > div#imageuploadbox > div.row > div > div').each(function (index, value) { 
        $("div#secondrow > div#imageuploadbox > div.row").height($(this).outerWidth());
    });
    //$(".gray-back,#email-inform").show();
    $("#email-inform > div > h3,#telegram-inform h3").click(function (){
      var el = $(this)[0];
      var range = document.createRange();
      range.selectNodeContents(el);
      var sel = window.getSelection();
      sel.removeAllRanges();
      sel.addRange(range);
    });

    $("button#prev-get-mail").click(function(){
      $(this).showEmailComposeDialog();
    });

    $("button#prev-get-telegram").click(function(){
      $(this).showTelegramComposeDialog();
    });
    $("button#prev-get-phone").click(function(){
      $(this).find("#prev-get-phone-span").html($("#inform input#phone").val());
    });
    $("input#staradvertise").change(function(event) {
      if($("input#staradvertise").is(":checked")){
        $("#cartShop > div > ul").prepend('<li id="p-urgent">آگهی فوری <span>۳۰۰۰ تومان</span></li>'); 
        $("#open-cart").find('span').html("۳۰۰۰ تومان");
        $("#cartShop > div").find('#p-all').children('span').html("۳۰۰۰ تومان");
      }else{
        $("#cartShop > div").find('#p-urgent').remove();
        $("#open-cart").find('span').html("رایگان");
        $("#cartShop > div").find('#p-all').children('span').html("۰ تومان");
      }
    });
    $("button#confirm-prev").click(function(){
      $.ajax({
        url : baseUrl + "/user/isLogin",
        success : function(data) {
          console.log(data);
          if(data.status == 200){
            $(document).uploadAdvertise();
          }else{
            $(document).showLoginDialog();
          }
        },
        error : function(){
          swal({title:Lang.get('advertise.error'),text:Lang.get('advertise.networkerror'),confirmButtonColor: "#c9302c"});
        }
      });
    });
    $("button#login-next").click(function(){
      //$("#login input#email").blur();
      $.ajax({
        url : baseUrl + "/user/phone:" + $("#login input#phone").val() + "/isAvailable",
        success : function(data) {
          console.log(data);
          if(data.status == 200){
            $(document).sendEmailCode();
          }else {
            $(document).showEnterPass();
          }
        },
        error : function(){
          swal({title:Lang.get('advertise.error'),text:Lang.get('advertise.networkerror'),confirmButtonColor: "#c9302c"});
        }
      });
    });
  });
  $(window).resize(function(){
    $(".dialogLocation").scrollTop(0);
    $('div#secondrow > div#imageuploadbox > div.row > div > div').each(function (index, value) { 
        $("div#secondrow > div#imageuploadbox > div.row").height($(this).outerWidth());
    });
  });
});
