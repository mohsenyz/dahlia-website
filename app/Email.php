<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Log;
class Email extends Model
{
    //Manage Sending Sms
    protected $table = 'EmailLog';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public static function sendCode($email, $msg){
    	$resp = send_mail_code($email, $msg);
    	$mail = new Email;
    	$mail->Email = $email;
    	$mail->Msg = $msg;
    	$mail->SentTime = time();
    	$mail->Status = (int)$resp;
    	$mail->save();
        Log::info("Email : " . $email . "         ++++++++++++++         " . $msg);
    	return $mail->Status; 
    }
}
