<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'Users';

    public $timestamps = false;

    protected $primaryKey = 'Phone';

    public function getAuthIdentifierName(){
        return "Phone";
    }

    public function getAuthIdentifier(){
        return $this->Phone;
    }

    protected $fillable = [
        'Name', 'Email', 'Password', 'Phone', 'City', 'Forget', 'isBlocked',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'Password', 'remember_token',
    ];
    public function getAuthPassword(){
        return $this->Password;
    }

    public static function issetPhone($phone){
        if(User::where("Phone", $phone)->where('hasPass', 1)->count() == 1){
            return true;
        }else{
            return false;
        }
    }

    public static function issetEmail($email){
        if(User::where("Email", $email)->where('hasPass', 1)->count() == 1){
            return true;
        }else{
            return false;
        }
    }
}
