<?php

namespace App;

class Geo
{
	private $data = "";
	public function setData()
	{
		$str = file_get_contents("http://ip-api.com/json");
		$this->data = json_decode($str);
	}
	public function getState(){
		return strtolower($this->data->regionName);
	}
	public function getCity(){
		//return strtolower($this->data->city);
		return '11';
	}
	public function getCountry(){
		return strtolower($this->data->country);
	}
	public function getContinent(){
		return strtolower(explode("/", $this->data->timezone)[0]);
	}
}
