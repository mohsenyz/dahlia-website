<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailBlacklist extends Model
{
    protected $table = 'EmailBlackList';
    protected $primaryKey = 'Email';
    public $incrementing = false;
    public $timestamps = false;

    public static function issetEmail($email){
    	if(EmailBlacklist::where("Email", $email)->count() == 0)
    		return false;
    	else
    		return true;
    }

    
    public static function setIntoBlacklist($email, $type){
        if(!EmailBlacklist::issetEmail($email)){
            $blacklist = new EmailBlacklist;
            $blacklist->Email = $email;
            $blacklist->Date = time();
            $blacklist->Type = $type;
            $blacklist->save();
        }
    }
}
