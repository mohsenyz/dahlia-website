<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use DB;
class Advertise extends Controller
{
    public function createAdvertise(Request $req){
    	if(!Auth::check()){
			return response()->json(['status'=>403, 'Message'=>'You cant access']);
		}
		DB::table('Advertise')->insert([
			'Sub' => $req->input('subject'),
			'Content'=>$req->input('content'),
			'Category'=>$req->input('category'),
			'Pic'=>$req->input('pic1') . "," . $req->input('pic2') . "," . $req->input('pic3'),
			'Phone'=>$req->input('phone'),
			'Pending'=>'0',
			'Date'=>time(),
			'Type'=>'0',
			'Commit'=>'0',
			'City'=> 'Isfahan',
			'Price'=>$req->input('price'),
			'email'=>$req->input('email'),
			'telegram'=>$req->input('telegram'),
			]);
		return response()->json(['status'=>200, 'Message'=>'Advertise created']);
		//$req->input('locations')
    }
}
