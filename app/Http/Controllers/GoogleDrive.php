<?php

namespace App\Http\Controllers;
use Google_Client as Google_Client;
use Google_Service_Drive as Google_Service_Drive;
use Google_Service_Drive_DriveFile as Google_Service_Drive_DriveFile;
use Google_Service_Drive_Permission as Google_Service_Drive_Permission;
use Storage;
class GoogleDrive
{
	private $application_name = "Dahlia";
	private static $CREDENTIALS_PATH = 'config/';
    private static $AUTH_CONFIG_FILE_DIR = 'config/auth/';
	private $scopes = array(
		Google_Service_Drive::DRIVE
	);

    public function getApplicationName(){
    	return $this->application_name;
    }
    public function setApplicationName(string $name){
    	$this->application_name = $name;
    }
    public function addScope($scope){
    	$this->scope[] = $scope;
    }
    public function getScopes(){
    	return $this->scopes;
    }
    public function getScope($position){
        return $this->scopes[$position];
    }
    public function getCredential($drive){
        if(Storage::disk('local')->exists(self::$CREDENTIALS_PATH . $drive . ".json")){
            return Storage::disk('local')->get(self::$CREDENTIALS_PATH . $drive . ".json");
        }else{
            throw new Exception("Credential not found", 1);
            return false;
        }
    }
    public function setCredential($source, $drive){
        return Storage::disk('local')->put(self::$CREDENTIALS_PATH . $drive . ".json",
            $source
            );
    }
    public function getAuthConfig($drive){
        if(Storage::disk('local')->exists(self::$AUTH_CONFIG_FILE_DIR . $drive . ".json")){
            return Storage::disk('local')->get(self::$AUTH_CONFIG_FILE_DIR . $drive . ".json");
        }else{
            throw new Exception("Credential not found", 1);
            return false;
        }
    }
    public function getClient($drive){
        $client = new Google_Client();
        $client->setApplicationName($this->getApplicationName());
        $client->setScopes($this->getScopes());
        $client->setAuthConfig($this->getAuthConfig($drive));
        $client->setAccessType('offline');
        $client->addScope("https://www.googleapis.com/auth/drive");
        $client->setAccessToken($this->getCredential($drive));
        if ($client->isAccessTokenExpired()){
            $client->refreshToken($client->getRefreshToken());
            $this->setCredential($client->getAccessToken(), $drive);
        }
        return $client;
    }
    public function getService($client){
        return new Google_Service_Drive($client);
    }

    public function uploadFile($name, $data, $mimeType, $uploadType = "multipart"){
        $file = new Google_Service_Drive_DriveFile();
        $file->setName($name);
        $service = $this->getService($this->getClient("1"));
        $result = $service->files->create(
            $file,
            array(
                'data' => $data,
                'mimeType' => $mimeType,
                'uploadType' => $uploadType
            )
        );
        $permission = new Google_Service_Drive_Permission();
        $permission->setRole( 'reader' );
        $permission->setType( 'anyone' );
        $results = $service->permissions->create( $result->id , $permission );
        return $result->id;
    }
}
