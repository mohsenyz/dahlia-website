<?php
namespace App\Http\Controllers;
class NumberFormatting{
	public static function Arabic_w2e($str){
		$arabic_eastern = array('٠', '١', '٢', '٣', '۴', '٥', '۶', '٧', '٨', '٩');
    	$arabic_western = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
    	return str_replace($arabic_western, $arabic_eastern, $str);
	}
}