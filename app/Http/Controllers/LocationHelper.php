<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Storage;


class LocationHelper extends Controller
{
    public function parseLocation($location){
    	if(strpos($location, "{}") !== false){
    		$parse = explode("{}", $location);
    		foreach ($parse as $key => $value) {
    			if(strpos($parse, ",") !== false){
    				$prs = explode(",", $parse);
    				if(count($prs) == 1){
    					//it is country
    					$continent  = $prs[0];
    					$country = $prs[1];
    				}else if(count($prs) == 2){
    					$continent = $prs[0];
    					$country = $prs[1];
    					$state = $prs[2];
    				}
    			}else{
    				//it is continent

    			}
    		}
    	}
    }



    public function parseContinent($con){
        $file = Storage::disk('local')->get("json/continent/all.json");
        $arr = json_decode($file);
        $con = strtolower($con) . "-g";
        foreach ($arr as $key => $value) {
        	if(trim($value->ID) == $con)
        		return $value->Title;
        }
    }


}
