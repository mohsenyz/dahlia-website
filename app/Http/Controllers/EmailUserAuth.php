<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Log;
use Session;
use Auth;
use App\User;
use App\EmailRegistring;
use App\EmailBlacklist;
use App\Geo;

use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
class EmailUserAuth extends Controller
{
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;
    //
    public function sendRegisterEmail(){
        if(!Session::has("email"))
            return response()->json(['status'=>0, 'Message'=>'Email number undefined']);
        $email = Session::get("email");
        if($email == null){
    		return response()->json(['status'=>502, 'Message'=>'Bad Request']);
    	}
    	if(EmailBlacklist::issetEmail($email))
    		return response()->json(['status'=>403, 'Message'=>'Cant access']);
    	if (EmailRegistring::issetEmail($email)) {
            Log::info("yeah the Email" . $email . " isset");
            if(EmailRegistring::isAvailable($email)[0]){
                if(EmailRegistring::setupCode($email)){
                    return response()->json(['status'=>200, 'Message'=>'Message Successfully sent']);
                }else{
                    return response()->json(['status'=>0, 'Message'=>'Message cant send']);
                }
            }else{
                return response()->json(['status'=>429, 'Message'=>'Wait for ' . EmailRegistring::isAvailable($email)[1] . ' Seconds']);
            }
        }else{
            if(EmailRegistring::setupCode($email)){
                return response()->json(['status'=>200, 'Message'=>'Message Successfully sent']);
            }else{
                return response()->json(['status'=>0, 'Message'=>'Message cant send']);
            }
        }
    }


    public function isUserAvailable($email){
        if(Auth::check())
            return response()->json(['status'=>403, 'Message'=>"You are already logged in"]);
        $email = validate_email($email);
        if($email == null){
            return response()->json(['status'=>502, 'Message'=>'Bad Request']);
        }
        Session::put("email", $email);
        if(User::issetEmail($email))
            return response()->json(['status'=>0, 'Message'=>"User isn't available"]);
        else
            return response()->json(['status'=>200, 'Message'=>"User is available"]);
    }


    public function isLogin(){
    	if(Auth::check()){
    		return response()->json(['status'=>200, 'Message'=>'User loggd in']);
    	}else{
    		return response()->json(['status'=>0, 'Message'=>'User not logged in']);
    	}
    }

    public function login(Request $req){
        if(!Session::has("email"))
            return response()->json(['status'=>502, 'Message'=>'Undefined mobile']);
        $email = Session::get("email");
        $password = $req->input('password');
        if(EmailBlacklist::issetEmail($email))
            return response()->json(['status'=>403, 'Message'=>'You are blocked. Please email our support team']);
    	if(Auth::attempt(['Email' => $email, 'password' => $password], true)){
    		return response()->json(['status'=>200, 'Message'=>'Logged in']);
    	}else{
    		return response()->json(['status'=>0, 'Message'=>'Email or password is wrong']);
    	}
    }

    public function RegisterEmailVerify($code){
        if(!Session::has("email"))
            return response()->json(['status'=>0, 'Message'=>'Email undefined']);
        $email = Session::get("email");
        if($email == null){
            return response()->json(['status'=>502, 'Message'=>'Bad Request']);
        }
        $verify = EmailRegistring::verify($code);
        if($verify[0]){
            Session::put("email_verify", $email);
            Session::forget("email");
            $user = new User;
            $user->Email = $email;
            $user->Register = '1';
            $user->Date = time();
            $user->City = Geo::getCity();
            $user->Forget = '0';
            $user->isBlocked = '0';
            $user->hasPass = '0';
            $user->save();
            return response()->json(['status'=>200, 'Message'=>'Verify successfully']);
        }else if($verify[1] == 0){
            return response()->json(['status'=>0, 'Message'=>'Wrong Code']);
        }else if($verify[1] == 1){
            return response()->json(['status'=>0, 'Message'=>'Time out']);
        }
    }



    public function setPassword($password){
        if(strlen($password) < 25 && strlen($password) > 7 && Session::has("email_verify") && Session::get("email_verify") != null){
            $user = User::where("Email", Session::get("email_verify"))->first();
            $user->Password = bcrypt($password);
            $user->hasPass = '1';
            $user->save();
            Session::forget("email_verify");
            return response()->json(['status'=>200, 'Message'=>'Password set successfully']);
        }else{
            return response()->json(['status'=>502, 'Message'=>'Bad Request']);
        }
    }
    public function logout(){
        Auth::logout();
        if(!Auth::check()){
            return response()->json(['status'=>200, 'Message'=>'Logout Successfully']);
        }else{
            return response()->json(['status'=>0, 'Message'=>'An error occurded']);
        }
    }
}
