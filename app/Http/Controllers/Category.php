<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App;
use DB;

/*
return [
    'appliances' => 'Appliances',
    'athletic' => 'Athletic',
    'business' => 'Business',
    'electronics' => 'Electronics',
    'estate' => 'Estate',
    'mobile' => 'Mobile',
    'personal' => 'Personal',
    'services' => 'Services',
    'vehicle' => 'Vehicle',
];
Appliances
Athletic
Business
Electronics
Estate
Mobile
Personal
Services
Vehicle

*/
class Category extends Controller
{
	public function getCategory(){
		App::setLocale("fa");
		return response()->json(
			array(
				['ID' => '1', 'Data' => trans('category.appliances'), 'Icon' => 'flaticon-washing-machine'],
				['ID' => '2', 'Data' => trans('category.athletic'), 'Icon' => 'flaticon-sports'],
				['ID' => '3', 'Data' => trans('category.business'), 'Icon' => 'flaticon-business'],
				['ID' => '4', 'Data' => trans('category.electronics'), 'Icon' => 'flaticon-computer'],
				['ID' => '5', 'Data' => trans('category.estate'), 'Icon' => 'flaticon-home'],
				['ID' => '6', 'Data' => trans('category.mobile'), 'Icon' => 'flaticon-smartphone'],
				['ID' => '7', 'Data' => trans('category.personal'), 'Icon' => 'flaticon-shoe'],
				['ID' => '8', 'Data' => trans('category.services'), 'Icon' => 'flaticon-call-center-worker-with-headset'],
				['ID' => '9', 'Data' => trans('category.vehicle'), 'Icon' => 'flaticon-sports-car'],
				['ID' => '10', 'Data' => trans('category.employment'), 'Icon' => 'flaticon-sports-car']
			)
		);
	}

	public function getSubCategory($id){
		App::setLocale("fa");
		$users = DB::table('CategoryType')->where('Type', $this->getSubCategoryFromId((int)$id))->get();
		foreach ($users as $key => $user) {
			$user->Title = trans("subcat." . strtolower(str_replace(' ', '', $user->Title)));
		}
		return $users;
	}

	public function getKindCategory($id){
		$users = DB::table('CategoryKind')->select("Id","Title")->where('Sub', $id)->get();
		return $users;
	}

	private function getSubCategoryFromId($id){
		$arr = array(
				'Appliances',
				'Athletic',
				'Business',
				'Electronics',
				'Estate',
				'Mobile',
				'Personal',
				'Services',
				'Vehicle',
				'Employment'
				);
		return strtolower($arr[($id -1)]);
	}

	public function parseCategoryToString($cat){
		App::setLocale("fa");
		if(strpos($cat, ",") === false){
			return trans('category.other');
		}
		$arr = explode(",", $cat);
		if(count($arr) == 2){
			return trans('category.' . strtolower($this->getSubCategoryFromId((int) $arr[0]))) . "," . trans('subcat.' . strtolower(str_replace(' ', '', DB::table('CategoryType')->where('Type', strtolower($this->getSubCategoryFromId((int) $arr[0])))->where('Id', $arr[1])->first()->Title)));
		}

		if(count($arr) == 3){
			return trans('category.' . strtolower($this->getSubCategoryFromId((int) $arr[0]))) . "," . trans('subcat.' . strtolower(str_replace(' ', '', DB::table('CategoryType')->where('Type', strtolower($this->getSubCategoryFromId((int) $arr[0])))->where('Id', $arr[1])->first()->Title))) . "," . DB::table('CategoryKind')->select("Id","Title")->where('Sub', $arr[1])->where("Id", $arr[2])->first()->Title;
		}
	}
}
