<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Log;
use Session;
use Auth;
use App\User;
use App\CodeRegistring;
use App\Blacklist;
use App\Geo;

use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
class UserAuth extends Controller
{
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;
    //
    public function sendRegisterCode(){
        if(!Session::has("phone"))
            return response()->json(['status'=>0, 'Message'=>'Phone number undefined']);
        $phone = Session::get("phone");
        if($phone == null){
    		return response()->json(['status'=>502, 'Message'=>'Bad Request']);
    	}
    	if(Blacklist::issetPhone($phone))
    		return response()->json(['status'=>403, 'Message'=>'Cant access']);
    	if (CodeRegistring::issetPhone($phone)) {
            Log::info("yeah the phone" . $phone . " isset");
            if(CodeRegistring::isAvailable($phone)[0]){
                if(CodeRegistring::setupCode($phone)){
                    return response()->json(['status'=>200, 'Message'=>'Message Successfully sent']);
                }else{
                    return response()->json(['status'=>0, 'Message'=>'Message cant send']);
                }
            }else{
                return response()->json(['status'=>429, 'Message'=>'Wait for ' . CodeRegistring::isAvailable($phone)[1] . ' Seconds']);
            }
        }else{
            if(CodeRegistring::setupCode($phone)){
                return response()->json(['status'=>200, 'Message'=>'Message Successfully sent']);
            }else{
                return response()->json(['status'=>0, 'Message'=>'Message cant send']);
            }
        }
    }


    public function isUserAvailable($phone){
        if(Auth::check())
            return response()->json(['status'=>403, 'Message'=>"You are already logged in"]);
        $phone = validate_phone($phone);
        if($phone == null){
            return response()->json(['status'=>502, 'Message'=>'Bad Request']);
        }
        Session::put("phone", $phone);
        if(User::issetPhone($phone))
            return response()->json(['status'=>0, 'Message'=>"User isn't available"]);
        else
            return response()->json(['status'=>200, 'Message'=>"User is available"]);
    }


    public function isLogin(){
    	if(Auth::check()){
    		return response()->json(['status'=>200, 'Message'=>'User loggd in']);
    	}else{
    		return response()->json(['status'=>0, 'Message'=>'User not logged in']);
    	}
    }

    public function login(Request $req){
        if(!Session::has("phone"))
            return response()->json(['status'=>502, 'Message'=>'Undefined mobile']);
        $phone = Session::get("phone");
        $password = $req->input('password');
        if(Blacklist::issetPhone($phone))
            return response()->json(['status'=>403, 'Message'=>'You are blocked. Please email our support team']);
    	if(Auth::attempt(['Phone' => $phone, 'password' => $password], true)){
    		return response()->json(['status'=>200, 'Message'=>'Logged in']);
    	}else{
    		return response()->json(['status'=>0, 'Message'=>'Phone number or password is wrong']);
    	}
    }

    public function RegisterCodeVerify($code){
        if(!Session::has("phone"))
            return response()->json(['status'=>0, 'Message'=>'Phone number undefined']);
        $phone = Session::get("phone");
        if($phone == null){
            return response()->json(['status'=>502, 'Message'=>'Bad Request']);
        }
        $verify = CodeRegistring::verify($code);
        if($verify[0]){
            Session::put("phone_verify", $phone);
            Session::forget("phone");
            $user = new User;
            $user->Phone = $phone;
            $user->Register = '1';
            $user->Date = time();
            try {
                $geo = new Geo;
                $user->City = $geo->getCity();
            } catch (Exception $e) {
                $user->City = 'isfahan';
            }
            $user->Forget = '0';
            $user->isBlocked = '0';
            $user->hasPass = '0';
            $user->save();
            return response()->json(['status'=>200, 'Message'=>'Verify successfully']);
        }else if($verify[1] == 0){
            return response()->json(['status'=>0, 'Message'=>'Wrong Code']);
        }else if($verify[1] == 1){
            return response()->json(['status'=>0, 'Message'=>'Time out']);
        }
    }



    public function setPassword($password){
        if(strlen($password) < 25 && strlen($password) > 7 && Session::has("phone_verify") && Session::get("phone_verify") != null){
            $user = User::where("Phone", Session::get("phone_verify"))->first();
            $user->Password = bcrypt($password);
            $user->hasPass = '1';
            $user->save();
            Auth::attempt(['Phone' => Session::get('phone_verify'), 'password' => $password], true);
            Session::forget("phone_verify");
            return response()->json(['status'=>200, 'Message'=>'Password set successfully']);
        }else{
            return response()->json(['status'=>502, 'Message'=>'Bad Request']);
        }
    }
    public function logout(){
        Auth::logout();
        if(!Auth::check()){
            return response()->json(['status'=>200, 'Message'=>'Logout Successfully']);
        }else{
            return response()->json(['status'=>0, 'Message'=>'An error occurded']);
        }
    }
}
