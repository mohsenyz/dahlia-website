<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Storage;
use Hash;
use Illuminate\Support\Facades\File;
use Image;

class ImageManager extends Controller
{

    public function upload(Request $request){
        if ($request->file('image')->isValid()) {
            $file = $request->file('image');
            $mime = $file->getMimeType();
            if($mime == "image/png" || $mime == "image/jpeg" || $mime == "image/gif"){
                $file_content = File::get($file);
                $name = $this->generateKey($request->ip());
                $img = Image::make($file_content);
                $width = $img->width() * 400 / $img->height();
                $img_thumb = $img;
                $img_mobile_thumb = $img;
                $img->contrast(5);
                $img->resize($width, 400);
                Storage::disk('local')->put("picture/" . $name . ".jpg", (string)$img->encode('jpg', 80));
                $img_mobile_thumb->contrast(5);
                $img_mobile_thumb->resize(80, 80);
                Storage::disk('local')->put("mobile-thumb/" . $name . ".jpg", (string)$img_mobile_thumb->encode('jpg', 90));
                $img_thumb->contrast(5);
                $width = $img_thumb->width() * 150 / $img_thumb->height();
                $img_thumb->resize($width, 150);
                Storage::disk('local')->put("site-thumb/" . $name . ".jpg", (string)$img_thumb->encode('jpg', 100));
            }
            return $name;
        }
    }


    //Google Drive   
    /*public function upload(Request $request){
    	if ($request->file('image')->isValid()) {
    		$file = $request->file('image');
    		$name = $this->generateKey($request->ip()) . "." . $request->file('image')->guessExtension();
    		$mime = $file->getMimeType();
    		if($mime == "image/png" || $mime == "image/jpeg" || $mime == "image/gif"){
                $gd = new GoogleDrive;
                $jpg = Image::make($file);//->encode('jpg', 100);
                $jpg->resize((450 * $jpg->width() / $jpg->height()), 450);
                $pic = $jpg->encode('jpg', 90);
                $result = $gd->uploadFile($name, $pic, $mime, 'multipart');
                /*$jpg = Image::make($file);//->encode('jpg', 100);
                $jpg->resize(150, 150);
                $pic = $jpg->encode('jpg', 90);
                $result = $gd->uploadFile($name, $pic, $mime, 'multipart');
    			//Storage::disk('local')->put($name . "." . $request->file('image')->guessExtension(),File::get($file));
                return response((string)$result);
    		}
    	}
    }*/

    
    private function generateKey($a){
    	$contents = Storage::disk('local')->get('randomCodeList.txt');
    	$arr = explode(" ", $contents);
    	$name = "" . (string)time() . "_" . (string)mt_rand() . "_" . md5($a);
    	if (in_array($name, $arr)) {
    		return generateKey();
    	}else{
    		Storage::disk('local')->put('randomCodeList.txt',$contents . "," . $name);
    		return $name;
    	}
    }
    public function getImage($type, $id){
        if($type == "m"){
            $file = Storage::disk('local')->get("mobile-thumb/" . $id.'.jpg');
        }
        if($type == "t"){
            $file = Storage::disk('local')->get("site-thumb/" . $id.'.jpg');
        }
        if($type == "s"){
            $file = Storage::disk('local')->get("picture/" . $id.'.jpg');
        }
        return response($file, 200)->header('Content-Type', 'image/jpeg');
    }
}
