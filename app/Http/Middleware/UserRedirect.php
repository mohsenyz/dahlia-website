<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use App\User;
class UserRedirect
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Session::has("phone_verify") && Session::get("phone_verify") != null && !$request->isMethod('post') && !$request->is('user/phone:saved/set/password:*') && !$request->is('test/destroy') && !$request->is('user/phone/setPassword'))
            return redirect('user/phone/setPassword');

        if(Session::has("email_verify") && Session::get("email_verify") != null && !$request->isMethod('post') && !$request->is('user/email:saved/set/password:*') && !$request->is('test/destroy') && !$request->is('user/phone/setPassword'))
            return redirect('user/email/setPassword');
        return $next($request);
    }
}
