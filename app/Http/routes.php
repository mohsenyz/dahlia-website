<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use Storage as Storage;
use Session as Session;
use App\Geo as Geo;
use Illuminate\Http\Request;
use App\Http\Controllers\NumberFormatting as NumberFormatting;
Route::group(['middleware' => ['web']], function () {
    //config web middleware, because i need to use auth and session


    //Adding new advertise
    Route::get('fa/advertise/new', function () {
    	App::setLocale("fa");
    	return view('advertise-fa');
	});
	Route::get('advertise/new', function () {
    	App::setLocale("fa");
    	return view('advertise');
	});
	Route::get('user/phone/setPassword', function () {
    	return view('setpassword');
	});
	Route::get('user/email/setPassword', function () {
    	return view('setpassword');
	});
    Route::get('view/{id}', function ($id) {
    	$adv = DB::table('Advertise')->where('Id', $id)->first();
    	return view('view',['adv' => $adv]);
	});
	Route::get('fa/view/{id}', function ($id) {
		App::setLocale("fa");
    	$adv = DB::table('Advertise')->where('Id', $id)->first();
    	return view('view-fa',['adv' => $adv]);
	});
	Route::get('sales/room', function () {
		$sales = DB::table('Salesroom')
                ->where('RoomNumber', Request::input("pid"))
                ->get();
    	return view('salesroom',['sales' => $sales]);
	}); 
	Route::get('fa/sales/room', function (Request $req) {
		App::setLocale("fa");
		if (!Auth::check()) {
			return redirect('fa/sales');
		}
		$belong = false;
		if(DB::table('Salesroom')->where('Id', Auth::user()->id)->where('RoomNumber', $req->input("pid"))->count() == 1){
			$belong = true;
		}
		$sales = DB::table('Salesroom')
                ->where('RoomNumber', $req->input("pid"))
                ->get();
        $salesinfo = DB::table('Sales')
                ->where('id', $req->input("pid"))
                ->first();
    	return view('salesroom-fa',['sales' => $sales, 'salesinfo' => $salesinfo, 'belong' => $belong]);
	});
	Route::get('fa/sales/room/admin', function (Request $req) {
		App::setLocale("fa");
		$sales = DB::table('Salesroom')
                ->where('RoomNumber', $req->input("pid"))
                ->get();
    	return view('adminroom',['sales' => $sales]);
	});

	Route::get('sales', function () {
    	$adv = DB::table('Advertise')->get();
		return view('sales',['adv' => $adv]);
	});
	Route::get('fa/sales', function () {
		App::setLocale("fa");
    	$adv = DB::table('Advertise')->get();
		return view('sales-fa',['adv' => $adv]);
	});
	Route::get('number', function(Request $req){
		return response(NumberFormatting::Arabic_w2e($req->input('q')));
	});

    //Upload Advertise Image
	Route::post('upload/image','ImageManager@upload');
	Route::get('upload/ali','ImageManager@ali');
	Route::get('image/{type}/{id}.jpg','ImageManager@getImage');
	Route::get('getAllCategory', function(){
		App::setLocale("fa");
		$users = DB::table('CategoryKind')->select("Id","Title")->get();
		return $users;
	});

	//Get list of Advertise catrgory
    Route::get('getCategory','Category@getCategory');

    //Get list of Advertise SubCategory
    Route::get('getSubCategory/{id}','Category@getSubCategory')->where('id', '[0-9]{1,3}');

    //Get list of Advertise kind category
    Route::get('getKindCategory/{id}','Category@getKindCategory')->where('id', '[0-9]{1,3}');

    //Send register code
    //We dont need it now , because register code will send after checking user not isset
    //Route::post('user/sendRegisterCode/{phone}', ['middleware' => 'ajax', 'uses'=>'UserAuth@sendRegisterCode']);

    //Check if user login
	Route::get('user/isLogin', ['middleware' => 'ajax', 'uses'=>'UserAuth@isLogin']);

	//Login user
	Route::post('user/login/phone', ['middleware' => 'ajax', 'uses'=>'UserAuth@login']);

	//Login by email
	Route::post('user/login/mail', ['middleware' => 'ajax', 'uses'=>'EmailUserAuth@login']);

	//Logout user
	Route::get('user/logout', ['middleware' => 'ajax', 'uses'=>'UserAuth@logout']);

	//check is user available
	Route::get('user/phone:{phone}/isAvailable', ['middleware' => 'ajax', 'uses'=>'UserAuth@isUserAvailable']);

	//Register user with a message
	Route::get('user/phone:saved/register', ['middleware' => 'ajax', 'uses'=>'UserAuth@sendRegisterCode']);

	//Verify user
	Route::get('user/phone:saved/verify:{code}', ['middleware' => 'ajax', 'uses'=>'UserAuth@RegisterCodeVerify']);

	//Set user password
	Route::get('user/phone:saved/set/password:{password}', ['middleware' => 'ajax', 'uses'=>'UserAuth@setPassword']);

/*
_____________________________________________________________________________________________________________
________________________________________    EMAIL REGISTRATION    ___________________________________________
________________________________________    ******************    ___________________________________________
____________________________________________******************_______________________________________________
*/
	//check is user available
	Route::get('user/email:{email}/isAvailable', ['middleware' => 'ajax', 'uses'=>'EmailUserAuth@isUserAvailable']);

	//Register user with a message
	Route::get('user/email:saved/register', ['middleware' => 'ajax', 'uses'=>'EmailUserAuth@sendRegisterEmail']);

	//Verify user
	Route::get('user/email:saved/verify:{code}', ['middleware' => 'ajax', 'uses'=>'EmailUserAuth@RegisterEmailVerify']);

	//Set user password
	Route::get('user/email:saved/set/password:{password}', ['middleware' => 'ajax', 'uses'=>'EmailUserAuth@setPassword']);


	Route::get('test/login:mohsen',function(){
		Auth::attempt(['Phone'=>'9162886638','password'=>'secret123']);
		if(Auth::check()){
			return response("login");
		}
	});
	Route::get('test/login:mohammad',function(){
		Auth::attempt(['Phone'=>'9387707657','password'=>'ali123456']);
		if(Auth::check()){
			return response("login");
		}
	});
	Route::post('test/req','Advertise@createAdvertise');
	Route::get('test/cat/{con}','LocationHelper@parseContinent');
	Route::get('test/destroy',function(){
		Session::forget("email_verify");
		Session::forget("phone_verify");
	});


	Route::get("/", function(){
		$adv = DB::table('Advertise')->orderBy('Date','desc')->get();
		//return view('main',['adv' => $adv]);
		return redirect('fa');
	});
	Route::get("fa/", function(){
		App::setLocale("fa");
		$adv = DB::table('Advertise')->orderBy('Date','desc')->get();
		return view('main-fa',['adv' => $adv]);
	});
	Route::get("test/app", function(){
		return response(DB::table('Advertise')->orderBy('Date','desc')->get(), 200)->header('Cache-Control', 'public, max-age=120');
	});
	Route::get('test/type', function(Request $req) {
		if(!Auth::check()){
			return response("Please login first");
		}
	    $id = Auth::user()->id;
	    $type = $req->input("type");
	    $type = explode("*", $type);
	    $dbtype = false;
	    $dbtyped = null;
	    if($type[0] == "true"){
	    	$dbtype = 1;
	    	$dbtyped = $type[1];
	    }
	    DB::table('Salesroom')
            ->where('Id', $id)
            ->where('RoomNumber', Session::get("room"))
            ->update(['IsWriting' => $dbtype, 'TypedText' => $dbtyped]);
	});
	Route::get('test/getType',function(){
		/*if(!Session::has("type"))
			return response("Is not typing");*/
			$users = DB::table('Salesroom')
                ->where('IsWriting', true)
                ->where('RoomNumber', Session::get("room"))
                ->get();
            $typer = "";
            foreach ($users as $user) {
            	if(!empty($user->TypedText))
            		$typer .= $user->Id . "#" . NumberFormatting::Arabic_w2e($user->TypedText) . "|";
            }
            return response($typer);
	});
	Route::get('test/session',function(){
		return Session::all();
	});


	Route::get('test/getPrice',function(){
		$prices = DB::table('Salesroom')
                ->where('RoomNumber', Session::get("room"))
                ->get();
        $arrp = array();
        foreach ($prices as $price) {
            if(!empty($price->Prices)){
            	$arrpr = explode("|", $price->Prices);
            	foreach ($arrpr as $value) {
            		$arrp[] = $value;
            	}
           	}
        }
        rsort($arrp);
        if(count($arrp) != 0){
        	$intmax = $arrp[0];
        }else{
        	$intmax = 0;
        }
        foreach ($prices as $price) {
            if(strpos($price->Prices, (string)$intmax) !== false){
            	$name = $price->Name;
            	$id = $price->Id;
           	}
        }
        if(isset($id)){
        	return response()->json(['status'=>200, 'price'=>$intmax, 'name'=> $name, 'id'=>$id]);
    	}
	});
	Route::get('test/setPrice',function(Request $req){
		$str = str_replace(",", "", $req->input('price'));
		$id = Auth::user()->id;
		$prices = DB::table('Salesroom')
                ->where('RoomNumber', Session::get("room"))
                ->get();
        $arrp = array();
        foreach ($prices as $price) {
            if(!empty($price->Prices)){
            	$arrpr = explode("|", $price->Prices);
            	foreach ($arrpr as $value) {
            		$arrp[] = $value;
            	}
           	}
        }
        rsort($arrp);
        if(count($arrp) != 0){
        	$intmax = $arrp[0];
        }else{
        	$intmax = 0;
        }
        if((double)$str <= $intmax){
        	return response()->json(['status'=>502, 'Message'=>'Price is less than latest price']);
        }
		$user = DB::table('Salesroom')
                ->where('Id', $id)
                ->where('RoomNumber', Session::get("room"))
                ->first();
        DB::table('Salesroom')
            ->where('Id', $id)
            ->where('RoomNumber', Session::get("room"))
            ->update(['Prices' => $user->Prices . $str . "|"]);
        return response()->json(['status'=>200, 'Message'=>'Price sets successfully']);
	});
	Route::get('test/setName', function(Request $req){
		if(DB::table('Salesroom')->where('Id', Auth::user()->id)->where('RoomNumber', Session::get('room'))->count() == 0 && Auth::check()){
			DB::table('Salesroom')->insert([
				'Id' => Auth::user()->id,
				'Name' => $req->input('name'),
				'RoomNumber' => Session::get("room"),
				'Phone' => Auth::user()->Phone
				]);
			return response()->json(['status'=>200]);
		}
	});
	Route::get('test/getall',function(){
		$str = "";
		$str .= "" . time() . "" ."<br>";
		$time1 = time();
		$it = DB::table('test')->get();
		$i = 0;
		foreach ($it as $value) {
			$p = 0;
			similar_text($value->content, 'laravel php routes route middleware web', $p);
			if ($p > 5) {
				$i++;
				$str .= "" . $i . " " . $value->subject . "<br>";
			}
		}
		$str .= "" . "" . time() . "<br>";
		$time2 = time();
		$str .= "<br><br><br><br>" . ($time2 - $time1);
		return response($str);
	});

	Route::get('test/compare',function(){
		return response((9999999999999.99 * 100));
	});
	/*
	------------------------- TEST ------------------------




	Route::get('test/auth/login', function(){
		if(Auth::attempt(['Phone' => "9162886638" , 'password' => "secret"], true)){
			return response()->json(['status' => "200"]);
		}else{
			return response()->json(['status' => "0"]);
		}
	});

	Route::get('test/auth/check', function(){
		if(Auth::check()){
			return Auth::user();
		}else{
			return response()->json(['status' => "0"]);
		}
	});
	Route::get('test/auth/logout', function(){
		Auth::logout();
		if(!Auth::check())
			return response()->json(['status' => "200"]);
		else
			return response()->json(['status' => "0"]);
	});




	------------------------- TEST ------------------------
	*/


	Route::get('salesroom/sendMessage:admin/msg', function(Request $req){
		if(!Session::has("room"))return response()->json(['status'=>500, 'Message'=>'Bad Request']);
		if(!Auth::check())return response()->json(['status'=>403, 'Message'=>'Forbidden']);
		if(DB::table('Salesroom')->where('Id', Auth::user()->id)->where('RoomNumber', Session::get('room'))->count() == 0)return response()->json(['status'=>403, 'Message'=>'Forbidden, please first register in the room']);
		$msg = $req->input('content', null);
		if($msg != null){
			$adminId = DB::table('Sales')->where('id', Session::get('room'))->first()->admin;
			$time = time();
			$res = DB::table('SalesroomMsg')->insertGetId([
				"sender" => Auth::user()->id,
				"receiver" => $adminId,
				"content" => $msg,
				"time" => $time,
				"read" => 0,
				]);
			return response()->json(['status'=>200, 'Message'=>'Sent', 'id'=> $res]);
		}
		return response()->json(['status'=>500, 'Message'=>'Bad Request']);
	});
	Route::get('salesroom/getMessages:admin/after', function(Request $req){
		if(!Session::has("room"))return response()->json(['status'=>500, 'Message'=>'Bad Request']);
		if(!Auth::check())return response()->json(['status'=>403, 'Message'=>'Forbidden']);
		if(DB::table('Salesroom')->where('Id', Auth::user()->id)->where('RoomNumber', Session::get('room'))->count() == 0)return response()->json(['status'=>403, 'Message'=>'Forbidden, please first register in the room']);
		$id = $req->input('id', null);
		if($id != null){
			$adminId = DB::table('Sales')->where('id', Session::get('room'))->first()->admin;
			$GLOBALS['z'] = $adminId;
			return DB::table('SalesroomMsg')
				->where('id', '>', (string)$id)
				->where(function($query){
					$query->where('sender', Auth::user()->id)
						->where('receiver', $GLOBALS['z']);
				})
				->orWhere(function($query){
					$query->where('receiver', Auth::user()->id)
						->where('sender', $GLOBALS['z']);
				})
				->where('id', '>', (string)$id)
				->get();
		}
		return response()->json(['status'=>500, 'Message'=>'Bad Request']);
	});

	Route::get('salesroom/getMessages:admin/setReaded/until', function(Request $req){
		if(!Session::has("room"))return response()->json(['status'=>500, 'Message'=>'Bad Request']);
		if(!Auth::check())return response()->json(['status'=>403, 'Message'=>'Forbidden']);
		if(DB::table('Salesroom')->where('Id', Auth::user()->id)->where('RoomNumber', Session::get('room'))->count() == 0)return response()->json(['status'=>403, 'Message'=>'Forbidden, please first register in the room']);
		$id = $req->input('id', null);
		if($id != null){
			$adminId = DB::table('Sales')->where('id', Session::get('room'))->first()->admin;
			DB::table('SalesroomMsg')->where("receiver", Auth::user()->id)->where('sender', $adminId)->where('id', '<=', $id)->update(['read' => 1]);
			return response()->json(['status'=>200, 'Message'=>'Updated']);
		}
		return response()->json(['status'=>500, 'Message'=>'Bad Request']);
	});

	Route::get('salesroom/getMessages:admin/getReaded/last', function(){
		if(!Session::has("room"))return response()->json(['status'=>500, 'Message'=>'Bad Request']);
		if(!Auth::check())return response()->json(['status'=>403, 'Message'=>'Forbidden']);
		if(DB::table('Salesroom')->where('Id', Auth::user()->id)->where('RoomNumber', Session::get('room'))->count() == 0)return response()->json(['status'=>403, 'Message'=>'Forbidden, please first register in the room']);
		$adminId = DB::table('Sales')->where('id', Session::get('room'))->first()->admin;
		$id = DB::table('SalesroomMsg')->where("sender", Auth::user()->id)->where('receiver', $adminId)->where('read', 1)->orderBy('id', 'desc')->first()->id;
		return response()->json(['status'=>200, 'Message'=>'Success', 'id'=> $id]);
	});
});
