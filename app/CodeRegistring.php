<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Blacklist;
use Session;
class CodeRegistring extends Model
{
	//This model created to manage sended code for registring :-)
    protected $table = 'CodeRegistring';
    protected $primaryKey = 'Phone';
    public $incrementing = false;
    public $timestamps = false;

    public static function issetPhone($phone){
    	if(CodeRegistring::where("Phone", $phone)->count() == 0){
    		return false;
    	}else{
    		return true;
        }
    }


    public static function isAvailable($phone){

    	$user = CodeRegistring::where("Phone", $phone)->first();
    	if((int)$user->Times == 0){
    		if((time() - (int)$user->SentDate) >= 90){
    			return array(true, null);
    		}else{
    			return array(false, 90 - (time() - (int)$user->SentDate));
    		}
    	}
    	if((int)$user->Times == 1){
    		if((time() - (int)$user->SentDate) >= 240){
    			return array(true, null);
    		}else{
    			return array(false, 240 - (time() - (int)$user->SentDate));
    		}
    	}
        if((int)$user->Times == 2){
            Blacklist::setIntoBlacklist($phone , 0);
        }
    }



    public static function setUpCode($phone){
    	//It must send msg too , using Sms model
        if(!CodeRegistring::issetPhone($phone)){
            $user = new CodeRegistring;
            $user->Phone = $phone;
        }else{
            $user = CodeRegistring::where("Phone", $phone)->first();
        }
        if(!CodeRegistring::issetPhone($phone))
            $user->Times = -1;
    	$user->Times = $user->Times + 1;
    	$user->Code = mt_rand(10000,99999);
    	$user->SentDate = time();
        if(Sms::sendCode($phone,"Your Code is " . (string)$user->Code) == 0){
            $user->save();
            return true;
        }else{
            return false;
        }
    }


    public static function verify($code){
        if(!Session::has("phone"))
            return response()->json(['status'=>0, 'Message'=>'Phone number undefined']);
        $phone = Session::get("phone");
        if($phone == null){
            return response()->json(['status'=>502, 'Message'=>'Bad Request']);
        }
        $user = CodeRegistring::where("Phone", $phone)->first();
        if((time() - (int)$user->SentDate) > 90)
            return array(false , 1);
        if($user->Code == $code)
            return array(true , 0);
        return array(false , 0);
    }


}
