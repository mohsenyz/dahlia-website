<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Log;
class Sms extends Model
{
    //Manage Sending Sms
    protected $table = 'SmsLog';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public static function sendCode($phone, $msg){
    	$resp = send_msg($phone, $msg);
    	$sms = new Sms;
    	$sms->Phone = $phone;
    	$sms->Msg = $msg;
    	$sms->SentTime = time();
    	$sms->Code = $resp[1];
    	$sms->Status = (int)$resp[0];
    	$sms->save();
        Log::info("Phone : " . "+98" . $phone . "         ++++++++++++++         " . $msg);
    	return $sms->Status; 
    }
}
