<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\EmailBlacklist;
use Session;
use Log;
class EmailRegistring extends Model
{
	//This model created to manage sended code for registring :-)
    protected $table = 'EmailRegistring';
    protected $primaryKey = 'Email';
    public $incrementing = false;
    public $timestamps = false;

    public static function issetEmail($email){
    	if(EmailRegistring::where("Email", $email)->count() == 0){
    		return false;
    	}else{
    		return true;
        }
    }


    public static function isAvailable($email){

    	$user = EmailRegistring::where("Email", $email)->first();
    	if((time() - (int)$user->SentDate) >= 60){
    		return array(true, null);
    	}else{
    		return array(false, 60 - (time() - (int)$user->SentDate));
    	}
        if((int)$user->Times == 50){
            EmailBlacklist::setIntoBlacklist($email , 0);
        }
    }



    public static function setUpCode($email){
    	//It must send msg too , using Sms model
        if(!EmailRegistring::issetEmail($email)){
            $user = new EmailRegistring;
            $user->Email = $email;
        }else{
            $user = EmailRegistring::where("Email", $email)->first();
        }
        if(!EmailRegistring::issetEmail($email))
            $user->Times = -1;
    	$user->Times = $user->Times + 1;
    	$user->Code = mt_rand(100000,999999);
    	$user->SentDate = time();
        Log::info($email . $user->Code);
        if(Email::sendCode($email,(string)$user->Code) == 200){
            $user->save();
            return true;
        }else{
            return false;
        }
    }


    public static function verify($code){
        if(!Session::has("email"))
            return response()->json(['status'=>0, 'Message'=>'Email undefined']);
        $email = Session::get("email");
        if($email == null){
            return response()->json(['status'=>502, 'Message'=>'Bad Request']);
        }
        $user = EmailRegistring::where("Email", $email)->first();
        if((time() - (int)$user->SentDate) > 90)
            return array(false , 1);
        if($user->Code == $code)
            return array(true , 0);
        return array(false , 0);
    }


}
