<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blacklist extends Model
{
    protected $table = 'BlackList';
    protected $primaryKey = 'Phone';
    public $incrementing = false;
    public $timestamps = false;

    public static function issetPhone($phone){
    	if(Blacklist::where("Phone", $phone)->count() == 0)
    		return false;
    	else
    		return true;
    }

    
    public static function setIntoBlacklist($phone, $type){
        if(!Blacklist::issetPhone($phone)){
            $blacklist = new Blacklist;
            $blacklist->Phone = $phone;
            $blacklist->Date = time();
            $blacklist->Type = $type;
            $blacklist->save();
        }
    }
}
